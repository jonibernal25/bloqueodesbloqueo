/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "nota_credito", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaCredito.findAll", query = "SELECT n FROM NotaCredito n"),
    @NamedQuery(name = "NotaCredito.findById", query = "SELECT n FROM NotaCredito n WHERE n.id = :id"),
    @NamedQuery(name = "NotaCredito.findByIdCliente", query = "SELECT n FROM NotaCredito n WHERE n.idCliente = :idCliente"),
    @NamedQuery(name = "NotaCredito.findByIdMoneda", query = "SELECT n FROM NotaCredito n WHERE n.idMoneda = :idMoneda"),
    @NamedQuery(name = "NotaCredito.findByNroNotaCredito", query = "SELECT n FROM NotaCredito n WHERE n.nroNotaCredito = :nroNotaCredito"),
    @NamedQuery(name = "NotaCredito.findByFecha", query = "SELECT n FROM NotaCredito n WHERE n.fecha = :fecha"),
    @NamedQuery(name = "NotaCredito.findByRazonSocial", query = "SELECT n FROM NotaCredito n WHERE n.razonSocial = :razonSocial"),
    @NamedQuery(name = "NotaCredito.findByDireccion", query = "SELECT n FROM NotaCredito n WHERE n.direccion = :direccion"),
    @NamedQuery(name = "NotaCredito.findByRuc", query = "SELECT n FROM NotaCredito n WHERE n.ruc = :ruc"),
    @NamedQuery(name = "NotaCredito.findByTelefono", query = "SELECT n FROM NotaCredito n WHERE n.telefono = :telefono"),
    @NamedQuery(name = "NotaCredito.findByAnulado", query = "SELECT n FROM NotaCredito n WHERE n.anulado = :anulado"),
    @NamedQuery(name = "NotaCredito.findByImpreso", query = "SELECT n FROM NotaCredito n WHERE n.impreso = :impreso"),
    @NamedQuery(name = "NotaCredito.findByEntregado", query = "SELECT n FROM NotaCredito n WHERE n.entregado = :entregado"),
    @NamedQuery(name = "NotaCredito.findByFechaEntrega", query = "SELECT n FROM NotaCredito n WHERE n.fechaEntrega = :fechaEntrega"),
    @NamedQuery(name = "NotaCredito.findByObservacion", query = "SELECT n FROM NotaCredito n WHERE n.observacion = :observacion"),
    @NamedQuery(name = "NotaCredito.findByMontoIva5", query = "SELECT n FROM NotaCredito n WHERE n.montoIva5 = :montoIva5"),
    @NamedQuery(name = "NotaCredito.findByMontoImponible5", query = "SELECT n FROM NotaCredito n WHERE n.montoImponible5 = :montoImponible5"),
    @NamedQuery(name = "NotaCredito.findByMontoTotal5", query = "SELECT n FROM NotaCredito n WHERE n.montoTotal5 = :montoTotal5"),
    @NamedQuery(name = "NotaCredito.findByMontoIva10", query = "SELECT n FROM NotaCredito n WHERE n.montoIva10 = :montoIva10"),
    @NamedQuery(name = "NotaCredito.findByMontoImponible10", query = "SELECT n FROM NotaCredito n WHERE n.montoImponible10 = :montoImponible10"),
    @NamedQuery(name = "NotaCredito.findByMontoTotal10", query = "SELECT n FROM NotaCredito n WHERE n.montoTotal10 = :montoTotal10"),
    @NamedQuery(name = "NotaCredito.findByMontoTotalExento", query = "SELECT n FROM NotaCredito n WHERE n.montoTotalExento = :montoTotalExento"),
    @NamedQuery(name = "NotaCredito.findByMontoIvaTotal", query = "SELECT n FROM NotaCredito n WHERE n.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "NotaCredito.findByMontoImponibleTotal", query = "SELECT n FROM NotaCredito n WHERE n.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "NotaCredito.findByMontoTotalNotaCredito", query = "SELECT n FROM NotaCredito n WHERE n.montoTotalNotaCredito = :montoTotalNotaCredito")})
public class NotaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "id_moneda")
    private int idMoneda;
    @Basic(optional = false)
    @Column(name = "nro_nota_credito")
    private String nroNotaCredito;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @Column(name = "impreso")
    private Character impreso;
    @Basic(optional = false)
    @Column(name = "entregado")
    private Character entregado;
    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrega;
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "monto_iva_5")
    private BigInteger montoIva5;
    @Basic(optional = false)
    @Column(name = "monto_imponible_5")
    private BigInteger montoImponible5;
    @Basic(optional = false)
    @Column(name = "monto_total_5")
    private BigInteger montoTotal5;
    @Basic(optional = false)
    @Column(name = "monto_iva_10")
    private BigInteger montoIva10;
    @Basic(optional = false)
    @Column(name = "monto_imponible_10")
    private BigInteger montoImponible10;
    @Basic(optional = false)
    @Column(name = "monto_total_10")
    private BigInteger montoTotal10;
    @Basic(optional = false)
    @Column(name = "monto_total_exento")
    private BigInteger montoTotalExento;
    @Basic(optional = false)
    @Column(name = "monto_iva_total")
    private BigInteger montoIvaTotal;
    @Basic(optional = false)
    @Column(name = "monto_imponible_total")
    private BigInteger montoImponibleTotal;
    @Basic(optional = false)
    @Column(name = "monto_total_nota_credito")
    private BigInteger montoTotalNotaCredito;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Talonario idTalonario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "notaCredito")
    private Collection<NotaCreditoDetalle> notaCreditoDetalleCollection;

    public NotaCredito() {
    }

    public NotaCredito(Integer id) {
        this.id = id;
    }

    public NotaCredito(Integer id, int idMoneda, String nroNotaCredito, Date fecha, String razonSocial, Character anulado, Character impreso, Character entregado, BigInteger montoIva5, BigInteger montoImponible5, BigInteger montoTotal5, BigInteger montoIva10, BigInteger montoImponible10, BigInteger montoTotal10, BigInteger montoTotalExento, BigInteger montoIvaTotal, BigInteger montoImponibleTotal, BigInteger montoTotalNotaCredito) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroNotaCredito = nroNotaCredito;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.anulado = anulado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigInteger getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigInteger montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigInteger getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigInteger montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigInteger getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigInteger montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigInteger getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigInteger montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigInteger getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigInteger montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigInteger getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigInteger montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigInteger getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigInteger montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigInteger getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigInteger montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigInteger getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigInteger montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigInteger getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigInteger montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public Talonario getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Talonario idTalonario) {
        this.idTalonario = idTalonario;
    }

    @XmlTransient
    public Collection<NotaCreditoDetalle> getNotaCreditoDetalleCollection() {
        return notaCreditoDetalleCollection;
    }

    public void setNotaCreditoDetalleCollection(Collection<NotaCreditoDetalle> notaCreditoDetalleCollection) {
        this.notaCreditoDetalleCollection = notaCreditoDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCredito)) {
            return false;
        }
        NotaCredito other = (NotaCredito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.NotaCredito[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "trafico_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TraficoDetalle.findAll", query = "SELECT t FROM TraficoDetalle t"),
    @NamedQuery(name = "TraficoDetalle.findByIdLiquidacion", query = "SELECT t FROM TraficoDetalle t WHERE t.traficoDetallePK.idLiquidacion = :idLiquidacion"),
    @NamedQuery(name = "TraficoDetalle.findByNroLinea", query = "SELECT t FROM TraficoDetalle t WHERE t.traficoDetallePK.nroLinea = :nroLinea"),
    @NamedQuery(name = "TraficoDetalle.findByIdTipoDocumento", query = "SELECT t FROM TraficoDetalle t WHERE t.traficoDetallePK.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "TraficoDetalle.findByCantDocRecibidos", query = "SELECT t FROM TraficoDetalle t WHERE t.cantDocRecibidos = :cantDocRecibidos"),
    @NamedQuery(name = "TraficoDetalle.findByCantDocEnviados", query = "SELECT t FROM TraficoDetalle t WHERE t.cantDocEnviados = :cantDocEnviados")})
public class TraficoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TraficoDetallePK traficoDetallePK;
    @Basic(optional = false)
    @Column(name = "cant_doc_recibidos")
    private int cantDocRecibidos;
    @Basic(optional = false)
    @Column(name = "cant_doc_enviados")
    private int cantDocEnviados;
    @JoinColumns({
        @JoinColumn(name = "id_liquidacion", referencedColumnName = "id_liquidacion", insertable = false, updatable = false),
        @JoinColumn(name = "nro_linea", referencedColumnName = "nro_linea", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private LiquidacionDetalle liquidacionDetalle;

    public TraficoDetalle() {
    }

    public TraficoDetalle(TraficoDetallePK traficoDetallePK) {
        this.traficoDetallePK = traficoDetallePK;
    }

    public TraficoDetalle(TraficoDetallePK traficoDetallePK, int cantDocRecibidos, int cantDocEnviados) {
        this.traficoDetallePK = traficoDetallePK;
        this.cantDocRecibidos = cantDocRecibidos;
        this.cantDocEnviados = cantDocEnviados;
    }

    public TraficoDetalle(int idLiquidacion, int nroLinea, int idTipoDocumento) {
        this.traficoDetallePK = new TraficoDetallePK(idLiquidacion, nroLinea, idTipoDocumento);
    }

    public TraficoDetallePK getTraficoDetallePK() {
        return traficoDetallePK;
    }

    public void setTraficoDetallePK(TraficoDetallePK traficoDetallePK) {
        this.traficoDetallePK = traficoDetallePK;
    }

    public int getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(int cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public int getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(int cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public LiquidacionDetalle getLiquidacionDetalle() {
        return liquidacionDetalle;
    }

    public void setLiquidacionDetalle(LiquidacionDetalle liquidacionDetalle) {
        this.liquidacionDetalle = liquidacionDetalle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (traficoDetallePK != null ? traficoDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TraficoDetalle)) {
            return false;
        }
        TraficoDetalle other = (TraficoDetalle) object;
        if ((this.traficoDetallePK == null && other.traficoDetallePK != null) || (this.traficoDetallePK != null && !this.traficoDetallePK.equals(other.traficoDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.TraficoDetalle[ traficoDetallePK=" + traficoDetallePK + " ]";
    }
    
}

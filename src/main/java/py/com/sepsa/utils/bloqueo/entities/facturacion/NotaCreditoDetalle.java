/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "nota_credito_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaCreditoDetalle.findAll", query = "SELECT n FROM NotaCreditoDetalle n"),
    @NamedQuery(name = "NotaCreditoDetalle.findByIdNotaCredito", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.notaCreditoDetallePK.idNotaCredito = :idNotaCredito"),
    @NamedQuery(name = "NotaCreditoDetalle.findByIdFactura", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.notaCreditoDetallePK.idFactura = :idFactura"),
    @NamedQuery(name = "NotaCreditoDetalle.findByNroLinea", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.notaCreditoDetallePK.nroLinea = :nroLinea"),
    @NamedQuery(name = "NotaCreditoDetalle.findByDescripcion", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.descripcion = :descripcion"),
    @NamedQuery(name = "NotaCreditoDetalle.findByPorcentajeIva", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.porcentajeIva = :porcentajeIva"),
    @NamedQuery(name = "NotaCreditoDetalle.findByMontoIva", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.montoIva = :montoIva"),
    @NamedQuery(name = "NotaCreditoDetalle.findByMontoImponible", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.montoImponible = :montoImponible"),
    @NamedQuery(name = "NotaCreditoDetalle.findByMontoTotal", query = "SELECT n FROM NotaCreditoDetalle n WHERE n.montoTotal = :montoTotal")})
public class NotaCreditoDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotaCreditoDetallePK notaCreditoDetallePK;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "porcentaje_iva")
    private BigInteger porcentajeIva;
    @Basic(optional = false)
    @Column(name = "monto_iva")
    private BigInteger montoIva;
    @Basic(optional = false)
    @Column(name = "monto_imponible")
    private BigInteger montoImponible;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private BigInteger montoTotal;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "id_nota_credito", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private NotaCredito notaCredito;

    public NotaCreditoDetalle() {
    }

    public NotaCreditoDetalle(NotaCreditoDetallePK notaCreditoDetallePK) {
        this.notaCreditoDetallePK = notaCreditoDetallePK;
    }

    public NotaCreditoDetalle(NotaCreditoDetallePK notaCreditoDetallePK, String descripcion, BigInteger porcentajeIva, BigInteger montoIva, BigInteger montoImponible, BigInteger montoTotal) {
        this.notaCreditoDetallePK = notaCreditoDetallePK;
        this.descripcion = descripcion;
        this.porcentajeIva = porcentajeIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
    }

    public NotaCreditoDetalle(int idNotaCredito, int idFactura, BigInteger nroLinea) {
        this.notaCreditoDetallePK = new NotaCreditoDetallePK(idNotaCredito, idFactura, nroLinea);
    }

    public NotaCreditoDetallePK getNotaCreditoDetallePK() {
        return notaCreditoDetallePK;
    }

    public void setNotaCreditoDetallePK(NotaCreditoDetallePK notaCreditoDetallePK) {
        this.notaCreditoDetallePK = notaCreditoDetallePK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(BigInteger porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigInteger getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigInteger montoIva) {
        this.montoIva = montoIva;
    }

    public BigInteger getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigInteger montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigInteger getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigInteger montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public NotaCredito getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(NotaCredito notaCredito) {
        this.notaCredito = notaCredito;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notaCreditoDetallePK != null ? notaCreditoDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaCreditoDetalle)) {
            return false;
        }
        NotaCreditoDetalle other = (NotaCreditoDetalle) object;
        if ((this.notaCreditoDetallePK == null && other.notaCreditoDetallePK != null) || (this.notaCreditoDetallePK != null && !this.notaCreditoDetallePK.equals(other.notaCreditoDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.NotaCreditoDetalle[ notaCreditoDetallePK=" + notaCreditoDetallePK + " ]";
    }
    
}

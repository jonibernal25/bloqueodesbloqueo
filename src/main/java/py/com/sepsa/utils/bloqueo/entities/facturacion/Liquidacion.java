/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "liquidacion", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Liquidacion.findAll", query = "SELECT l FROM Liquidacion l"),
    @NamedQuery(name = "Liquidacion.findById", query = "SELECT l FROM Liquidacion l WHERE l.id = :id"),
    @NamedQuery(name = "Liquidacion.findByIdMoneda", query = "SELECT l FROM Liquidacion l WHERE l.idMoneda = :idMoneda"),
    @NamedQuery(name = "Liquidacion.findByFechaDesde", query = "SELECT l FROM Liquidacion l WHERE l.fechaDesde = :fechaDesde"),
    @NamedQuery(name = "Liquidacion.findByFechaHasta", query = "SELECT l FROM Liquidacion l WHERE l.fechaHasta = :fechaHasta"),
    @NamedQuery(name = "Liquidacion.findByFechaProceso", query = "SELECT l FROM Liquidacion l WHERE l.fechaProceso = :fechaProceso"),
    @NamedQuery(name = "Liquidacion.findByMontoTotalDetalle", query = "SELECT l FROM Liquidacion l WHERE l.montoTotalDetalle = :montoTotalDetalle"),
    @NamedQuery(name = "Liquidacion.findByMontoTotalExcedente", query = "SELECT l FROM Liquidacion l WHERE l.montoTotalExcedente = :montoTotalExcedente"),
    @NamedQuery(name = "Liquidacion.findByMontoDescuentoGeneral", query = "SELECT l FROM Liquidacion l WHERE l.montoDescuentoGeneral = :montoDescuentoGeneral"),
    @NamedQuery(name = "Liquidacion.findByMontoDescuentoExcedente", query = "SELECT l FROM Liquidacion l WHERE l.montoDescuentoExcedente = :montoDescuentoExcedente"),
    @NamedQuery(name = "Liquidacion.findByMontoTotalLiquidado", query = "SELECT l FROM Liquidacion l WHERE l.montoTotalLiquidado = :montoTotalLiquidado"),
    @NamedQuery(name = "Liquidacion.findByCantDocRecibidos", query = "SELECT l FROM Liquidacion l WHERE l.cantDocRecibidos = :cantDocRecibidos"),
    @NamedQuery(name = "Liquidacion.findByCantDocEnviados", query = "SELECT l FROM Liquidacion l WHERE l.cantDocEnviados = :cantDocEnviados"),
    @NamedQuery(name = "Liquidacion.findByCantDocExcedidos", query = "SELECT l FROM Liquidacion l WHERE l.cantDocExcedidos = :cantDocExcedidos"),
    @NamedQuery(name = "Liquidacion.findByMontoMinimo", query = "SELECT l FROM Liquidacion l WHERE l.montoMinimo = :montoMinimo"),
    @NamedQuery(name = "Liquidacion.findByIdContrato", query = "SELECT l FROM Liquidacion l WHERE l.idContrato = :idContrato"),
    @NamedQuery(name = "Liquidacion.findByEstado", query = "SELECT l FROM Liquidacion l WHERE l.estado = :estado"),
    @NamedQuery(name = "Liquidacion.findByIdMontoMinimo", query = "SELECT l FROM Liquidacion l WHERE l.idMontoMinimo = :idMontoMinimo"),
    @NamedQuery(name = "Liquidacion.findByCobroAdelantado", query = "SELECT l FROM Liquidacion l WHERE l.cobroAdelantado = :cobroAdelantado")})
public class Liquidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_moneda")
    private Integer idMoneda;
    @Basic(optional = false)
    @Column(name = "fecha_desde")
    @Temporal(TemporalType.DATE)
    private Date fechaDesde;
    @Basic(optional = false)
    @Column(name = "fecha_hasta")
    @Temporal(TemporalType.DATE)
    private Date fechaHasta;
    @Basic(optional = false)
    @Column(name = "fecha_proceso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaProceso;
    @Basic(optional = false)
    @Column(name = "monto_total_detalle")
    private BigInteger montoTotalDetalle;
    @Basic(optional = false)
    @Column(name = "monto_total_excedente")
    private BigInteger montoTotalExcedente;
    @Basic(optional = false)
    @Column(name = "monto_descuento_general")
    private BigInteger montoDescuentoGeneral;
    @Basic(optional = false)
    @Column(name = "monto_descuento_excedente")
    private BigInteger montoDescuentoExcedente;
    @Basic(optional = false)
    @Column(name = "monto_total_liquidado")
    private BigInteger montoTotalLiquidado;
    @Basic(optional = false)
    @Column(name = "cant_doc_recibidos")
    private int cantDocRecibidos;
    @Basic(optional = false)
    @Column(name = "cant_doc_enviados")
    private int cantDocEnviados;
    @Basic(optional = false)
    @Column(name = "cant_doc_excedidos")
    private int cantDocExcedidos;
    @Basic(optional = false)
    @Column(name = "monto_minimo")
    private Character montoMinimo;
    @Column(name = "id_contrato")
    private Integer idContrato;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "id_monto_minimo")
    private Integer idMontoMinimo;
    @Basic(optional = false)
    @Column(name = "cobro_adelantado")
    private Character cobroAdelantado;
    @OneToMany(mappedBy = "idLiquidacion")
    private Collection<Factura> facturaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liquidacion")
    private Collection<LiquidacionDescuento> liquidacionDescuentoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liquidacion")
    private Collection<LiquidacionDetalle> liquidacionDetalleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liquidacion")
    private Collection<ExcedenteDetalle> excedenteDetalleCollection;

    public Liquidacion() {
    }

    public Liquidacion(Integer id) {
        this.id = id;
    }

    public Liquidacion(Integer id, Date fechaDesde, Date fechaHasta, Date fechaProceso, BigInteger montoTotalDetalle, BigInteger montoTotalExcedente, BigInteger montoDescuentoGeneral, BigInteger montoDescuentoExcedente, BigInteger montoTotalLiquidado, int cantDocRecibidos, int cantDocEnviados, int cantDocExcedidos, Character montoMinimo, Character estado, Character cobroAdelantado) {
        this.id = id;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.fechaProceso = fechaProceso;
        this.montoTotalDetalle = montoTotalDetalle;
        this.montoTotalExcedente = montoTotalExcedente;
        this.montoDescuentoGeneral = montoDescuentoGeneral;
        this.montoDescuentoExcedente = montoDescuentoExcedente;
        this.montoTotalLiquidado = montoTotalLiquidado;
        this.cantDocRecibidos = cantDocRecibidos;
        this.cantDocEnviados = cantDocEnviados;
        this.cantDocExcedidos = cantDocExcedidos;
        this.montoMinimo = montoMinimo;
        this.estado = estado;
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public BigInteger getMontoTotalDetalle() {
        return montoTotalDetalle;
    }

    public void setMontoTotalDetalle(BigInteger montoTotalDetalle) {
        this.montoTotalDetalle = montoTotalDetalle;
    }

    public BigInteger getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(BigInteger montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public BigInteger getMontoDescuentoGeneral() {
        return montoDescuentoGeneral;
    }

    public void setMontoDescuentoGeneral(BigInteger montoDescuentoGeneral) {
        this.montoDescuentoGeneral = montoDescuentoGeneral;
    }

    public BigInteger getMontoDescuentoExcedente() {
        return montoDescuentoExcedente;
    }

    public void setMontoDescuentoExcedente(BigInteger montoDescuentoExcedente) {
        this.montoDescuentoExcedente = montoDescuentoExcedente;
    }

    public BigInteger getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(BigInteger montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }

    public int getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(int cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public int getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(int cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public int getCantDocExcedidos() {
        return cantDocExcedidos;
    }

    public void setCantDocExcedidos(int cantDocExcedidos) {
        this.cantDocExcedidos = cantDocExcedidos;
    }

    public Character getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Character montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    @XmlTransient
    public Collection<Factura> getFacturaCollection() {
        return facturaCollection;
    }

    public void setFacturaCollection(Collection<Factura> facturaCollection) {
        this.facturaCollection = facturaCollection;
    }

    @XmlTransient
    public Collection<LiquidacionDescuento> getLiquidacionDescuentoCollection() {
        return liquidacionDescuentoCollection;
    }

    public void setLiquidacionDescuentoCollection(Collection<LiquidacionDescuento> liquidacionDescuentoCollection) {
        this.liquidacionDescuentoCollection = liquidacionDescuentoCollection;
    }

    @XmlTransient
    public Collection<LiquidacionDetalle> getLiquidacionDetalleCollection() {
        return liquidacionDetalleCollection;
    }

    public void setLiquidacionDetalleCollection(Collection<LiquidacionDetalle> liquidacionDetalleCollection) {
        this.liquidacionDetalleCollection = liquidacionDetalleCollection;
    }

    @XmlTransient
    public Collection<ExcedenteDetalle> getExcedenteDetalleCollection() {
        return excedenteDetalleCollection;
    }

    public void setExcedenteDetalleCollection(Collection<ExcedenteDetalle> excedenteDetalleCollection) {
        this.excedenteDetalleCollection = excedenteDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Liquidacion)) {
            return false;
        }
        Liquidacion other = (Liquidacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Liquidacion[ id=" + id + " ]";
    }
    
}

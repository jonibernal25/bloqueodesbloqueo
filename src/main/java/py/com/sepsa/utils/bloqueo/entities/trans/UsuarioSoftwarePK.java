/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class UsuarioSoftwarePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Basic(optional = false)
    @Column(name = "id_software")
    private int idSoftware;

    public UsuarioSoftwarePK() {
    }

    public UsuarioSoftwarePK(int idUsuario, int idSoftware) {
        this.idUsuario = idUsuario;
        this.idSoftware = idSoftware;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(int idSoftware) {
        this.idSoftware = idSoftware;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idSoftware;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSoftwarePK)) {
            return false;
        }
        UsuarioSoftwarePK other = (UsuarioSoftwarePK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idSoftware != other.idSoftware) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.UsuarioSoftwarePK[ idUsuario=" + idUsuario + ", idSoftware=" + idSoftware + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "proveedor_sociedad", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProveedorSociedad.findAll", query = "SELECT p FROM ProveedorSociedad p"),
    @NamedQuery(name = "ProveedorSociedad.findByIdSociedad", query = "SELECT p FROM ProveedorSociedad p WHERE p.proveedorSociedadPK.idSociedad = :idSociedad"),
    @NamedQuery(name = "ProveedorSociedad.findByIdProveedor", query = "SELECT p FROM ProveedorSociedad p WHERE p.proveedorSociedadPK.idProveedor = :idProveedor")})
public class ProveedorSociedad implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProveedorSociedadPK proveedorSociedadPK;
    @JoinColumn(name = "id_sociedad", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SociedadCliente sociedadCliente;

    public ProveedorSociedad() {
    }

    public ProveedorSociedad(ProveedorSociedadPK proveedorSociedadPK) {
        this.proveedorSociedadPK = proveedorSociedadPK;
    }

    public ProveedorSociedad(int idSociedad, int idProveedor) {
        this.proveedorSociedadPK = new ProveedorSociedadPK(idSociedad, idProveedor);
    }

    public ProveedorSociedadPK getProveedorSociedadPK() {
        return proveedorSociedadPK;
    }

    public void setProveedorSociedadPK(ProveedorSociedadPK proveedorSociedadPK) {
        this.proveedorSociedadPK = proveedorSociedadPK;
    }

    public SociedadCliente getSociedadCliente() {
        return sociedadCliente;
    }

    public void setSociedadCliente(SociedadCliente sociedadCliente) {
        this.sociedadCliente = sociedadCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorSociedadPK != null ? proveedorSociedadPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorSociedad)) {
            return false;
        }
        ProveedorSociedad other = (ProveedorSociedad) object;
        if ((this.proveedorSociedadPK == null && other.proveedorSociedadPK != null) || (this.proveedorSociedadPK != null && !this.proveedorSociedadPK.equals(other.proveedorSociedadPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ProveedorSociedad[ proveedorSociedadPK=" + proveedorSociedadPK + " ]";
    }
    
}

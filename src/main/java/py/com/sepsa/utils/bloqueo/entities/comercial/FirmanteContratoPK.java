/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class FirmanteContratoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_contrato")
    private int idContrato;
    @Basic(optional = false)
    @Column(name = "id_firmante")
    private int idFirmante;

    public FirmanteContratoPK() {
    }

    public FirmanteContratoPK(int idContrato, int idFirmante) {
        this.idContrato = idContrato;
        this.idFirmante = idFirmante;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public int getIdFirmante() {
        return idFirmante;
    }

    public void setIdFirmante(int idFirmante) {
        this.idFirmante = idFirmante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContrato;
        hash += (int) idFirmante;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FirmanteContratoPK)) {
            return false;
        }
        FirmanteContratoPK other = (FirmanteContratoPK) object;
        if (this.idContrato != other.idContrato) {
            return false;
        }
        if (this.idFirmante != other.idFirmante) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.FirmanteContratoPK[ idContrato=" + idContrato + ", idFirmante=" + idFirmante + " ]";
    }
    
}

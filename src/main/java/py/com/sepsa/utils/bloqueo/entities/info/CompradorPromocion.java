/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "comprador_promocion", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompradorPromocion.findAll", query = "SELECT c FROM CompradorPromocion c"),
    @NamedQuery(name = "CompradorPromocion.findByIdComprador", query = "SELECT c FROM CompradorPromocion c WHERE c.compradorPromocionPK.idComprador = :idComprador"),
    @NamedQuery(name = "CompradorPromocion.findByIdPromocion", query = "SELECT c FROM CompradorPromocion c WHERE c.compradorPromocionPK.idPromocion = :idPromocion"),
    @NamedQuery(name = "CompradorPromocion.findByEstado", query = "SELECT c FROM CompradorPromocion c WHERE c.estado = :estado")})
public class CompradorPromocion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CompradorPromocionPK compradorPromocionPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_comprador", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;
    @JoinColumn(name = "id_promocion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Promocion promocion;

    public CompradorPromocion() {
    }

    public CompradorPromocion(CompradorPromocionPK compradorPromocionPK) {
        this.compradorPromocionPK = compradorPromocionPK;
    }

    public CompradorPromocion(CompradorPromocionPK compradorPromocionPK, Character estado) {
        this.compradorPromocionPK = compradorPromocionPK;
        this.estado = estado;
    }

    public CompradorPromocion(int idComprador, int idPromocion) {
        this.compradorPromocionPK = new CompradorPromocionPK(idComprador, idPromocion);
    }

    public CompradorPromocionPK getCompradorPromocionPK() {
        return compradorPromocionPK;
    }

    public void setCompradorPromocionPK(CompradorPromocionPK compradorPromocionPK) {
        this.compradorPromocionPK = compradorPromocionPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compradorPromocionPK != null ? compradorPromocionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompradorPromocion)) {
            return false;
        }
        CompradorPromocion other = (CompradorPromocion) object;
        if ((this.compradorPromocionPK == null && other.compradorPromocionPK != null) || (this.compradorPromocionPK != null && !this.compradorPromocionPK.equals(other.compradorPromocionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.CompradorPromocion[ compradorPromocionPK=" + compradorPromocionPK + " ]";
    }
    
}

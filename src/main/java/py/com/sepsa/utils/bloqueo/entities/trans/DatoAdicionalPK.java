/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class DatoAdicionalPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_dato_adicional")
    private int idTipoDatoAdicional;

    public DatoAdicionalPK() {
    }

    public DatoAdicionalPK(int idDocumento, int idTipoDocumento, int idTipoDatoAdicional) {
        this.idDocumento = idDocumento;
        this.idTipoDocumento = idTipoDocumento;
        this.idTipoDatoAdicional = idTipoDatoAdicional;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getIdTipoDatoAdicional() {
        return idTipoDatoAdicional;
    }

    public void setIdTipoDatoAdicional(int idTipoDatoAdicional) {
        this.idTipoDatoAdicional = idTipoDatoAdicional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idDocumento;
        hash += (int) idTipoDocumento;
        hash += (int) idTipoDatoAdicional;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoAdicionalPK)) {
            return false;
        }
        DatoAdicionalPK other = (DatoAdicionalPK) object;
        if (this.idDocumento != other.idDocumento) {
            return false;
        }
        if (this.idTipoDocumento != other.idTipoDocumento) {
            return false;
        }
        if (this.idTipoDatoAdicional != other.idTipoDatoAdicional) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.DatoAdicionalPK[ idDocumento=" + idDocumento + ", idTipoDocumento=" + idTipoDocumento + ", idTipoDatoAdicional=" + idTipoDatoAdicional + " ]";
    }
    
}

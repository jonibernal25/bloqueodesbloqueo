/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "actualizacion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actualizacion.findAll", query = "SELECT a FROM Actualizacion a"),
    @NamedQuery(name = "Actualizacion.findById", query = "SELECT a FROM Actualizacion a WHERE a.id = :id"),
    @NamedQuery(name = "Actualizacion.findByEstado", query = "SELECT a FROM Actualizacion a WHERE a.estado = :estado"),
    @NamedQuery(name = "Actualizacion.findByFechaActualizar", query = "SELECT a FROM Actualizacion a WHERE a.fechaActualizar = :fechaActualizar"),
    @NamedQuery(name = "Actualizacion.findByFechaActualizacion", query = "SELECT a FROM Actualizacion a WHERE a.fechaActualizacion = :fechaActualizacion")})
public class Actualizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "fecha_actualizar")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizar;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_configuracion", referencedColumnName = "id")
    @ManyToOne
    private Configuracion idConfiguracion;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Instalacion idInstalacion;
    @JoinColumns({
        @JoinColumn(name = "id_software", referencedColumnName = "id_software"),
        @JoinColumn(name = "version", referencedColumnName = "version")})
    @ManyToOne
    private VersionSoftware versionSoftware;

    public Actualizacion() {
    }

    public Actualizacion(Integer id) {
        this.id = id;
    }

    public Actualizacion(Integer id, Character estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFechaActualizar() {
        return fechaActualizar;
    }

    public void setFechaActualizar(Date fechaActualizar) {
        this.fechaActualizar = fechaActualizar;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Configuracion getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(Configuracion idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public Instalacion getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(Instalacion idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public VersionSoftware getVersionSoftware() {
        return versionSoftware;
    }

    public void setVersionSoftware(VersionSoftware versionSoftware) {
        this.versionSoftware = versionSoftware;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actualizacion)) {
            return false;
        }
        Actualizacion other = (Actualizacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.Actualizacion[ id=" + id + " ]";
    }
    
}

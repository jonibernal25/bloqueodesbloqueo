/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tarifa_rango", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TarifaRango.findAll", query = "SELECT t FROM TarifaRango t"),
    @NamedQuery(name = "TarifaRango.findByCantidadInicial", query = "SELECT t FROM TarifaRango t WHERE t.cantidadInicial = :cantidadInicial"),
    @NamedQuery(name = "TarifaRango.findByCantidadFinal", query = "SELECT t FROM TarifaRango t WHERE t.cantidadFinal = :cantidadFinal"),
    @NamedQuery(name = "TarifaRango.findById", query = "SELECT t FROM TarifaRango t WHERE t.id = :id"),
    @NamedQuery(name = "TarifaRango.findByEstado", query = "SELECT t FROM TarifaRango t WHERE t.estado = :estado"),
    @NamedQuery(name = "TarifaRango.findByMonto", query = "SELECT t FROM TarifaRango t WHERE t.monto = :monto"),
    @NamedQuery(name = "TarifaRango.findByPorcentaje", query = "SELECT t FROM TarifaRango t WHERE t.porcentaje = :porcentaje")})
public class TarifaRango implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "cantidad_inicial")
    private Integer cantidadInicial;
    @Column(name = "cantidad_final")
    private Integer cantidadFinal;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "monto")
    private BigInteger monto;
    @Column(name = "porcentaje")
    private BigInteger porcentaje;
    @JoinColumn(name = "id_tarifa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tarifa idTarifa;

    public TarifaRango() {
    }

    public TarifaRango(Integer id) {
        this.id = id;
    }

    public TarifaRango(Integer id, Character estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public BigInteger getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigInteger porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Tarifa getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Tarifa idTarifa) {
        this.idTarifa = idTarifa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TarifaRango)) {
            return false;
        }
        TarifaRango other = (TarifaRango) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.TarifaRango[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class SociedadClienteLocalPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_sociedad")
    private int idSociedad;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @Column(name = "id_local")
    private int idLocal;

    public SociedadClienteLocalPK() {
    }

    public SociedadClienteLocalPK(int idSociedad, int idPersona, int idLocal) {
        this.idSociedad = idSociedad;
        this.idPersona = idPersona;
        this.idLocal = idLocal;
    }

    public int getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(int idSociedad) {
        this.idSociedad = idSociedad;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSociedad;
        hash += (int) idPersona;
        hash += (int) idLocal;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SociedadClienteLocalPK)) {
            return false;
        }
        SociedadClienteLocalPK other = (SociedadClienteLocalPK) object;
        if (this.idSociedad != other.idSociedad) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idLocal != other.idLocal) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.SociedadClienteLocalPK[ idSociedad=" + idSociedad + ", idPersona=" + idPersona + ", idLocal=" + idLocal + " ]";
    }
    
}

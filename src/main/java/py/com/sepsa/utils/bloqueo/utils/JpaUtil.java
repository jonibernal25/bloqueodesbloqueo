/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.utils;

import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.eclipse.persistence.config.PersistenceUnitProperties;

/**
 *
 * @author Rubén Ortiz
 */
public class JpaUtil {
     /**
     * Fábrica del manejador de entidades
     */
    private static EntityManagerFactory emfSepsa;
    
    /**
     * URL de la conexion
     */
    private static String URL_SEPSA;
    
    /**
     * URL de la conexion
     */
    private static String DB_USER_SEPSA;
    
    /**
     * URL de la conexion
     */
    private static String DB_PASS_SEPSA;

    public static String getURL_SEPSA() {
        return URL_SEPSA;
    }

    public static void setURL_SEPSA(String URL_SEPSA) {
        JpaUtil.URL_SEPSA = URL_SEPSA;
    }

    public static String getDB_USER_SEPSA() {
        return DB_USER_SEPSA;
    }

    public static void setDB_USER_SEPSA(String DB_USER_SEPSA) {
        JpaUtil.DB_USER_SEPSA = DB_USER_SEPSA;
    }

    public static String getDB_PASS_SEPSA() {
        return DB_PASS_SEPSA;
    }

    public static void setDB_PASS_SEPSA(String DB_PASS_SEPSA) {
        JpaUtil.DB_PASS_SEPSA = DB_PASS_SEPSA;
    }

    /**
     * Obtiene el EntityManagerFactory
     * @return EntityManagerFactory
     */
    public static EntityManagerFactory getEntityManagerFactorySepsa() {
        if(emfSepsa == null) {
            Properties properties = new Properties();
            properties.put(PersistenceUnitProperties.JDBC_URL, URL_SEPSA);
            properties.put(PersistenceUnitProperties.JDBC_USER, DB_USER_SEPSA);
            properties.put(PersistenceUnitProperties.JDBC_PASSWORD, DB_PASS_SEPSA);
            emfSepsa= Persistence.createEntityManagerFactory("sepsaPU", properties);
        }
        return emfSepsa;
    }
}

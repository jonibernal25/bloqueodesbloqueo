/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "producto_servicio", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductoServicio.findAll", query = "SELECT p FROM ProductoServicio p"),
    @NamedQuery(name = "ProductoServicio.findByIdProducto", query = "SELECT p FROM ProductoServicio p WHERE p.productoServicioPK.idProducto = :idProducto"),
    @NamedQuery(name = "ProductoServicio.findByIdServicio", query = "SELECT p FROM ProductoServicio p WHERE p.productoServicioPK.idServicio = :idServicio")})
public class ProductoServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoServicioPK productoServicioPK;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoServicio")
    private Collection<Excedente> excedenteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoServicio")
    private Collection<MontoMinimo> montoMinimoCollection;
    @OneToMany(mappedBy = "productoServicio")
    private Collection<Tarifa> tarifaCollection;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoServicio")
    private Collection<Contrato> contratoCollection;

    public ProductoServicio() {
    }

    public ProductoServicio(ProductoServicioPK productoServicioPK) {
        this.productoServicioPK = productoServicioPK;
    }

    public ProductoServicio(int idProducto, int idServicio) {
        this.productoServicioPK = new ProductoServicioPK(idProducto, idServicio);
    }

    public ProductoServicioPK getProductoServicioPK() {
        return productoServicioPK;
    }

    public void setProductoServicioPK(ProductoServicioPK productoServicioPK) {
        this.productoServicioPK = productoServicioPK;
    }

    @XmlTransient
    public Collection<Excedente> getExcedenteCollection() {
        return excedenteCollection;
    }

    public void setExcedenteCollection(Collection<Excedente> excedenteCollection) {
        this.excedenteCollection = excedenteCollection;
    }

    @XmlTransient
    public Collection<MontoMinimo> getMontoMinimoCollection() {
        return montoMinimoCollection;
    }

    public void setMontoMinimoCollection(Collection<MontoMinimo> montoMinimoCollection) {
        this.montoMinimoCollection = montoMinimoCollection;
    }

    @XmlTransient
    public Collection<Tarifa> getTarifaCollection() {
        return tarifaCollection;
    }

    public void setTarifaCollection(Collection<Tarifa> tarifaCollection) {
        this.tarifaCollection = tarifaCollection;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @XmlTransient
    public Collection<Contrato> getContratoCollection() {
        return contratoCollection;
    }

    public void setContratoCollection(Collection<Contrato> contratoCollection) {
        this.contratoCollection = contratoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoServicioPK != null ? productoServicioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoServicio)) {
            return false;
        }
        ProductoServicio other = (ProductoServicio) object;
        if ((this.productoServicioPK == null && other.productoServicioPK != null) || (this.productoServicioPK != null && !this.productoServicioPK.equals(other.productoServicioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ProductoServicio[ productoServicioPK=" + productoServicioPK + " ]";
    }
    
}

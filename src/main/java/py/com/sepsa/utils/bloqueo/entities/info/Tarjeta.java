/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tarjeta", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarjeta.findAll", query = "SELECT t FROM Tarjeta t"),
    @NamedQuery(name = "Tarjeta.findByNroTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.tarjetaPK.nroTarjeta = :nroTarjeta"),
    @NamedQuery(name = "Tarjeta.findByIdPersona", query = "SELECT t FROM Tarjeta t WHERE t.tarjetaPK.idPersona = :idPersona"),
    @NamedQuery(name = "Tarjeta.findByPin", query = "SELECT t FROM Tarjeta t WHERE t.pin = :pin"),
    @NamedQuery(name = "Tarjeta.findByEstado", query = "SELECT t FROM Tarjeta t WHERE t.estado = :estado"),
    @NamedQuery(name = "Tarjeta.findByPrincipal", query = "SELECT t FROM Tarjeta t WHERE t.principal = :principal"),
    @NamedQuery(name = "Tarjeta.findByVencimiento", query = "SELECT t FROM Tarjeta t WHERE t.vencimiento = :vencimiento")})
public class Tarjeta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TarjetaPK tarjetaPK;
    @Basic(optional = false)
    @Column(name = "pin")
    private String pin;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "principal")
    private Character principal;
    @Basic(optional = false)
    @Column(name = "vencimiento")
    private String vencimiento;
    @JoinColumn(name = "id_cuenta", referencedColumnName = "id")
    @ManyToOne
    private Cuenta idCuenta;
    @JoinColumn(name = "id_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;

    public Tarjeta() {
    }

    public Tarjeta(TarjetaPK tarjetaPK) {
        this.tarjetaPK = tarjetaPK;
    }

    public Tarjeta(TarjetaPK tarjetaPK, String pin, Character estado, Character principal, String vencimiento) {
        this.tarjetaPK = tarjetaPK;
        this.pin = pin;
        this.estado = estado;
        this.principal = principal;
        this.vencimiento = vencimiento;
    }

    public Tarjeta(String nroTarjeta, int idPersona) {
        this.tarjetaPK = new TarjetaPK(nroTarjeta, idPersona);
    }

    public TarjetaPK getTarjetaPK() {
        return tarjetaPK;
    }

    public void setTarjetaPK(TarjetaPK tarjetaPK) {
        this.tarjetaPK = tarjetaPK;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public String getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    public Cuenta getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Cuenta idCuenta) {
        this.idCuenta = idCuenta;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tarjetaPK != null ? tarjetaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarjeta)) {
            return false;
        }
        Tarjeta other = (Tarjeta) object;
        if ((this.tarjetaPK == null && other.tarjetaPK != null) || (this.tarjetaPK != null && !this.tarjetaPK.equals(other.tarjetaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Tarjeta[ tarjetaPK=" + tarjetaPK + " ]";
    }
    
}

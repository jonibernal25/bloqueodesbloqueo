/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contrato_tarifa", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContratoTarifa.findAll", query = "SELECT c FROM ContratoTarifa c"),
    @NamedQuery(name = "ContratoTarifa.findByIdContrato", query = "SELECT c FROM ContratoTarifa c WHERE c.contratoTarifaPK.idContrato = :idContrato"),
    @NamedQuery(name = "ContratoTarifa.findByIdTarifa", query = "SELECT c FROM ContratoTarifa c WHERE c.contratoTarifaPK.idTarifa = :idTarifa")})
public class ContratoTarifa implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContratoTarifaPK contratoTarifaPK;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contrato contrato;
    @JoinColumn(name = "id_tarifa", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tarifa tarifa;
    @JoinColumn(name = "id_tipo_negocio", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoNegocio idTipoNegocio;

    public ContratoTarifa() {
    }

    public ContratoTarifa(ContratoTarifaPK contratoTarifaPK) {
        this.contratoTarifaPK = contratoTarifaPK;
    }

    public ContratoTarifa(int idContrato, int idTarifa) {
        this.contratoTarifaPK = new ContratoTarifaPK(idContrato, idTarifa);
    }

    public ContratoTarifaPK getContratoTarifaPK() {
        return contratoTarifaPK;
    }

    public void setContratoTarifaPK(ContratoTarifaPK contratoTarifaPK) {
        this.contratoTarifaPK = contratoTarifaPK;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Tarifa getTarifa() {
        return tarifa;
    }

    public void setTarifa(Tarifa tarifa) {
        this.tarifa = tarifa;
    }

    public TipoNegocio getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public void setIdTipoNegocio(TipoNegocio idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contratoTarifaPK != null ? contratoTarifaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoTarifa)) {
            return false;
        }
        ContratoTarifa other = (ContratoTarifa) object;
        if ((this.contratoTarifaPK == null && other.contratoTarifaPK != null) || (this.contratoTarifaPK != null && !this.contratoTarifaPK.equals(other.contratoTarifaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ContratoTarifa[ contratoTarifaPK=" + contratoTarifaPK + " ]";
    }
    
}

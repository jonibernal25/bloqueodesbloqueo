/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "configuracion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Configuracion.findAll", query = "SELECT c FROM Configuracion c"),
    @NamedQuery(name = "Configuracion.findById", query = "SELECT c FROM Configuracion c WHERE c.id = :id"),
    @NamedQuery(name = "Configuracion.findByFechaAlta", query = "SELECT c FROM Configuracion c WHERE c.fechaAlta = :fechaAlta"),
    @NamedQuery(name = "Configuracion.findByFechaBaja", query = "SELECT c FROM Configuracion c WHERE c.fechaBaja = :fechaBaja")})
public class Configuracion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @Column(name = "fecha_baja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    @OneToMany(mappedBy = "idConfiguracion")
    private Collection<Actualizacion> actualizacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "configuracion")
    private Collection<ConfiguracionXml> configuracionXmlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConfiguracion")
    private Collection<Instalacion> instalacionCollection;

    public Configuracion() {
    }

    public Configuracion(Integer id) {
        this.id = id;
    }

    public Configuracion(Integer id, Date fechaAlta) {
        this.id = id;
        this.fechaAlta = fechaAlta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    @XmlTransient
    public Collection<Actualizacion> getActualizacionCollection() {
        return actualizacionCollection;
    }

    public void setActualizacionCollection(Collection<Actualizacion> actualizacionCollection) {
        this.actualizacionCollection = actualizacionCollection;
    }

    @XmlTransient
    public Collection<ConfiguracionXml> getConfiguracionXmlCollection() {
        return configuracionXmlCollection;
    }

    public void setConfiguracionXmlCollection(Collection<ConfiguracionXml> configuracionXmlCollection) {
        this.configuracionXmlCollection = configuracionXmlCollection;
    }

    @XmlTransient
    public Collection<Instalacion> getInstalacionCollection() {
        return instalacionCollection;
    }

    public void setInstalacionCollection(Collection<Instalacion> instalacionCollection) {
        this.instalacionCollection = instalacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Configuracion)) {
            return false;
        }
        Configuracion other = (Configuracion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.Configuracion[ id=" + id + " ]";
    }
    
}

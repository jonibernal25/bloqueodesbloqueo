/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "retencion_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RetencionDetalle.findAll", query = "SELECT r FROM RetencionDetalle r"),
    @NamedQuery(name = "RetencionDetalle.findByIdFactura", query = "SELECT r FROM RetencionDetalle r WHERE r.retencionDetallePK.idFactura = :idFactura"),
    @NamedQuery(name = "RetencionDetalle.findByIdRetencion", query = "SELECT r FROM RetencionDetalle r WHERE r.retencionDetallePK.idRetencion = :idRetencion"),
    @NamedQuery(name = "RetencionDetalle.findByMontoImponible", query = "SELECT r FROM RetencionDetalle r WHERE r.montoImponible = :montoImponible"),
    @NamedQuery(name = "RetencionDetalle.findByMontoIva", query = "SELECT r FROM RetencionDetalle r WHERE r.montoIva = :montoIva"),
    @NamedQuery(name = "RetencionDetalle.findByMontoTotal", query = "SELECT r FROM RetencionDetalle r WHERE r.montoTotal = :montoTotal"),
    @NamedQuery(name = "RetencionDetalle.findByMontoRetenido", query = "SELECT r FROM RetencionDetalle r WHERE r.montoRetenido = :montoRetenido")})
public class RetencionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RetencionDetallePK retencionDetallePK;
    @Basic(optional = false)
    @Column(name = "monto_imponible")
    private BigInteger montoImponible;
    @Basic(optional = false)
    @Column(name = "monto_iva")
    private BigInteger montoIva;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private BigInteger montoTotal;
    @Basic(optional = false)
    @Column(name = "monto_retenido")
    private BigInteger montoRetenido;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "id_retencion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Retencion retencion;

    public RetencionDetalle() {
    }

    public RetencionDetalle(RetencionDetallePK retencionDetallePK) {
        this.retencionDetallePK = retencionDetallePK;
    }

    public RetencionDetalle(RetencionDetallePK retencionDetallePK, BigInteger montoImponible, BigInteger montoIva, BigInteger montoTotal, BigInteger montoRetenido) {
        this.retencionDetallePK = retencionDetallePK;
        this.montoImponible = montoImponible;
        this.montoIva = montoIva;
        this.montoTotal = montoTotal;
        this.montoRetenido = montoRetenido;
    }

    public RetencionDetalle(int idFactura, int idRetencion) {
        this.retencionDetallePK = new RetencionDetallePK(idFactura, idRetencion);
    }

    public RetencionDetallePK getRetencionDetallePK() {
        return retencionDetallePK;
    }

    public void setRetencionDetallePK(RetencionDetallePK retencionDetallePK) {
        this.retencionDetallePK = retencionDetallePK;
    }

    public BigInteger getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigInteger montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigInteger getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigInteger montoIva) {
        this.montoIva = montoIva;
    }

    public BigInteger getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigInteger montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigInteger getMontoRetenido() {
        return montoRetenido;
    }

    public void setMontoRetenido(BigInteger montoRetenido) {
        this.montoRetenido = montoRetenido;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Retencion getRetencion() {
        return retencion;
    }

    public void setRetencion(Retencion retencion) {
        this.retencion = retencion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (retencionDetallePK != null ? retencionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RetencionDetalle)) {
            return false;
        }
        RetencionDetalle other = (RetencionDetalle) object;
        if ((this.retencionDetallePK == null && other.retencionDetallePK != null) || (this.retencionDetallePK != null && !this.retencionDetallePK.equals(other.retencionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.RetencionDetalle[ retencionDetallePK=" + retencionDetallePK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.mail.MessagingException;
import py.com.sepsa.utils.bloqueo.entities.comercial.Producto;
import py.com.sepsa.utils.bloqueo.entities.facturacion.Factura;
import py.com.sepsa.utils.bloqueo.entities.liquidacion.LiquidacionProducto;
import py.com.sepsa.utils.bloqueo.entities.trans.Usuario;
import py.com.sepsa.utils.bloqueo.utils.BdUtils;

/**
 * Clase para el bloqueo de usuarios
 * @author Jonathan D. Bernal Fernández
 */
public class Bloqueo {
    
    /**
     * Manejador de base de datos
     */
    private BdUtils bdUtils;
    
    /**
     * Lista de productos
     */
    private List<String> productos;
    
    /**
     * Lista de mails
     */
    private final String mails;
    
    /**
     * Procesa el bloqueo de usuarios
     * @throws java.io.IOException
     * @throws javax.mail.MessagingException
     */
    public void procesarBloqueos() throws IOException, MessagingException {
        
        for (String producto : productos) {
            
            Producto prod = bdUtils.findProducto(producto);
            
            if(prod == null) {
                continue;
            }
            
            List<Factura> facturas = bdUtils.getFacturasPendientes(null);
        
            List<Factura> bloqueados = new ArrayList<>();
            
            for (Factura factura : facturas) {
                
                LiquidacionProducto lp = getLiquidacionProducto(
                        factura.getIdCliente(), prod.getId());
                
                if(lp == null) {
                    continue;
                }
                
                Calendar now = Calendar.getInstance();
                Calendar fact = Calendar.getInstance();
                fact.setTime(factura.getFecha());
                fact.add(Calendar.DAY_OF_MONTH, lp.getPlazo());
                fact.add(Calendar.DAY_OF_MONTH, lp.getExtension());
                
                if(now.compareTo(fact) > 0) {
                    
                    List<Usuario> usuarios =
                            realizarBloqueo(factura.getIdCliente());
                    
                    if(usuarios != null && !usuarios.isEmpty()) {
                        bloqueados.add(factura);
                    }
                }
            }
            
            ReportGenerator.generarReporteBloqueo(bdUtils, prod, bloqueados, mails);
        }
    }

    /**
     * Realiza el bloqueo de los usuarios de un cliente
     * @param idCliente Identificador del cliente
     */
    private List<Usuario> realizarBloqueo(Integer idCliente) {
        
        if(idCliente == null) {
            return new ArrayList<>();
        }
        
        List<Usuario> usuarios = bdUtils.findUsuario(idCliente, 'A');
        
        for (Usuario usuario : usuarios) {
            usuario.setEstado('B');
            bdUtils.edit(usuario);
        }
        
        return usuarios;
    }
    
    /**
     * Obtiene una liquidación de producto
     * @param idCliente Identificador de cliente
     * @param idProducto Identificador de producto
     * @return Liquidación de producto
     */
    private LiquidacionProducto getLiquidacionProducto(Integer idCliente,
            Integer idProducto) {
        
        if(idProducto == null) {
            return null;
        }
        
        LiquidacionProducto result = bdUtils.findLiquidacionProducto(idProducto,
                idCliente);
        
        if(result != null) {
            return result;
        }
        
        result = bdUtils.findLiquidacionProducto(idProducto, null);
        
        return result;
    }
    
    /**
     * Constructor
     * @param bdUtils Manejador de base de datos
     * @param productos Lista de productos
     * @param mails Lista de mails
     */
    public Bloqueo(BdUtils bdUtils, List<String> productos, String mails) {
        this.bdUtils = bdUtils;
        this.productos = productos;
        this.mails = mails;
    }
}

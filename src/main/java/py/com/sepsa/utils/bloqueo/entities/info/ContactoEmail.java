/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contacto_email", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactoEmail.findAll", query = "SELECT c FROM ContactoEmail c"),
    @NamedQuery(name = "ContactoEmail.findByIdEmail", query = "SELECT c FROM ContactoEmail c WHERE c.contactoEmailPK.idEmail = :idEmail"),
    @NamedQuery(name = "ContactoEmail.findByIdContacto", query = "SELECT c FROM ContactoEmail c WHERE c.contactoEmailPK.idContacto = :idContacto"),
    @NamedQuery(name = "ContactoEmail.findByNotificacionDocumento", query = "SELECT c FROM ContactoEmail c WHERE c.notificacionDocumento = :notificacionDocumento"),
    @NamedQuery(name = "ContactoEmail.findByNotificacionFactura", query = "SELECT c FROM ContactoEmail c WHERE c.notificacionFactura = :notificacionFactura"),
    @NamedQuery(name = "ContactoEmail.findByNotificacionNotaCredito", query = "SELECT c FROM ContactoEmail c WHERE c.notificacionNotaCredito = :notificacionNotaCredito"),
    @NamedQuery(name = "ContactoEmail.findByNotificacionFactoring", query = "SELECT c FROM ContactoEmail c WHERE c.notificacionFactoring = :notificacionFactoring"),
    @NamedQuery(name = "ContactoEmail.findByNotificacionRecibo", query = "SELECT c FROM ContactoEmail c WHERE c.notificacionRecibo = :notificacionRecibo")})
public class ContactoEmail implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContactoEmailPK contactoEmailPK;
    @Basic(optional = false)
    @Column(name = "notificacion_documento")
    private Character notificacionDocumento;
    @Basic(optional = false)
    @Column(name = "notificacion_factura")
    private Character notificacionFactura;
    @Basic(optional = false)
    @Column(name = "notificacion_nota_credito")
    private Character notificacionNotaCredito;
    @Basic(optional = false)
    @Column(name = "notificacion_factoring")
    private Character notificacionFactoring;
    @Basic(optional = false)
    @Column(name = "notificacion_recibo")
    private Character notificacionRecibo;
    @JoinColumn(name = "id_contacto", referencedColumnName = "id_contacto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contacto contacto;
    @JoinColumn(name = "id_email", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Email email;

    public ContactoEmail() {
    }

    public ContactoEmail(ContactoEmailPK contactoEmailPK) {
        this.contactoEmailPK = contactoEmailPK;
    }

    public ContactoEmail(ContactoEmailPK contactoEmailPK, Character notificacionDocumento, Character notificacionFactura, Character notificacionNotaCredito, Character notificacionFactoring, Character notificacionRecibo) {
        this.contactoEmailPK = contactoEmailPK;
        this.notificacionDocumento = notificacionDocumento;
        this.notificacionFactura = notificacionFactura;
        this.notificacionNotaCredito = notificacionNotaCredito;
        this.notificacionFactoring = notificacionFactoring;
        this.notificacionRecibo = notificacionRecibo;
    }

    public ContactoEmail(int idEmail, int idContacto) {
        this.contactoEmailPK = new ContactoEmailPK(idEmail, idContacto);
    }

    public ContactoEmailPK getContactoEmailPK() {
        return contactoEmailPK;
    }

    public void setContactoEmailPK(ContactoEmailPK contactoEmailPK) {
        this.contactoEmailPK = contactoEmailPK;
    }

    public Character getNotificacionDocumento() {
        return notificacionDocumento;
    }

    public void setNotificacionDocumento(Character notificacionDocumento) {
        this.notificacionDocumento = notificacionDocumento;
    }

    public Character getNotificacionFactura() {
        return notificacionFactura;
    }

    public void setNotificacionFactura(Character notificacionFactura) {
        this.notificacionFactura = notificacionFactura;
    }

    public Character getNotificacionNotaCredito() {
        return notificacionNotaCredito;
    }

    public void setNotificacionNotaCredito(Character notificacionNotaCredito) {
        this.notificacionNotaCredito = notificacionNotaCredito;
    }

    public Character getNotificacionFactoring() {
        return notificacionFactoring;
    }

    public void setNotificacionFactoring(Character notificacionFactoring) {
        this.notificacionFactoring = notificacionFactoring;
    }

    public Character getNotificacionRecibo() {
        return notificacionRecibo;
    }

    public void setNotificacionRecibo(Character notificacionRecibo) {
        this.notificacionRecibo = notificacionRecibo;
    }

    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactoEmailPK != null ? contactoEmailPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactoEmail)) {
            return false;
        }
        ContactoEmail other = (ContactoEmail) object;
        if ((this.contactoEmailPK == null && other.contactoEmailPK != null) || (this.contactoEmailPK != null && !this.contactoEmailPK.equals(other.contactoEmailPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.ContactoEmail[ contactoEmailPK=" + contactoEmailPK + " ]";
    }
    
}

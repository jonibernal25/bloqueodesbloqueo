/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cheque", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cheque.findAll", query = "SELECT c FROM Cheque c"),
    @NamedQuery(name = "Cheque.findById", query = "SELECT c FROM Cheque c WHERE c.id = :id"),
    @NamedQuery(name = "Cheque.findByIdEntidadFinanciera", query = "SELECT c FROM Cheque c WHERE c.idEntidadFinanciera = :idEntidadFinanciera"),
    @NamedQuery(name = "Cheque.findByNroCheque", query = "SELECT c FROM Cheque c WHERE c.nroCheque = :nroCheque"),
    @NamedQuery(name = "Cheque.findByFechaEmision", query = "SELECT c FROM Cheque c WHERE c.fechaEmision = :fechaEmision"),
    @NamedQuery(name = "Cheque.findByFechaPago", query = "SELECT c FROM Cheque c WHERE c.fechaPago = :fechaPago"),
    @NamedQuery(name = "Cheque.findByMontoCheque", query = "SELECT c FROM Cheque c WHERE c.montoCheque = :montoCheque")})
public class Cheque implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;
    @Basic(optional = false)
    @Column(name = "nro_cheque")
    private String nroCheque;
    @Basic(optional = false)
    @Column(name = "fecha_emision")
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;
    @Basic(optional = false)
    @Column(name = "monto_cheque")
    private BigInteger montoCheque;
    @OneToMany(mappedBy = "idCheque")
    private Collection<CobroDetalle> cobroDetalleCollection;
    @JoinColumn(name = "id_deposito", referencedColumnName = "id")
    @ManyToOne
    private Deposito idDeposito;

    public Cheque() {
    }

    public Cheque(Integer id) {
        this.id = id;
    }

    public Cheque(Integer id, String nroCheque, Date fechaEmision, BigInteger montoCheque) {
        this.id = id;
        this.nroCheque = nroCheque;
        this.fechaEmision = fechaEmision;
        this.montoCheque = montoCheque;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public String getNroCheque() {
        return nroCheque;
    }

    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigInteger getMontoCheque() {
        return montoCheque;
    }

    public void setMontoCheque(BigInteger montoCheque) {
        this.montoCheque = montoCheque;
    }

    @XmlTransient
    public Collection<CobroDetalle> getCobroDetalleCollection() {
        return cobroDetalleCollection;
    }

    public void setCobroDetalleCollection(Collection<CobroDetalle> cobroDetalleCollection) {
        this.cobroDetalleCollection = cobroDetalleCollection;
    }

    public Deposito getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Deposito idDeposito) {
        this.idDeposito = idDeposito;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cheque)) {
            return false;
        }
        Cheque other = (Cheque) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Cheque[ id=" + id + " ]";
    }
    
}

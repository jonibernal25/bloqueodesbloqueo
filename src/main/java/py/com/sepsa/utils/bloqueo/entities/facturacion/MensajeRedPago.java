/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "mensaje_red_pago", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MensajeRedPago.findAll", query = "SELECT m FROM MensajeRedPago m"),
    @NamedQuery(name = "MensajeRedPago.findById", query = "SELECT m FROM MensajeRedPago m WHERE m.id = :id"),
    @NamedQuery(name = "MensajeRedPago.findByDescripcion", query = "SELECT m FROM MensajeRedPago m WHERE m.descripcion = :descripcion")})
public class MensajeRedPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idMensaje")
    private Collection<TransaccionRedPago> transaccionRedPagoCollection;

    public MensajeRedPago() {
    }

    public MensajeRedPago(Integer id) {
        this.id = id;
    }

    public MensajeRedPago(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TransaccionRedPago> getTransaccionRedPagoCollection() {
        return transaccionRedPagoCollection;
    }

    public void setTransaccionRedPagoCollection(Collection<TransaccionRedPago> transaccionRedPagoCollection) {
        this.transaccionRedPagoCollection = transaccionRedPagoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MensajeRedPago)) {
            return false;
        }
        MensajeRedPago other = (MensajeRedPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.MensajeRedPago[ id=" + id + " ]";
    }
    
}

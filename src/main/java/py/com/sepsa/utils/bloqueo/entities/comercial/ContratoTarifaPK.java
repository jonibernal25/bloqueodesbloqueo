/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ContratoTarifaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_contrato")
    private int idContrato;
    @Basic(optional = false)
    @Column(name = "id_tarifa")
    private int idTarifa;

    public ContratoTarifaPK() {
    }

    public ContratoTarifaPK(int idContrato, int idTarifa) {
        this.idContrato = idContrato;
        this.idTarifa = idTarifa;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }

    public int getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(int idTarifa) {
        this.idTarifa = idTarifa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idContrato;
        hash += (int) idTarifa;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoTarifaPK)) {
            return false;
        }
        ContratoTarifaPK other = (ContratoTarifaPK) object;
        if (this.idContrato != other.idContrato) {
            return false;
        }
        if (this.idTarifa != other.idTarifa) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ContratoTarifaPK[ idContrato=" + idContrato + ", idTarifa=" + idTarifa + " ]";
    }
    
}

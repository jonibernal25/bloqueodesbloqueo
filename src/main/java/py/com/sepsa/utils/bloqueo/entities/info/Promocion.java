/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "promocion", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promocion.findAll", query = "SELECT p FROM Promocion p"),
    @NamedQuery(name = "Promocion.findById", query = "SELECT p FROM Promocion p WHERE p.id = :id"),
    @NamedQuery(name = "Promocion.findByFechaInicio", query = "SELECT p FROM Promocion p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Promocion.findByFechaFin", query = "SELECT p FROM Promocion p WHERE p.fechaFin = :fechaFin"),
    @NamedQuery(name = "Promocion.findByPorcentajeDescuento", query = "SELECT p FROM Promocion p WHERE p.porcentajeDescuento = :porcentajeDescuento"),
    @NamedQuery(name = "Promocion.findByPrecioPromocion", query = "SELECT p FROM Promocion p WHERE p.precioPromocion = :precioPromocion"),
    @NamedQuery(name = "Promocion.findByCantidadMinima", query = "SELECT p FROM Promocion p WHERE p.cantidadMinima = :cantidadMinima"),
    @NamedQuery(name = "Promocion.findByCantidadPromocion", query = "SELECT p FROM Promocion p WHERE p.cantidadPromocion = :cantidadPromocion"),
    @NamedQuery(name = "Promocion.findByEstado", query = "SELECT p FROM Promocion p WHERE p.estado = :estado")})
public class Promocion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "porcentaje_descuento")
    private int porcentajeDescuento;
    @Basic(optional = false)
    @Column(name = "precio_promocion")
    private BigInteger precioPromocion;
    @Basic(optional = false)
    @Column(name = "cantidad_minima")
    private BigInteger cantidadMinima;
    @Basic(optional = false)
    @Column(name = "cantidad_promocion")
    private BigInteger cantidadPromocion;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_tipo_promocion", referencedColumnName = "id")
    @ManyToOne
    private TipoPromocion idTipoPromocion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "promocion")
    private Collection<CompradorPromocion> compradorPromocionCollection;

    public Promocion() {
    }

    public Promocion(Integer id) {
        this.id = id;
    }

    public Promocion(Integer id, int porcentajeDescuento, BigInteger precioPromocion, BigInteger cantidadMinima, BigInteger cantidadPromocion, Character estado) {
        this.id = id;
        this.porcentajeDescuento = porcentajeDescuento;
        this.precioPromocion = precioPromocion;
        this.cantidadMinima = cantidadMinima;
        this.cantidadPromocion = cantidadPromocion;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(int porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public BigInteger getPrecioPromocion() {
        return precioPromocion;
    }

    public void setPrecioPromocion(BigInteger precioPromocion) {
        this.precioPromocion = precioPromocion;
    }

    public BigInteger getCantidadMinima() {
        return cantidadMinima;
    }

    public void setCantidadMinima(BigInteger cantidadMinima) {
        this.cantidadMinima = cantidadMinima;
    }

    public BigInteger getCantidadPromocion() {
        return cantidadPromocion;
    }

    public void setCantidadPromocion(BigInteger cantidadPromocion) {
        this.cantidadPromocion = cantidadPromocion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public TipoPromocion getIdTipoPromocion() {
        return idTipoPromocion;
    }

    public void setIdTipoPromocion(TipoPromocion idTipoPromocion) {
        this.idTipoPromocion = idTipoPromocion;
    }

    @XmlTransient
    public Collection<CompradorPromocion> getCompradorPromocionCollection() {
        return compradorPromocionCollection;
    }

    public void setCompradorPromocionCollection(Collection<CompradorPromocion> compradorPromocionCollection) {
        this.compradorPromocionCollection = compradorPromocionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promocion)) {
            return false;
        }
        Promocion other = (Promocion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Promocion[ id=" + id + " ]";
    }
    
}

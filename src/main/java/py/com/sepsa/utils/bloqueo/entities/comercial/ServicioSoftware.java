/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "servicio_software", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicioSoftware.findAll", query = "SELECT s FROM ServicioSoftware s"),
    @NamedQuery(name = "ServicioSoftware.findByIdServicio", query = "SELECT s FROM ServicioSoftware s WHERE s.servicioSoftwarePK.idServicio = :idServicio"),
    @NamedQuery(name = "ServicioSoftware.findByIdSoftware", query = "SELECT s FROM ServicioSoftware s WHERE s.servicioSoftwarePK.idSoftware = :idSoftware")})
public class ServicioSoftware implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ServicioSoftwarePK servicioSoftwarePK;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicio servicio;

    public ServicioSoftware() {
    }

    public ServicioSoftware(ServicioSoftwarePK servicioSoftwarePK) {
        this.servicioSoftwarePK = servicioSoftwarePK;
    }

    public ServicioSoftware(int idServicio, int idSoftware) {
        this.servicioSoftwarePK = new ServicioSoftwarePK(idServicio, idSoftware);
    }

    public ServicioSoftwarePK getServicioSoftwarePK() {
        return servicioSoftwarePK;
    }

    public void setServicioSoftwarePK(ServicioSoftwarePK servicioSoftwarePK) {
        this.servicioSoftwarePK = servicioSoftwarePK;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (servicioSoftwarePK != null ? servicioSoftwarePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicioSoftware)) {
            return false;
        }
        ServicioSoftware other = (ServicioSoftware) object;
        if ((this.servicioSoftwarePK == null && other.servicioSoftwarePK != null) || (this.servicioSoftwarePK != null && !this.servicioSoftwarePK.equals(other.servicioSoftwarePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ServicioSoftware[ servicioSoftwarePK=" + servicioSoftwarePK + " ]";
    }
    
}

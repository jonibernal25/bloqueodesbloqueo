/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tasa_cambio", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TasaCambio.findAll", query = "SELECT t FROM TasaCambio t"),
    @NamedQuery(name = "TasaCambio.findByIdMoneda", query = "SELECT t FROM TasaCambio t WHERE t.tasaCambioPK.idMoneda = :idMoneda"),
    @NamedQuery(name = "TasaCambio.findByFecha", query = "SELECT t FROM TasaCambio t WHERE t.tasaCambioPK.fecha = :fecha"),
    @NamedQuery(name = "TasaCambio.findByValorCompra", query = "SELECT t FROM TasaCambio t WHERE t.valorCompra = :valorCompra"),
    @NamedQuery(name = "TasaCambio.findByValorVenta", query = "SELECT t FROM TasaCambio t WHERE t.valorVenta = :valorVenta")})
public class TasaCambio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TasaCambioPK tasaCambioPK;
    @Basic(optional = false)
    @Column(name = "valor_compra")
    private BigInteger valorCompra;
    @Basic(optional = false)
    @Column(name = "valor_venta")
    private BigInteger valorVenta;
    @JoinColumn(name = "id_moneda", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Moneda moneda;

    public TasaCambio() {
    }

    public TasaCambio(TasaCambioPK tasaCambioPK) {
        this.tasaCambioPK = tasaCambioPK;
    }

    public TasaCambio(TasaCambioPK tasaCambioPK, BigInteger valorCompra, BigInteger valorVenta) {
        this.tasaCambioPK = tasaCambioPK;
        this.valorCompra = valorCompra;
        this.valorVenta = valorVenta;
    }

    public TasaCambio(int idMoneda, Date fecha) {
        this.tasaCambioPK = new TasaCambioPK(idMoneda, fecha);
    }

    public TasaCambioPK getTasaCambioPK() {
        return tasaCambioPK;
    }

    public void setTasaCambioPK(TasaCambioPK tasaCambioPK) {
        this.tasaCambioPK = tasaCambioPK;
    }

    public BigInteger getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(BigInteger valorCompra) {
        this.valorCompra = valorCompra;
    }

    public BigInteger getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(BigInteger valorVenta) {
        this.valorVenta = valorVenta;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tasaCambioPK != null ? tasaCambioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TasaCambio)) {
            return false;
        }
        TasaCambio other = (TasaCambio) object;
        if ((this.tasaCambioPK == null && other.tasaCambioPK != null) || (this.tasaCambioPK != null && !this.tasaCambioPK.equals(other.tasaCambioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.TasaCambio[ tasaCambioPK=" + tasaCambioPK + " ]";
    }
    
}

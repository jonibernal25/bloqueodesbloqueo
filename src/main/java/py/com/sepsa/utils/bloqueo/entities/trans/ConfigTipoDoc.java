/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "config_tipo_doc", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfigTipoDoc.findAll", query = "SELECT c FROM ConfigTipoDoc c"),
    @NamedQuery(name = "ConfigTipoDoc.findById", query = "SELECT c FROM ConfigTipoDoc c WHERE c.id = :id"),
    @NamedQuery(name = "ConfigTipoDoc.findByDescripcion", query = "SELECT c FROM ConfigTipoDoc c WHERE c.descripcion = :descripcion")})
public class ConfigTipoDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_tipo_dato", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoDato idTipoDato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "configTipoDoc")
    private Collection<PersonaConfigTipoDoc> personaConfigTipoDocCollection;

    public ConfigTipoDoc() {
    }

    public ConfigTipoDoc(Integer id) {
        this.id = id;
    }

    public ConfigTipoDoc(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoDato getIdTipoDato() {
        return idTipoDato;
    }

    public void setIdTipoDato(TipoDato idTipoDato) {
        this.idTipoDato = idTipoDato;
    }

    @XmlTransient
    public Collection<PersonaConfigTipoDoc> getPersonaConfigTipoDocCollection() {
        return personaConfigTipoDocCollection;
    }

    public void setPersonaConfigTipoDocCollection(Collection<PersonaConfigTipoDoc> personaConfigTipoDocCollection) {
        this.personaConfigTipoDocCollection = personaConfigTipoDocCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigTipoDoc)) {
            return false;
        }
        ConfigTipoDoc other = (ConfigTipoDoc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ConfigTipoDoc[ id=" + id + " ]";
    }
    
}

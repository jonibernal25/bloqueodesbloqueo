/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "usuario_local", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioLocal.findAll", query = "SELECT u FROM UsuarioLocal u"),
    @NamedQuery(name = "UsuarioLocal.findByIdUsuario", query = "SELECT u FROM UsuarioLocal u WHERE u.usuarioLocalPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuarioLocal.findByIdPersona", query = "SELECT u FROM UsuarioLocal u WHERE u.usuarioLocalPK.idPersona = :idPersona"),
    @NamedQuery(name = "UsuarioLocal.findByIdLocal", query = "SELECT u FROM UsuarioLocal u WHERE u.usuarioLocalPK.idLocal = :idLocal"),
    @NamedQuery(name = "UsuarioLocal.findByEstado", query = "SELECT u FROM UsuarioLocal u WHERE u.estado = :estado")})
public class UsuarioLocal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioLocalPK usuarioLocalPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public UsuarioLocal() {
    }

    public UsuarioLocal(UsuarioLocalPK usuarioLocalPK) {
        this.usuarioLocalPK = usuarioLocalPK;
    }

    public UsuarioLocal(UsuarioLocalPK usuarioLocalPK, Character estado) {
        this.usuarioLocalPK = usuarioLocalPK;
        this.estado = estado;
    }

    public UsuarioLocal(int idUsuario, int idPersona, int idLocal) {
        this.usuarioLocalPK = new UsuarioLocalPK(idUsuario, idPersona, idLocal);
    }

    public UsuarioLocalPK getUsuarioLocalPK() {
        return usuarioLocalPK;
    }

    public void setUsuarioLocalPK(UsuarioLocalPK usuarioLocalPK) {
        this.usuarioLocalPK = usuarioLocalPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioLocalPK != null ? usuarioLocalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioLocal)) {
            return false;
        }
        UsuarioLocal other = (UsuarioLocal) object;
        if ((this.usuarioLocalPK == null && other.usuarioLocalPK != null) || (this.usuarioLocalPK != null && !this.usuarioLocalPK.equals(other.usuarioLocalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.UsuarioLocal[ usuarioLocalPK=" + usuarioLocalPK + " ]";
    }
    
}

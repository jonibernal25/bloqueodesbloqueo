/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class LiquidacionDescuentoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_liquidacion")
    private int idLiquidacion;
    @Basic(optional = false)
    @Column(name = "id_descuento")
    private int idDescuento;

    public LiquidacionDescuentoPK() {
    }

    public LiquidacionDescuentoPK(int idLiquidacion, int idDescuento) {
        this.idLiquidacion = idLiquidacion;
        this.idDescuento = idDescuento;
    }

    public int getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(int idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public int getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(int idDescuento) {
        this.idDescuento = idDescuento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idLiquidacion;
        hash += (int) idDescuento;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionDescuentoPK)) {
            return false;
        }
        LiquidacionDescuentoPK other = (LiquidacionDescuentoPK) object;
        if (this.idLiquidacion != other.idLiquidacion) {
            return false;
        }
        if (this.idDescuento != other.idDescuento) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.LiquidacionDescuentoPK[ idLiquidacion=" + idLiquidacion + ", idDescuento=" + idDescuento + " ]";
    }
    
}

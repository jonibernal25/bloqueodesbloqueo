/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ParametroPersonaValorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_parametro_persona")
    private int idParametroPersona;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;

    public ParametroPersonaValorPK() {
    }

    public ParametroPersonaValorPK(int idParametroPersona, int idPersona) {
        this.idParametroPersona = idParametroPersona;
        this.idPersona = idPersona;
    }

    public int getIdParametroPersona() {
        return idParametroPersona;
    }

    public void setIdParametroPersona(int idParametroPersona) {
        this.idParametroPersona = idParametroPersona;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idParametroPersona;
        hash += (int) idPersona;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParametroPersonaValorPK)) {
            return false;
        }
        ParametroPersonaValorPK other = (ParametroPersonaValorPK) object;
        if (this.idParametroPersona != other.idParametroPersona) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ParametroPersonaValorPK[ idParametroPersona=" + idParametroPersona + ", idPersona=" + idPersona + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ExcedenteDetallePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_liquidacion")
    private int idLiquidacion;
    @Basic(optional = false)
    @Column(name = "id")
    private int id;

    public ExcedenteDetallePK() {
    }

    public ExcedenteDetallePK(int idLiquidacion, int id) {
        this.idLiquidacion = idLiquidacion;
        this.id = id;
    }

    public int getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(int idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idLiquidacion;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcedenteDetallePK)) {
            return false;
        }
        ExcedenteDetallePK other = (ExcedenteDetallePK) object;
        if (this.idLiquidacion != other.idLiquidacion) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.ExcedenteDetallePK[ idLiquidacion=" + idLiquidacion + ", id=" + id + " ]";
    }
    
}

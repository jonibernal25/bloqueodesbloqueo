/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contrato", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrato.findAll", query = "SELECT c FROM Contrato c"),
    @NamedQuery(name = "Contrato.findById", query = "SELECT c FROM Contrato c WHERE c.id = :id"),
    @NamedQuery(name = "Contrato.findByIdCuenta", query = "SELECT c FROM Contrato c WHERE c.idCuenta = :idCuenta"),
    @NamedQuery(name = "Contrato.findByNroContrato", query = "SELECT c FROM Contrato c WHERE c.nroContrato = :nroContrato"),
    @NamedQuery(name = "Contrato.findByFechaInicio", query = "SELECT c FROM Contrato c WHERE c.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Contrato.findByFechaFin", query = "SELECT c FROM Contrato c WHERE c.fechaFin = :fechaFin"),
    @NamedQuery(name = "Contrato.findByFirmado", query = "SELECT c FROM Contrato c WHERE c.firmado = :firmado"),
    @NamedQuery(name = "Contrato.findByDocumentoAval", query = "SELECT c FROM Contrato c WHERE c.documentoAval = :documentoAval"),
    @NamedQuery(name = "Contrato.findByRenovacionAutomatica", query = "SELECT c FROM Contrato c WHERE c.renovacionAutomatica = :renovacionAutomatica"),
    @NamedQuery(name = "Contrato.findByDebitoAutomatico", query = "SELECT c FROM Contrato c WHERE c.debitoAutomatico = :debitoAutomatico"),
    @NamedQuery(name = "Contrato.findByPlazoRescision", query = "SELECT c FROM Contrato c WHERE c.plazoRescision = :plazoRescision"),
    @NamedQuery(name = "Contrato.findByObservacion", query = "SELECT c FROM Contrato c WHERE c.observacion = :observacion"),
    @NamedQuery(name = "Contrato.findByEstado", query = "SELECT c FROM Contrato c WHERE c.estado = :estado"),
    @NamedQuery(name = "Contrato.findByCobroAdelantado", query = "SELECT c FROM Contrato c WHERE c.cobroAdelantado = :cobroAdelantado"),
    @NamedQuery(name = "Contrato.findByMontoAcuerdo", query = "SELECT c FROM Contrato c WHERE c.montoAcuerdo = :montoAcuerdo"),
    @NamedQuery(name = "Contrato.findByFechaVencimientoAcuerdo", query = "SELECT c FROM Contrato c WHERE c.fechaVencimientoAcuerdo = :fechaVencimientoAcuerdo")})
public class Contrato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_cuenta")
    private Integer idCuenta;
    @Basic(optional = false)
    @Column(name = "nro_contrato")
    private String nroContrato;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "firmado")
    private Character firmado;
    @Basic(optional = false)
    @Column(name = "documento_aval")
    private Character documentoAval;
    @Basic(optional = false)
    @Column(name = "renovacion_automatica")
    private Character renovacionAutomatica;
    @Basic(optional = false)
    @Column(name = "debito_automatico")
    private Character debitoAutomatico;
    @Column(name = "plazo_rescision")
    private BigInteger plazoRescision;
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "cobro_adelantado")
    private Character cobroAdelantado;
    @Column(name = "monto_acuerdo")
    private BigInteger montoAcuerdo;
    @Column(name = "fecha_vencimiento_acuerdo")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimientoAcuerdo;
    @ManyToMany(mappedBy = "contratoCollection")
    private Collection<Anexo> anexoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato")
    private Collection<FirmanteContrato> firmanteContratoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato")
    private Collection<Renovacion> renovacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idContrato")
    private Collection<Descuento> descuentoCollection;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne(optional = false)
    private Cliente idCliente;
    @JoinColumn(name = "id_monto_minimo", referencedColumnName = "id")
    @ManyToOne
    private MontoMinimo idMontoMinimo;
    @JoinColumn(name = "id_periodo_cobro", referencedColumnName = "id")
    @ManyToOne
    private PeriodoCobro idPeriodoCobro;
    @JoinColumns({
        @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio"),
        @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")})
    @ManyToOne(optional = false)
    private ProductoServicio productoServicio;
    @JoinColumn(name = "id_tipo_contrato", referencedColumnName = "id")
    @ManyToOne
    private TipoContrato idTipoContrato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contrato")
    private Collection<ContratoTarifa> contratoTarifaCollection;

    public Contrato() {
    }

    public Contrato(Integer id) {
        this.id = id;
    }

    public Contrato(Integer id, String nroContrato, Date fechaInicio, Date fechaFin, Character firmado, Character documentoAval, Character renovacionAutomatica, Character debitoAutomatico, Character estado, Character cobroAdelantado) {
        this.id = id;
        this.nroContrato = nroContrato;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.firmado = firmado;
        this.documentoAval = documentoAval;
        this.renovacionAutomatica = renovacionAutomatica;
        this.debitoAutomatico = debitoAutomatico;
        this.estado = estado;
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Character getFirmado() {
        return firmado;
    }

    public void setFirmado(Character firmado) {
        this.firmado = firmado;
    }

    public Character getDocumentoAval() {
        return documentoAval;
    }

    public void setDocumentoAval(Character documentoAval) {
        this.documentoAval = documentoAval;
    }

    public Character getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(Character renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Character getDebitoAutomatico() {
        return debitoAutomatico;
    }

    public void setDebitoAutomatico(Character debitoAutomatico) {
        this.debitoAutomatico = debitoAutomatico;
    }

    public BigInteger getPlazoRescision() {
        return plazoRescision;
    }

    public void setPlazoRescision(BigInteger plazoRescision) {
        this.plazoRescision = plazoRescision;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public BigInteger getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(BigInteger montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public Date getFechaVencimientoAcuerdo() {
        return fechaVencimientoAcuerdo;
    }

    public void setFechaVencimientoAcuerdo(Date fechaVencimientoAcuerdo) {
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }

    @XmlTransient
    public Collection<Anexo> getAnexoCollection() {
        return anexoCollection;
    }

    public void setAnexoCollection(Collection<Anexo> anexoCollection) {
        this.anexoCollection = anexoCollection;
    }

    @XmlTransient
    public Collection<FirmanteContrato> getFirmanteContratoCollection() {
        return firmanteContratoCollection;
    }

    public void setFirmanteContratoCollection(Collection<FirmanteContrato> firmanteContratoCollection) {
        this.firmanteContratoCollection = firmanteContratoCollection;
    }

    @XmlTransient
    public Collection<Renovacion> getRenovacionCollection() {
        return renovacionCollection;
    }

    public void setRenovacionCollection(Collection<Renovacion> renovacionCollection) {
        this.renovacionCollection = renovacionCollection;
    }

    @XmlTransient
    public Collection<Descuento> getDescuentoCollection() {
        return descuentoCollection;
    }

    public void setDescuentoCollection(Collection<Descuento> descuentoCollection) {
        this.descuentoCollection = descuentoCollection;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public MontoMinimo getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdMontoMinimo(MontoMinimo idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public PeriodoCobro getIdPeriodoCobro() {
        return idPeriodoCobro;
    }

    public void setIdPeriodoCobro(PeriodoCobro idPeriodoCobro) {
        this.idPeriodoCobro = idPeriodoCobro;
    }

    public ProductoServicio getProductoServicio() {
        return productoServicio;
    }

    public void setProductoServicio(ProductoServicio productoServicio) {
        this.productoServicio = productoServicio;
    }

    public TipoContrato getIdTipoContrato() {
        return idTipoContrato;
    }

    public void setIdTipoContrato(TipoContrato idTipoContrato) {
        this.idTipoContrato = idTipoContrato;
    }

    @XmlTransient
    public Collection<ContratoTarifa> getContratoTarifaCollection() {
        return contratoTarifaCollection;
    }

    public void setContratoTarifaCollection(Collection<ContratoTarifa> contratoTarifaCollection) {
        this.contratoTarifaCollection = contratoTarifaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Contrato[ id=" + id + " ]";
    }
    
}

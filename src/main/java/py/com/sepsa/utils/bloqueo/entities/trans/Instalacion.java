/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "instalacion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Instalacion.findAll", query = "SELECT i FROM Instalacion i"),
    @NamedQuery(name = "Instalacion.findById", query = "SELECT i FROM Instalacion i WHERE i.id = :id"),
    @NamedQuery(name = "Instalacion.findByPc", query = "SELECT i FROM Instalacion i WHERE i.pc = :pc"),
    @NamedQuery(name = "Instalacion.findByIp", query = "SELECT i FROM Instalacion i WHERE i.ip = :ip"),
    @NamedQuery(name = "Instalacion.findByEstado", query = "SELECT i FROM Instalacion i WHERE i.estado = :estado"),
    @NamedQuery(name = "Instalacion.findByClave", query = "SELECT i FROM Instalacion i WHERE i.clave = :clave")})
public class Instalacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "pc")
    private String pc;
    @Basic(optional = false)
    @Column(name = "ip")
    private String ip;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "clave")
    private String clave;
    @OneToMany(mappedBy = "idInstalacion")
    private Collection<Operacion> operacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInstalacion")
    private Collection<Actualizacion> actualizacionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instalacion")
    private Collection<InstalacionUsuario> instalacionUsuarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instalacion")
    private Collection<InstalacionLocal> instalacionLocalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInstalacion")
    private Collection<HistoricoClave> historicoClaveCollection;
    @JoinColumn(name = "id_algoritmo_encriptacion", referencedColumnName = "id")
    @ManyToOne
    private AlgoritmoEncriptacion idAlgoritmoEncriptacion;
    @JoinColumn(name = "id_configuracion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Configuracion idConfiguracion;
    @JoinColumns({
        @JoinColumn(name = "id_software", referencedColumnName = "id_software"),
        @JoinColumn(name = "version", referencedColumnName = "version")})
    @ManyToOne(optional = false)
    private VersionSoftware versionSoftware;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInstalacion")
    private Collection<ClaveInstalacion> claveInstalacionCollection;

    public Instalacion() {
    }

    public Instalacion(Integer id) {
        this.id = id;
    }

    public Instalacion(Integer id, String pc, String ip, Character estado) {
        this.id = id;
        this.pc = pc;
        this.ip = ip;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @XmlTransient
    public Collection<Operacion> getOperacionCollection() {
        return operacionCollection;
    }

    public void setOperacionCollection(Collection<Operacion> operacionCollection) {
        this.operacionCollection = operacionCollection;
    }

    @XmlTransient
    public Collection<Actualizacion> getActualizacionCollection() {
        return actualizacionCollection;
    }

    public void setActualizacionCollection(Collection<Actualizacion> actualizacionCollection) {
        this.actualizacionCollection = actualizacionCollection;
    }

    @XmlTransient
    public Collection<InstalacionUsuario> getInstalacionUsuarioCollection() {
        return instalacionUsuarioCollection;
    }

    public void setInstalacionUsuarioCollection(Collection<InstalacionUsuario> instalacionUsuarioCollection) {
        this.instalacionUsuarioCollection = instalacionUsuarioCollection;
    }

    @XmlTransient
    public Collection<InstalacionLocal> getInstalacionLocalCollection() {
        return instalacionLocalCollection;
    }

    public void setInstalacionLocalCollection(Collection<InstalacionLocal> instalacionLocalCollection) {
        this.instalacionLocalCollection = instalacionLocalCollection;
    }

    @XmlTransient
    public Collection<HistoricoClave> getHistoricoClaveCollection() {
        return historicoClaveCollection;
    }

    public void setHistoricoClaveCollection(Collection<HistoricoClave> historicoClaveCollection) {
        this.historicoClaveCollection = historicoClaveCollection;
    }

    public AlgoritmoEncriptacion getIdAlgoritmoEncriptacion() {
        return idAlgoritmoEncriptacion;
    }

    public void setIdAlgoritmoEncriptacion(AlgoritmoEncriptacion idAlgoritmoEncriptacion) {
        this.idAlgoritmoEncriptacion = idAlgoritmoEncriptacion;
    }

    public Configuracion getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(Configuracion idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public VersionSoftware getVersionSoftware() {
        return versionSoftware;
    }

    public void setVersionSoftware(VersionSoftware versionSoftware) {
        this.versionSoftware = versionSoftware;
    }

    @XmlTransient
    public Collection<ClaveInstalacion> getClaveInstalacionCollection() {
        return claveInstalacionCollection;
    }

    public void setClaveInstalacionCollection(Collection<ClaveInstalacion> claveInstalacionCollection) {
        this.claveInstalacionCollection = claveInstalacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instalacion)) {
            return false;
        }
        Instalacion other = (Instalacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.Instalacion[ id=" + id + " ]";
    }
    
}

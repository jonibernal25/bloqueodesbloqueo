/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "instalacion_usuario", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstalacionUsuario.findAll", query = "SELECT i FROM InstalacionUsuario i"),
    @NamedQuery(name = "InstalacionUsuario.findByIdInstalacion", query = "SELECT i FROM InstalacionUsuario i WHERE i.instalacionUsuarioPK.idInstalacion = :idInstalacion"),
    @NamedQuery(name = "InstalacionUsuario.findByIdUsuario", query = "SELECT i FROM InstalacionUsuario i WHERE i.instalacionUsuarioPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "InstalacionUsuario.findByEstado", query = "SELECT i FROM InstalacionUsuario i WHERE i.estado = :estado")})
public class InstalacionUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InstalacionUsuarioPK instalacionUsuarioPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Instalacion instalacion;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public InstalacionUsuario() {
    }

    public InstalacionUsuario(InstalacionUsuarioPK instalacionUsuarioPK) {
        this.instalacionUsuarioPK = instalacionUsuarioPK;
    }

    public InstalacionUsuario(InstalacionUsuarioPK instalacionUsuarioPK, Character estado) {
        this.instalacionUsuarioPK = instalacionUsuarioPK;
        this.estado = estado;
    }

    public InstalacionUsuario(int idInstalacion, int idUsuario) {
        this.instalacionUsuarioPK = new InstalacionUsuarioPK(idInstalacion, idUsuario);
    }

    public InstalacionUsuarioPK getInstalacionUsuarioPK() {
        return instalacionUsuarioPK;
    }

    public void setInstalacionUsuarioPK(InstalacionUsuarioPK instalacionUsuarioPK) {
        this.instalacionUsuarioPK = instalacionUsuarioPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Instalacion getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(Instalacion instalacion) {
        this.instalacion = instalacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instalacionUsuarioPK != null ? instalacionUsuarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstalacionUsuario)) {
            return false;
        }
        InstalacionUsuario other = (InstalacionUsuario) object;
        if ((this.instalacionUsuarioPK == null && other.instalacionUsuarioPK != null) || (this.instalacionUsuarioPK != null && !this.instalacionUsuarioPK.equals(other.instalacionUsuarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.InstalacionUsuario[ instalacionUsuarioPK=" + instalacionUsuarioPK + " ]";
    }
    
}

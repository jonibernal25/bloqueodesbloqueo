/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class PersonaConfigTipoDocPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @Column(name = "id_config_tipo_doc")
    private int idConfigTipoDoc;

    public PersonaConfigTipoDocPK() {
    }

    public PersonaConfigTipoDocPK(int idPersona, int idConfigTipoDoc) {
        this.idPersona = idPersona;
        this.idConfigTipoDoc = idConfigTipoDoc;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdConfigTipoDoc() {
        return idConfigTipoDoc;
    }

    public void setIdConfigTipoDoc(int idConfigTipoDoc) {
        this.idConfigTipoDoc = idConfigTipoDoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPersona;
        hash += (int) idConfigTipoDoc;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaConfigTipoDocPK)) {
            return false;
        }
        PersonaConfigTipoDocPK other = (PersonaConfigTipoDocPK) object;
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idConfigTipoDoc != other.idConfigTipoDoc) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.PersonaConfigTipoDocPK[ idPersona=" + idPersona + ", idConfigTipoDoc=" + idConfigTipoDoc + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "excedente_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExcedenteDetalle.findAll", query = "SELECT e FROM ExcedenteDetalle e"),
    @NamedQuery(name = "ExcedenteDetalle.findByIdLiquidacion", query = "SELECT e FROM ExcedenteDetalle e WHERE e.excedenteDetallePK.idLiquidacion = :idLiquidacion"),
    @NamedQuery(name = "ExcedenteDetalle.findByIdTipoDocumento", query = "SELECT e FROM ExcedenteDetalle e WHERE e.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "ExcedenteDetalle.findByCantDocExcedidos", query = "SELECT e FROM ExcedenteDetalle e WHERE e.cantDocExcedidos = :cantDocExcedidos"),
    @NamedQuery(name = "ExcedenteDetalle.findByMontoExcedente", query = "SELECT e FROM ExcedenteDetalle e WHERE e.montoExcedente = :montoExcedente"),
    @NamedQuery(name = "ExcedenteDetalle.findById", query = "SELECT e FROM ExcedenteDetalle e WHERE e.excedenteDetallePK.id = :id"),
    @NamedQuery(name = "ExcedenteDetalle.findByIdExcedente", query = "SELECT e FROM ExcedenteDetalle e WHERE e.idExcedente = :idExcedente")})
public class ExcedenteDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExcedenteDetallePK excedenteDetallePK;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "cant_doc_excedidos")
    private int cantDocExcedidos;
    @Basic(optional = false)
    @Column(name = "monto_excedente")
    private BigInteger montoExcedente;
    @Column(name = "id_excedente")
    private Integer idExcedente;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Liquidacion liquidacion;

    public ExcedenteDetalle() {
    }

    public ExcedenteDetalle(ExcedenteDetallePK excedenteDetallePK) {
        this.excedenteDetallePK = excedenteDetallePK;
    }

    public ExcedenteDetalle(ExcedenteDetallePK excedenteDetallePK, int cantDocExcedidos, BigInteger montoExcedente) {
        this.excedenteDetallePK = excedenteDetallePK;
        this.cantDocExcedidos = cantDocExcedidos;
        this.montoExcedente = montoExcedente;
    }

    public ExcedenteDetalle(int idLiquidacion, int id) {
        this.excedenteDetallePK = new ExcedenteDetallePK(idLiquidacion, id);
    }

    public ExcedenteDetallePK getExcedenteDetallePK() {
        return excedenteDetallePK;
    }

    public void setExcedenteDetallePK(ExcedenteDetallePK excedenteDetallePK) {
        this.excedenteDetallePK = excedenteDetallePK;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getCantDocExcedidos() {
        return cantDocExcedidos;
    }

    public void setCantDocExcedidos(int cantDocExcedidos) {
        this.cantDocExcedidos = cantDocExcedidos;
    }

    public BigInteger getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(BigInteger montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public Integer getIdExcedente() {
        return idExcedente;
    }

    public void setIdExcedente(Integer idExcedente) {
        this.idExcedente = idExcedente;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (excedenteDetallePK != null ? excedenteDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcedenteDetalle)) {
            return false;
        }
        ExcedenteDetalle other = (ExcedenteDetalle) object;
        if ((this.excedenteDetallePK == null && other.excedenteDetallePK != null) || (this.excedenteDetallePK != null && !this.excedenteDetallePK.equals(other.excedenteDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.ExcedenteDetalle[ excedenteDetallePK=" + excedenteDetallePK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cobro_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CobroDetalle.findAll", query = "SELECT c FROM CobroDetalle c"),
    @NamedQuery(name = "CobroDetalle.findByIdCobro", query = "SELECT c FROM CobroDetalle c WHERE c.cobroDetallePK.idCobro = :idCobro"),
    @NamedQuery(name = "CobroDetalle.findByIdFactura", query = "SELECT c FROM CobroDetalle c WHERE c.cobroDetallePK.idFactura = :idFactura"),
    @NamedQuery(name = "CobroDetalle.findByIdCuenta", query = "SELECT c FROM CobroDetalle c WHERE c.idCuenta = :idCuenta"),
    @NamedQuery(name = "CobroDetalle.findByMontoCobro", query = "SELECT c FROM CobroDetalle c WHERE c.montoCobro = :montoCobro"),
    @NamedQuery(name = "CobroDetalle.findByEstado", query = "SELECT c FROM CobroDetalle c WHERE c.estado = :estado"),
    @NamedQuery(name = "CobroDetalle.findByNroLinea", query = "SELECT c FROM CobroDetalle c WHERE c.cobroDetallePK.nroLinea = :nroLinea")})
public class CobroDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CobroDetallePK cobroDetallePK;
    @Column(name = "id_cuenta")
    private Integer idCuenta;
    @Basic(optional = false)
    @Column(name = "monto_cobro")
    private BigInteger montoCobro;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "id_cobro", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cobro cobro;
    @JoinColumn(name = "id_concepto_cobro", referencedColumnName = "id")
    @ManyToOne
    private ConceptoCobro idConceptoCobro;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "id_tipo_cobro", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoCobro idTipoCobro;

    public CobroDetalle() {
    }

    public CobroDetalle(CobroDetallePK cobroDetallePK) {
        this.cobroDetallePK = cobroDetallePK;
    }

    public CobroDetalle(CobroDetallePK cobroDetallePK, BigInteger montoCobro, Character estado) {
        this.cobroDetallePK = cobroDetallePK;
        this.montoCobro = montoCobro;
        this.estado = estado;
    }

    public CobroDetalle(int idCobro, int idFactura, int nroLinea) {
        this.cobroDetallePK = new CobroDetallePK(idCobro, idFactura, nroLinea);
    }

    public CobroDetallePK getCobroDetallePK() {
        return cobroDetallePK;
    }

    public void setCobroDetallePK(CobroDetallePK cobroDetallePK) {
        this.cobroDetallePK = cobroDetallePK;
    }

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public BigInteger getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigInteger montoCobro) {
        this.montoCobro = montoCobro;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public Cobro getCobro() {
        return cobro;
    }

    public void setCobro(Cobro cobro) {
        this.cobro = cobro;
    }

    public ConceptoCobro getIdConceptoCobro() {
        return idConceptoCobro;
    }

    public void setIdConceptoCobro(ConceptoCobro idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public TipoCobro getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(TipoCobro idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cobroDetallePK != null ? cobroDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobroDetalle)) {
            return false;
        }
        CobroDetalle other = (CobroDetalle) object;
        if ((this.cobroDetallePK == null && other.cobroDetallePK != null) || (this.cobroDetallePK != null && !this.cobroDetallePK.equals(other.cobroDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.CobroDetalle[ cobroDetallePK=" + cobroDetallePK + " ]";
    }
    
}

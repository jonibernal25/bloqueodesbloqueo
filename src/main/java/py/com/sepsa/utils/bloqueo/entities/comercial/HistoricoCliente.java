/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "historico_cliente", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoCliente.findAll", query = "SELECT h FROM HistoricoCliente h"),
    @NamedQuery(name = "HistoricoCliente.findById", query = "SELECT h FROM HistoricoCliente h WHERE h.id = :id"),
    @NamedQuery(name = "HistoricoCliente.findByIdUsuario", query = "SELECT h FROM HistoricoCliente h WHERE h.idUsuario = :idUsuario"),
    @NamedQuery(name = "HistoricoCliente.findByIdOperacion", query = "SELECT h FROM HistoricoCliente h WHERE h.idOperacion = :idOperacion"),
    @NamedQuery(name = "HistoricoCliente.findByFechaOperacion", query = "SELECT h FROM HistoricoCliente h WHERE h.fechaOperacion = :fechaOperacion"),
    @NamedQuery(name = "HistoricoCliente.findByEstado", query = "SELECT h FROM HistoricoCliente h WHERE h.estado = :estado"),
    @NamedQuery(name = "HistoricoCliente.findByConfirmado", query = "SELECT h FROM HistoricoCliente h WHERE h.confirmado = :confirmado"),
    @NamedQuery(name = "HistoricoCliente.findByIdUsuarioAuth", query = "SELECT h FROM HistoricoCliente h WHERE h.idUsuarioAuth = :idUsuarioAuth"),
    @NamedQuery(name = "HistoricoCliente.findByFechaAutorizacion", query = "SELECT h FROM HistoricoCliente h WHERE h.fechaAutorizacion = :fechaAutorizacion")})
public class HistoricoCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Basic(optional = false)
    @Column(name = "id_operacion")
    private int idOperacion;
    @Basic(optional = false)
    @Column(name = "fecha_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaOperacion;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "confirmado")
    private Character confirmado;
    @Column(name = "id_usuario_auth")
    private Integer idUsuarioAuth;
    @Column(name = "fecha_autorizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAutorizacion;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne(optional = false)
    private Cliente idCliente;

    public HistoricoCliente() {
    }

    public HistoricoCliente(Integer id) {
        this.id = id;
    }

    public HistoricoCliente(Integer id, int idUsuario, int idOperacion, Date fechaOperacion, Character estado, Character confirmado) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.idOperacion = idOperacion;
        this.fechaOperacion = fechaOperacion;
        this.estado = estado;
        this.confirmado = confirmado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(int idOperacion) {
        this.idOperacion = idOperacion;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(Character confirmado) {
        this.confirmado = confirmado;
    }

    public Integer getIdUsuarioAuth() {
        return idUsuarioAuth;
    }

    public void setIdUsuarioAuth(Integer idUsuarioAuth) {
        this.idUsuarioAuth = idUsuarioAuth;
    }

    public Date getFechaAutorizacion() {
        return fechaAutorizacion;
    }

    public void setFechaAutorizacion(Date fechaAutorizacion) {
        this.fechaAutorizacion = fechaAutorizacion;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoCliente)) {
            return false;
        }
        HistoricoCliente other = (HistoricoCliente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.HistoricoCliente[ id=" + id + " ]";
    }
    
}

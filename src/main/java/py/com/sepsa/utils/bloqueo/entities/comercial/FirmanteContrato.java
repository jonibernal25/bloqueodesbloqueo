/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "firmante_contrato", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FirmanteContrato.findAll", query = "SELECT f FROM FirmanteContrato f"),
    @NamedQuery(name = "FirmanteContrato.findByIdContrato", query = "SELECT f FROM FirmanteContrato f WHERE f.firmanteContratoPK.idContrato = :idContrato"),
    @NamedQuery(name = "FirmanteContrato.findByIdFirmante", query = "SELECT f FROM FirmanteContrato f WHERE f.firmanteContratoPK.idFirmante = :idFirmante")})
public class FirmanteContrato implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FirmanteContratoPK firmanteContratoPK;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contrato contrato;

    public FirmanteContrato() {
    }

    public FirmanteContrato(FirmanteContratoPK firmanteContratoPK) {
        this.firmanteContratoPK = firmanteContratoPK;
    }

    public FirmanteContrato(int idContrato, int idFirmante) {
        this.firmanteContratoPK = new FirmanteContratoPK(idContrato, idFirmante);
    }

    public FirmanteContratoPK getFirmanteContratoPK() {
        return firmanteContratoPK;
    }

    public void setFirmanteContratoPK(FirmanteContratoPK firmanteContratoPK) {
        this.firmanteContratoPK = firmanteContratoPK;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (firmanteContratoPK != null ? firmanteContratoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FirmanteContrato)) {
            return false;
        }
        FirmanteContrato other = (FirmanteContrato) object;
        if ((this.firmanteContratoPK == null && other.firmanteContratoPK != null) || (this.firmanteContratoPK != null && !this.firmanteContratoPK.equals(other.firmanteContratoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.FirmanteContrato[ firmanteContratoPK=" + firmanteContratoPK + " ]";
    }
    
}

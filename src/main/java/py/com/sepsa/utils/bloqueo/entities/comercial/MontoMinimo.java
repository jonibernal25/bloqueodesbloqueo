/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "monto_minimo", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MontoMinimo.findAll", query = "SELECT m FROM MontoMinimo m"),
    @NamedQuery(name = "MontoMinimo.findById", query = "SELECT m FROM MontoMinimo m WHERE m.id = :id"),
    @NamedQuery(name = "MontoMinimo.findByMonto", query = "SELECT m FROM MontoMinimo m WHERE m.monto = :monto"),
    @NamedQuery(name = "MontoMinimo.findByEstado", query = "SELECT m FROM MontoMinimo m WHERE m.estado = :estado")})
public class MontoMinimo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "monto")
    private BigInteger monto;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumns({
        @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio"),
        @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")})
    @ManyToOne(optional = false)
    private ProductoServicio productoServicio;
    @OneToMany(mappedBy = "idMontoMinimo")
    private Collection<Contrato> contratoCollection;

    public MontoMinimo() {
    }

    public MontoMinimo(Integer id) {
        this.id = id;
    }

    public MontoMinimo(Integer id, BigInteger monto, Character estado) {
        this.id = id;
        this.monto = monto;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public ProductoServicio getProductoServicio() {
        return productoServicio;
    }

    public void setProductoServicio(ProductoServicio productoServicio) {
        this.productoServicio = productoServicio;
    }

    @XmlTransient
    public Collection<Contrato> getContratoCollection() {
        return contratoCollection;
    }

    public void setContratoCollection(Collection<Contrato> contratoCollection) {
        this.contratoCollection = contratoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MontoMinimo)) {
            return false;
        }
        MontoMinimo other = (MontoMinimo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.MontoMinimo[ id=" + id + " ]";
    }
    
}

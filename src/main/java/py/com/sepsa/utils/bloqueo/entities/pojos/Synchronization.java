/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.pojos;

import java.math.BigInteger;

/**
 * POJO para las sincronizaciones
 * @author Jonathan D. Bernal Fernández
 */
public class Synchronization {

    /**
     * Constructor
     * @param synchronizationId Identificador de sincronizacion
     * @param countryId Identificador de pais
     * @param countryCode Codigo de pais
     * @param subdivisionId Identificador de subdivision
     * @param subdivisionCode Codigo de subdivision
     * @param providerId Identificador del proveedor
     * @param providerLocalId Identificador del local del proveedor
     * @param providerGln GLN del proveedor
     * @param providerRuc RUC del proveedor
     * @param providerRazon Razon del proveedor
     * @param productGtin GTIN del producto
     * @param buyerId Identificador del comprador
     * @param buyerLocalId Identificador del local del comprador
     * @param buyerGln GLN del comprador
     * @param buyerRuc RUC del comprador
     * @param buyerRazon Razon del comprador
     * @param rdpGln GLN recipient data pool
     * @param state Estado
     */
    public Synchronization(Integer synchronizationId, Integer countryId,
            Integer countryCode, Integer subdivisionId, String subdivisionCode,
            Integer providerId, Integer providerLocalId, BigInteger providerGln, String providerRuc,
            Object providerRazon, String productGtin, Integer buyerId, Integer buyerLocalId,
            BigInteger buyerGln, String buyerRuc, String buyerRazon, BigInteger rdpGln,
            Character state) {
        this.synchronizationId = synchronizationId;
        this.countryId = countryId;
        this.countryCode = countryCode;
        this.subdivisionId = subdivisionId;
        this.subdivisionCode = subdivisionCode;
        this.providerId = providerId;
        this.providerLocalId = providerLocalId;
        this.providerGln = providerGln;
        this.providerRuc = providerRuc;
        this.providerRazon = providerRazon == null
                ? null
                : String.valueOf(providerRazon);
        this.productGtin = productGtin;
        this.buyerId = buyerId;
        this.buyerLocalId = buyerLocalId;
        this.buyerGln = buyerGln;
        this.buyerRuc = buyerRuc;
        this.buyerRazon = buyerRazon == null
                ? null
                : String.valueOf(buyerRazon);
        this.rdpGln = rdpGln;
        this.state = state;
    }
    
    /**
     * Identificador de la sincronizacion
     */
    private Integer synchronizationId;
    
    /**
     * Identificador de codigo pais
     */
    private Integer countryId;
    
    /**
     * Codigo de pais
     */
    private Integer countryCode;
    
    /**
     * Identificador de subdivision
     */
    private Integer subdivisionId;
    
    /**
     * Codigo de subdivision
     */
    private String subdivisionCode;
    
    /**
     * Identificador del proveedor
     */
    private Integer providerId;
    
    /**
     * Identificador del local del proveedor
     */
    private Integer providerLocalId;
    
    /**
     * GLN del proveedor
     */
    private BigInteger providerGln;
    
    /**
     * RUC del proveedor
     */
    private String providerRuc;
    
    /**
     * Razon del proveedor
     */
    private String providerRazon;
    
    /**
     * Código GTIN del producto
     */
    private String productGtin;
    
    /**
     * Identificador del comprador
     */
    private Integer buyerId;
    
    /**
     * Identificador del local del comprador
     */
    private Integer buyerLocalId;
    
    /**
     * GLN del comprador
     */
    private BigInteger buyerGln;
    
    /**
     * RUC del comprador
     */
    private String buyerRuc;
    
    /**
     * Razon del comprador
     */
    private String buyerRazon;
    
    /**
     * Recipient data pool
     */
    private BigInteger rdpGln;
    
    /**
     * Estado
     */
    private Character state;

    public Integer getSynchronizationId() {
        return synchronizationId;
    }

    public void setSynchronizationId(Integer synchronizationId) {
        this.synchronizationId = synchronizationId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getSubdivisionId() {
        return subdivisionId;
    }

    public void setSubdivisionId(Integer subdivisionId) {
        this.subdivisionId = subdivisionId;
    }

    public String getSubdivisionCode() {
        return subdivisionCode;
    }

    public void setSubdivisionCode(String subdivisionCode) {
        this.subdivisionCode = subdivisionCode;
    }

    public BigInteger getProviderGln() {
        return providerGln;
    }

    public void setProviderGln(BigInteger providerGln) {
        this.providerGln = providerGln;
    }

    public String getProviderRuc() {
        return providerRuc;
    }

    public void setProviderRuc(String providerRuc) {
        this.providerRuc = providerRuc;
    }

    public String getProviderRazon() {
        return providerRazon;
    }

    public void setProviderRazon(String providerRazon) {
        this.providerRazon = providerRazon;
    }

    public String getProductGtin() {
        return productGtin;
    }

    public void setProductGtin(String productGtin) {
        this.productGtin = productGtin;
    }

    public BigInteger getBuyerGln() {
        return buyerGln;
    }

    public void setBuyerGln(BigInteger buyerGln) {
        this.buyerGln = buyerGln;
    }

    public String getBuyerRuc() {
        return buyerRuc;
    }

    public void setBuyerRuc(String buyerRuc) {
        this.buyerRuc = buyerRuc;
    }

    public String getBuyerRazon() {
        return buyerRazon;
    }

    public void setBuyerRazon(String buyerRazon) {
        this.buyerRazon = buyerRazon;
    }

    public BigInteger getRdpGln() {
        return rdpGln;
    }

    public void setRdpGln(BigInteger rdpGln) {
        this.rdpGln = rdpGln;
    }

    public Character getState() {
        return state;
    }

    public void setState(Character state) {
        this.state = state;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setProviderLocalId(Integer providerLocalId) {
        this.providerLocalId = providerLocalId;
    }

    public Integer getProviderLocalId() {
        return providerLocalId;
    }

    public void setBuyerLocalId(Integer buyerLocalId) {
        this.buyerLocalId = buyerLocalId;
    }

    public Integer getBuyerLocalId() {
        return buyerLocalId;
    }
}

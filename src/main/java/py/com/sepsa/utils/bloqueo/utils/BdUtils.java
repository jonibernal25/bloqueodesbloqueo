/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.utils;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.utils.bloqueo.entities.comercial.Cliente;
import py.com.sepsa.utils.bloqueo.entities.comercial.Producto;
import py.com.sepsa.utils.bloqueo.entities.facturacion.Factura;
import py.com.sepsa.utils.bloqueo.entities.info.Local;
import py.com.sepsa.utils.bloqueo.entities.info.Persona;
import py.com.sepsa.utils.bloqueo.entities.liquidacion.LiquidacionProducto;
import py.com.sepsa.utils.bloqueo.entities.trans.Usuario;


/**
 *
 * @author Rubén Ortiz
 */
public class BdUtils {

    /**
     * Fábrica del manejador de entidades
     */
    private EntityManagerFactory emfSepsa = null;
    
    
    /**
     * Obtiene la descripcion de un local
     * @param idLocal Identificador de local
     * @param idPersona Identificador de persona
     * @return Descripcion de local
     */
    public Local findLocal(Integer idLocal, Integer idPersona) {
        
        Local result = null;
        
        if(idLocal != null && idPersona != null) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
            Root<Local> root = cq.from(Local.class);
            cq.select(root);

            cq.where(qb.and(qb.equal(root.get("localPK").get("id"), idLocal),
                    qb.equal(root.get("localPK").get("idPersona"), idPersona)));

            javax.persistence.Query q = getEntityManagerSepsa()
                    .createQuery(cq)
                    .setHint("eclipselink.refresh", "true");

            List<Local> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
            
        return result;
    }

    private EntityManager getEntityManagerSepsa() {
        if(emfSepsa == null) {
            emfSepsa = JpaUtil.getEntityManagerFactorySepsa();
        }
        return emfSepsa.createEntityManager();
    }
    
    /*
    * Constructor
     */
    public BdUtils() {}
    
    /**
     * Obtiene la lista de facturas pendientes de pago
     * @param idCliente Identificador de cliente
     * @return Lista de facturas
     */
    public List<Factura> getFacturasPendientes(Integer idCliente) {
        
        String where = "";
        
        if(idCliente != null) {
            where = String.format("%s AND f.id_cliente = %d", where, idCliente);
        }
        
        String sql = "SELECT DISTINCT ON(\"id_cliente\") *"
                + " FROM facturacion.factura f"
                + " WHERE f.anulado = 'N'"
                + " AND f.cobrado = 'N'"
                + where
                + " ORDER BY f.id_cliente, f.fecha";
        
        javax.persistence.Query q = getEntityManagerSepsa()
                .createNativeQuery(sql, Factura.class);
        
        List<Factura> facturas = q.getResultList();
        
        return facturas;
    }
    
    /**
     * Obtiene la liquidación por producto
     * @param idProducto Identificador de producto
     * @param idCliente Identificador de cliente
     * @return Liquidación por producto
     */
    public LiquidacionProducto findLiquidacionProducto(Integer idProducto,
            Integer idCliente) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<LiquidacionProducto> root = cq.from(LiquidacionProducto.class);
        
        cq.select(root);
        
        Predicate pred = qb.and();
        
        if(idProducto != null) {
            pred = qb.and(pred, qb.equal(root.get("idProducto"), idProducto));
        }
        
        if(idCliente != null) {
            pred = qb.and(pred, qb.equal(root.get("idCliente"), idCliente));
        }
        
        cq.where(pred);
        
        Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
        
        List<LiquidacionProducto> list = q.getResultList();
        
        LiquidacionProducto result = list == null || list.isEmpty()
                ? null
                : list.get(0);

        return result;
    }
    
    /**
     * Obtiene la persona
     * @param idPersona Identificador de persona
     * @return Persona
     */
    public Persona findPersona(Integer idPersona) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Persona> root = cq.from(Persona.class);
        
        cq.select(root);
        
        Predicate pred = qb.and();
        
        if(idPersona != null) {
            pred = qb.and(pred, qb.equal(root.get("id"), idPersona));
        }
        
        cq.where(pred);
        
        Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
        
        List<Persona> list = q.getResultList();
        
        Persona result = list == null || list.isEmpty()
                ? null
                : list.get(0);

        return result;
    }
    
    /**
     * Obtiene el cliente
     * @param idCliente Identificador de cliente
     * @return Persona
     */
    public Cliente findCliente(Integer idCliente) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Cliente> root = cq.from(Cliente.class);
        
        cq.select(root);
        
        Predicate pred = qb.and();
        
        if(idCliente != null) {
            pred = qb.and(pred, qb.equal(root.get("idCliente"), idCliente));
        }
        
        cq.where(pred);
        
        Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
        
        List<Cliente> list = q.getResultList();
        
        Cliente result = list == null || list.isEmpty()
                ? null
                : list.get(0);

        return result;
    }
    
    /**
     * Obtiene la lista de usuarios
     * @param idPersona Identificador de cliente
     * @param estado Estado del usuario
     * @return Lista de usuarios
     */
    public List<Usuario> findUsuario(Integer idPersona, Character estado) {
        
        CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Usuario> root = cq.from(Usuario.class);
        cq.select(root);
        
        Predicate pred = qb.and();
        
        if(idPersona != null) {
            pred = qb.and(pred, qb.equal(root.get("idPersona"), idPersona));
        }
        
        if(estado != null) {
            pred = qb.and(pred, qb.equal(root.get("estado"), estado));
        }
        
        cq.where(pred);
        
        Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
        List<Usuario> usuarios = q.getResultList();

        return usuarios;
    }
    
    /**
     * Obtiene un usuario
     * @param usuario Usuario
     * @return Usuario
     */
    public Usuario findUsuario(String usuario) {
        Usuario result = null;
        if(usuario != null && !usuario.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);
            cq.where(qb.equal(root.get("usuario"), usuario));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Usuario> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Obtiene un producto segun descripcion
     * @param descripcion Descripción del producto
     * @return Producto
     */
    public Producto findProducto(String descripcion) {
        Producto result = null;
        if(descripcion != null && !descripcion.trim().isEmpty()) {
            CriteriaBuilder qb = getEntityManagerSepsa().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Producto> root = cq.from(Producto.class);
            cq.select(root);
            cq.where(qb.equal(root.get("descripcion"), descripcion));
            Query q = getEntityManagerSepsa().createQuery(cq).setHint("eclipselink.refresh", "true");
            List<Producto> lista = q.getResultList();
            if(lista != null && !lista.isEmpty()) {
                result = lista.get(0);
            }
        }
        return result;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean edit(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param object Objeto
     * @return 
     */
    public <T> boolean create(T object) {
        boolean saved = true;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            saved = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return saved;
    }
    
    /**
     * Edita una instancia de documento
     * @param documento Documento
     * @return 
     */
    public Object find(Class clazz, Object id) {
        Object t = null;
        EntityManager em = null;
        try {
            em = getEntityManagerSepsa();
            t = em.find(clazz, id);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return t;
    }
}

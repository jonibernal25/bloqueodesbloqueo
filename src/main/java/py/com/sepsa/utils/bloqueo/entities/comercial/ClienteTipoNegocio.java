/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cliente_tipo_negocio", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClienteTipoNegocio.findAll", query = "SELECT c FROM ClienteTipoNegocio c"),
    @NamedQuery(name = "ClienteTipoNegocio.findByIdTipoNegocio", query = "SELECT c FROM ClienteTipoNegocio c WHERE c.clienteTipoNegocioPK.idTipoNegocio = :idTipoNegocio"),
    @NamedQuery(name = "ClienteTipoNegocio.findByIdCliente", query = "SELECT c FROM ClienteTipoNegocio c WHERE c.clienteTipoNegocioPK.idCliente = :idCliente"),
    @NamedQuery(name = "ClienteTipoNegocio.findByPrincipal", query = "SELECT c FROM ClienteTipoNegocio c WHERE c.principal = :principal")})
public class ClienteTipoNegocio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClienteTipoNegocioPK clienteTipoNegocioPK;
    @Basic(optional = false)
    @Column(name = "principal")
    private Character principal;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "id_tipo_negocio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoNegocio tipoNegocio;

    public ClienteTipoNegocio() {
    }

    public ClienteTipoNegocio(ClienteTipoNegocioPK clienteTipoNegocioPK) {
        this.clienteTipoNegocioPK = clienteTipoNegocioPK;
    }

    public ClienteTipoNegocio(ClienteTipoNegocioPK clienteTipoNegocioPK, Character principal) {
        this.clienteTipoNegocioPK = clienteTipoNegocioPK;
        this.principal = principal;
    }

    public ClienteTipoNegocio(int idTipoNegocio, int idCliente) {
        this.clienteTipoNegocioPK = new ClienteTipoNegocioPK(idTipoNegocio, idCliente);
    }

    public ClienteTipoNegocioPK getClienteTipoNegocioPK() {
        return clienteTipoNegocioPK;
    }

    public void setClienteTipoNegocioPK(ClienteTipoNegocioPK clienteTipoNegocioPK) {
        this.clienteTipoNegocioPK = clienteTipoNegocioPK;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoNegocio getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(TipoNegocio tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteTipoNegocioPK != null ? clienteTipoNegocioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteTipoNegocio)) {
            return false;
        }
        ClienteTipoNegocio other = (ClienteTipoNegocio) object;
        if ((this.clienteTipoNegocioPK == null && other.clienteTipoNegocioPK != null) || (this.clienteTipoNegocioPK != null && !this.clienteTipoNegocioPK.equals(other.clienteTipoNegocioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ClienteTipoNegocio[ clienteTipoNegocioPK=" + clienteTipoNegocioPK + " ]";
    }
    
}

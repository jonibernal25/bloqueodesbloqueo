/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cobro", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cobro.findAll", query = "SELECT c FROM Cobro c"),
    @NamedQuery(name = "Cobro.findById", query = "SELECT c FROM Cobro c WHERE c.id = :id"),
    @NamedQuery(name = "Cobro.findByFecha", query = "SELECT c FROM Cobro c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Cobro.findByMontoCobro", query = "SELECT c FROM Cobro c WHERE c.montoCobro = :montoCobro"),
    @NamedQuery(name = "Cobro.findByNroRecibo", query = "SELECT c FROM Cobro c WHERE c.nroRecibo = :nroRecibo"),
    @NamedQuery(name = "Cobro.findByEstado", query = "SELECT c FROM Cobro c WHERE c.estado = :estado"),
    @NamedQuery(name = "Cobro.findByEnviado", query = "SELECT c FROM Cobro c WHERE c.enviado = :enviado"),
    @NamedQuery(name = "Cobro.findByFechaEnvio", query = "SELECT c FROM Cobro c WHERE c.fechaEnvio = :fechaEnvio")})
public class Cobro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "monto_cobro")
    private BigInteger montoCobro;
    @Basic(optional = false)
    @Column(name = "nro_recibo")
    private String nroRecibo;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "enviado")
    private Character enviado;
    @Column(name = "fecha_envio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEnvio;
    @JoinColumn(name = "id_lugar_cobro", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private LugarCobro idLugarCobro;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Talonario idTalonario;
    @JoinColumn(name = "id_transaccion_red", referencedColumnName = "id")
    @ManyToOne
    private TransaccionRedPago idTransaccionRed;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cobro")
    private Collection<CobroDetalle> cobroDetalleCollection;

    public Cobro() {
    }

    public Cobro(Integer id) {
        this.id = id;
    }

    public Cobro(Integer id, Date fecha, BigInteger montoCobro, String nroRecibo, Character estado) {
        this.id = id;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigInteger montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getEnviado() {
        return enviado;
    }

    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public LugarCobro getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(LugarCobro idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public Talonario getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Talonario idTalonario) {
        this.idTalonario = idTalonario;
    }

    public TransaccionRedPago getIdTransaccionRed() {
        return idTransaccionRed;
    }

    public void setIdTransaccionRed(TransaccionRedPago idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    @XmlTransient
    public Collection<CobroDetalle> getCobroDetalleCollection() {
        return cobroDetalleCollection;
    }

    public void setCobroDetalleCollection(Collection<CobroDetalle> cobroDetalleCollection) {
        this.cobroDetalleCollection = cobroDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobro)) {
            return false;
        }
        Cobro other = (Cobro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Cobro[ id=" + id + " ]";
    }
    
}

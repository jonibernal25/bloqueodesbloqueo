/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tipo_negocio", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoNegocio.findAll", query = "SELECT t FROM TipoNegocio t"),
    @NamedQuery(name = "TipoNegocio.findById", query = "SELECT t FROM TipoNegocio t WHERE t.id = :id"),
    @NamedQuery(name = "TipoNegocio.findByDescripcion", query = "SELECT t FROM TipoNegocio t WHERE t.descripcion = :descripcion")})
public class TipoNegocio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoNegocio")
    private Collection<ContratoTarifa> contratoTarifaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoNegocio")
    private Collection<LocalTipoNegocio> localTipoNegocioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoNegocio")
    private Collection<ClienteTipoNegocio> clienteTipoNegocioCollection;

    public TipoNegocio() {
    }

    public TipoNegocio(Integer id) {
        this.id = id;
    }

    public TipoNegocio(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<ContratoTarifa> getContratoTarifaCollection() {
        return contratoTarifaCollection;
    }

    public void setContratoTarifaCollection(Collection<ContratoTarifa> contratoTarifaCollection) {
        this.contratoTarifaCollection = contratoTarifaCollection;
    }

    @XmlTransient
    public Collection<LocalTipoNegocio> getLocalTipoNegocioCollection() {
        return localTipoNegocioCollection;
    }

    public void setLocalTipoNegocioCollection(Collection<LocalTipoNegocio> localTipoNegocioCollection) {
        this.localTipoNegocioCollection = localTipoNegocioCollection;
    }

    @XmlTransient
    public Collection<ClienteTipoNegocio> getClienteTipoNegocioCollection() {
        return clienteTipoNegocioCollection;
    }

    public void setClienteTipoNegocioCollection(Collection<ClienteTipoNegocio> clienteTipoNegocioCollection) {
        this.clienteTipoNegocioCollection = clienteTipoNegocioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoNegocio)) {
            return false;
        }
        TipoNegocio other = (TipoNegocio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.TipoNegocio[ id=" + id + " ]";
    }
    
}

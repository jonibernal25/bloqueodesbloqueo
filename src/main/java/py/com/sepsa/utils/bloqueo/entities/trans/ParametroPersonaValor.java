/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "parametro_persona_valor", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParametroPersonaValor.findAll", query = "SELECT p FROM ParametroPersonaValor p"),
    @NamedQuery(name = "ParametroPersonaValor.findByValor", query = "SELECT p FROM ParametroPersonaValor p WHERE p.valor = :valor"),
    @NamedQuery(name = "ParametroPersonaValor.findByIdParametroPersona", query = "SELECT p FROM ParametroPersonaValor p WHERE p.parametroPersonaValorPK.idParametroPersona = :idParametroPersona"),
    @NamedQuery(name = "ParametroPersonaValor.findByIdPersona", query = "SELECT p FROM ParametroPersonaValor p WHERE p.parametroPersonaValorPK.idPersona = :idPersona")})
public class ParametroPersonaValor implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParametroPersonaValorPK parametroPersonaValorPK;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "id_parametro_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ParametroPersona parametroPersona;

    public ParametroPersonaValor() {
    }

    public ParametroPersonaValor(ParametroPersonaValorPK parametroPersonaValorPK) {
        this.parametroPersonaValorPK = parametroPersonaValorPK;
    }

    public ParametroPersonaValor(ParametroPersonaValorPK parametroPersonaValorPK, String valor) {
        this.parametroPersonaValorPK = parametroPersonaValorPK;
        this.valor = valor;
    }

    public ParametroPersonaValor(int idParametroPersona, int idPersona) {
        this.parametroPersonaValorPK = new ParametroPersonaValorPK(idParametroPersona, idPersona);
    }

    public ParametroPersonaValorPK getParametroPersonaValorPK() {
        return parametroPersonaValorPK;
    }

    public void setParametroPersonaValorPK(ParametroPersonaValorPK parametroPersonaValorPK) {
        this.parametroPersonaValorPK = parametroPersonaValorPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ParametroPersona getParametroPersona() {
        return parametroPersona;
    }

    public void setParametroPersona(ParametroPersona parametroPersona) {
        this.parametroPersona = parametroPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parametroPersonaValorPK != null ? parametroPersonaValorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParametroPersonaValor)) {
            return false;
        }
        ParametroPersonaValor other = (ParametroPersonaValor) object;
        if ((this.parametroPersonaValorPK == null && other.parametroPersonaValorPK != null) || (this.parametroPersonaValorPK != null && !this.parametroPersonaValorPK.equals(other.parametroPersonaValorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ParametroPersonaValor[ parametroPersonaValorPK=" + parametroPersonaValorPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "promotor_local_comprador", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PromotorLocalComprador.findAll", query = "SELECT p FROM PromotorLocalComprador p"),
    @NamedQuery(name = "PromotorLocalComprador.findByIdPromotor", query = "SELECT p FROM PromotorLocalComprador p WHERE p.promotorLocalCompradorPK.idPromotor = :idPromotor"),
    @NamedQuery(name = "PromotorLocalComprador.findByIdComprador", query = "SELECT p FROM PromotorLocalComprador p WHERE p.promotorLocalCompradorPK.idComprador = :idComprador"),
    @NamedQuery(name = "PromotorLocalComprador.findByIdLocalComprador", query = "SELECT p FROM PromotorLocalComprador p WHERE p.promotorLocalCompradorPK.idLocalComprador = :idLocalComprador")})
public class PromotorLocalComprador implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PromotorLocalCompradorPK promotorLocalCompradorPK;

    public PromotorLocalComprador() {
    }

    public PromotorLocalComprador(PromotorLocalCompradorPK promotorLocalCompradorPK) {
        this.promotorLocalCompradorPK = promotorLocalCompradorPK;
    }

    public PromotorLocalComprador(int idPromotor, int idComprador, int idLocalComprador) {
        this.promotorLocalCompradorPK = new PromotorLocalCompradorPK(idPromotor, idComprador, idLocalComprador);
    }

    public PromotorLocalCompradorPK getPromotorLocalCompradorPK() {
        return promotorLocalCompradorPK;
    }

    public void setPromotorLocalCompradorPK(PromotorLocalCompradorPK promotorLocalCompradorPK) {
        this.promotorLocalCompradorPK = promotorLocalCompradorPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promotorLocalCompradorPK != null ? promotorLocalCompradorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromotorLocalComprador)) {
            return false;
        }
        PromotorLocalComprador other = (PromotorLocalComprador) object;
        if ((this.promotorLocalCompradorPK == null && other.promotorLocalCompradorPK != null) || (this.promotorLocalCompradorPK != null && !this.promotorLocalCompradorPK.equals(other.promotorLocalCompradorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.PromotorLocalComprador[ promotorLocalCompradorPK=" + promotorLocalCompradorPK + " ]";
    }
    
}

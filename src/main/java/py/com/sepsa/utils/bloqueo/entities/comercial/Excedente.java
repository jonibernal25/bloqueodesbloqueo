/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "excedente", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Excedente.findAll", query = "SELECT e FROM Excedente e"),
    @NamedQuery(name = "Excedente.findByIdTipoDocumento", query = "SELECT e FROM Excedente e WHERE e.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "Excedente.findByCantidadMaxima", query = "SELECT e FROM Excedente e WHERE e.cantidadMaxima = :cantidadMaxima"),
    @NamedQuery(name = "Excedente.findByMontoExcedente", query = "SELECT e FROM Excedente e WHERE e.montoExcedente = :montoExcedente"),
    @NamedQuery(name = "Excedente.findById", query = "SELECT e FROM Excedente e WHERE e.id = :id"),
    @NamedQuery(name = "Excedente.findByEstado", query = "SELECT e FROM Excedente e WHERE e.estado = :estado")})
public class Excedente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_tipo_documento")
    private Integer idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "cantidad_maxima")
    private int cantidadMaxima;
    @Basic(optional = false)
    @Column(name = "monto_excedente")
    private BigInteger montoExcedente;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @JoinColumns({
        @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio"),
        @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")})
    @ManyToOne(optional = false)
    private ProductoServicio productoServicio;

    public Excedente() {
    }

    public Excedente(Integer id) {
        this.id = id;
    }

    public Excedente(Integer id, int cantidadMaxima, BigInteger montoExcedente, Character estado) {
        this.id = id;
        this.cantidadMaxima = cantidadMaxima;
        this.montoExcedente = montoExcedente;
        this.estado = estado;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getCantidadMaxima() {
        return cantidadMaxima;
    }

    public void setCantidadMaxima(int cantidadMaxima) {
        this.cantidadMaxima = cantidadMaxima;
    }

    public BigInteger getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(BigInteger montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public ProductoServicio getProductoServicio() {
        return productoServicio;
    }

    public void setProductoServicio(ProductoServicio productoServicio) {
        this.productoServicio = productoServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Excedente)) {
            return false;
        }
        Excedente other = (Excedente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Excedente[ id=" + id + " ]";
    }
    
}

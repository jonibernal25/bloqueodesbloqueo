/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "renovacion", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Renovacion.findAll", query = "SELECT r FROM Renovacion r"),
    @NamedQuery(name = "Renovacion.findById", query = "SELECT r FROM Renovacion r WHERE r.renovacionPK.id = :id"),
    @NamedQuery(name = "Renovacion.findByIdContrato", query = "SELECT r FROM Renovacion r WHERE r.renovacionPK.idContrato = :idContrato"),
    @NamedQuery(name = "Renovacion.findByNroRenovacion", query = "SELECT r FROM Renovacion r WHERE r.nroRenovacion = :nroRenovacion"),
    @NamedQuery(name = "Renovacion.findByFechaRenovacion", query = "SELECT r FROM Renovacion r WHERE r.fechaRenovacion = :fechaRenovacion"),
    @NamedQuery(name = "Renovacion.findByFechaInicio", query = "SELECT r FROM Renovacion r WHERE r.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Renovacion.findByFechaFin", query = "SELECT r FROM Renovacion r WHERE r.fechaFin = :fechaFin"),
    @NamedQuery(name = "Renovacion.findByObservacion", query = "SELECT r FROM Renovacion r WHERE r.observacion = :observacion")})
public class Renovacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RenovacionPK renovacionPK;
    @Basic(optional = false)
    @Column(name = "nro_renovacion")
    private String nroRenovacion;
    @Basic(optional = false)
    @Column(name = "fecha_renovacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRenovacion;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contrato contrato;

    public Renovacion() {
    }

    public Renovacion(RenovacionPK renovacionPK) {
        this.renovacionPK = renovacionPK;
    }

    public Renovacion(RenovacionPK renovacionPK, String nroRenovacion, Date fechaRenovacion, Date fechaInicio, Date fechaFin) {
        this.renovacionPK = renovacionPK;
        this.nroRenovacion = nroRenovacion;
        this.fechaRenovacion = fechaRenovacion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Renovacion(int id, int idContrato) {
        this.renovacionPK = new RenovacionPK(id, idContrato);
    }

    public RenovacionPK getRenovacionPK() {
        return renovacionPK;
    }

    public void setRenovacionPK(RenovacionPK renovacionPK) {
        this.renovacionPK = renovacionPK;
    }

    public String getNroRenovacion() {
        return nroRenovacion;
    }

    public void setNroRenovacion(String nroRenovacion) {
        this.nroRenovacion = nroRenovacion;
    }

    public Date getFechaRenovacion() {
        return fechaRenovacion;
    }

    public void setFechaRenovacion(Date fechaRenovacion) {
        this.fechaRenovacion = fechaRenovacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (renovacionPK != null ? renovacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Renovacion)) {
            return false;
        }
        Renovacion other = (Renovacion) object;
        if ((this.renovacionPK == null && other.renovacionPK != null) || (this.renovacionPK != null && !this.renovacionPK.equals(other.renovacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Renovacion[ renovacionPK=" + renovacionPK + " ]";
    }
    
}

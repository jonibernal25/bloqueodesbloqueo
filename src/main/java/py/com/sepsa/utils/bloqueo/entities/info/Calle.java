/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "calle", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Calle.findAll", query = "SELECT c FROM Calle c"),
    @NamedQuery(name = "Calle.findById", query = "SELECT c FROM Calle c WHERE c.id = :id"),
    @NamedQuery(name = "Calle.findByDescripcion", query = "SELECT c FROM Calle c WHERE c.descripcion = :descripcion")})
public class Calle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_barrio", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Barrio idBarrio;
    @OneToMany(mappedBy = "idCalle4")
    private Collection<Direccion> direccionCollection;
    @OneToMany(mappedBy = "idCalle3")
    private Collection<Direccion> direccionCollection1;
    @OneToMany(mappedBy = "idCalle2")
    private Collection<Direccion> direccionCollection2;
    @OneToMany(mappedBy = "idCalle1")
    private Collection<Direccion> direccionCollection3;

    public Calle() {
    }

    public Calle(Integer id) {
        this.id = id;
    }

    public Calle(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Barrio getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Barrio idBarrio) {
        this.idBarrio = idBarrio;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection() {
        return direccionCollection;
    }

    public void setDireccionCollection(Collection<Direccion> direccionCollection) {
        this.direccionCollection = direccionCollection;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection1() {
        return direccionCollection1;
    }

    public void setDireccionCollection1(Collection<Direccion> direccionCollection1) {
        this.direccionCollection1 = direccionCollection1;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection2() {
        return direccionCollection2;
    }

    public void setDireccionCollection2(Collection<Direccion> direccionCollection2) {
        this.direccionCollection2 = direccionCollection2;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection3() {
        return direccionCollection3;
    }

    public void setDireccionCollection3(Collection<Direccion> direccionCollection3) {
        this.direccionCollection3 = direccionCollection3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Calle)) {
            return false;
        }
        Calle other = (Calle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Calle[ id=" + id + " ]";
    }
    
}

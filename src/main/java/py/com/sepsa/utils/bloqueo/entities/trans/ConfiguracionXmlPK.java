/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ConfiguracionXmlPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_configuracion")
    private int idConfiguracion;
    @Basic(optional = false)
    @Column(name = "id_xml")
    private int idXml;

    public ConfiguracionXmlPK() {
    }

    public ConfiguracionXmlPK(int idConfiguracion, int idXml) {
        this.idConfiguracion = idConfiguracion;
        this.idXml = idXml;
    }

    public int getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(int idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public int getIdXml() {
        return idXml;
    }

    public void setIdXml(int idXml) {
        this.idXml = idXml;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idConfiguracion;
        hash += (int) idXml;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionXmlPK)) {
            return false;
        }
        ConfiguracionXmlPK other = (ConfiguracionXmlPK) object;
        if (this.idConfiguracion != other.idConfiguracion) {
            return false;
        }
        if (this.idXml != other.idXml) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ConfiguracionXmlPK[ idConfiguracion=" + idConfiguracion + ", idXml=" + idXml + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "clave_instalacion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaveInstalacion.findAll", query = "SELECT c FROM ClaveInstalacion c"),
    @NamedQuery(name = "ClaveInstalacion.findByClave", query = "SELECT c FROM ClaveInstalacion c WHERE c.clave = :clave"),
    @NamedQuery(name = "ClaveInstalacion.findByEstado", query = "SELECT c FROM ClaveInstalacion c WHERE c.estado = :estado"),
    @NamedQuery(name = "ClaveInstalacion.findByFechaInsercion", query = "SELECT c FROM ClaveInstalacion c WHERE c.fechaInsercion = :fechaInsercion")})
public class ClaveInstalacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "clave")
    private String clave;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Instalacion idInstalacion;

    public ClaveInstalacion() {
    }

    public ClaveInstalacion(String clave) {
        this.clave = clave;
    }

    public ClaveInstalacion(String clave, Character estado, Date fechaInsercion) {
        this.clave = clave;
        this.estado = estado;
        this.fechaInsercion = fechaInsercion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Instalacion getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(Instalacion idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaveInstalacion)) {
            return false;
        }
        ClaveInstalacion other = (ClaveInstalacion) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ClaveInstalacion[ clave=" + clave + " ]";
    }
    
}

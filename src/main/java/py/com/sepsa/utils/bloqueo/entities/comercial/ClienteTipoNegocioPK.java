/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ClienteTipoNegocioPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_tipo_negocio")
    private int idTipoNegocio;
    @Basic(optional = false)
    @Column(name = "id_cliente")
    private int idCliente;

    public ClienteTipoNegocioPK() {
    }

    public ClienteTipoNegocioPK(int idTipoNegocio, int idCliente) {
        this.idTipoNegocio = idTipoNegocio;
        this.idCliente = idCliente;
    }

    public int getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public void setIdTipoNegocio(int idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idTipoNegocio;
        hash += (int) idCliente;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteTipoNegocioPK)) {
            return false;
        }
        ClienteTipoNegocioPK other = (ClienteTipoNegocioPK) object;
        if (this.idTipoNegocio != other.idTipoNegocio) {
            return false;
        }
        if (this.idCliente != other.idCliente) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ClienteTipoNegocioPK[ idTipoNegocio=" + idTipoNegocio + ", idCliente=" + idCliente + " ]";
    }
    
}

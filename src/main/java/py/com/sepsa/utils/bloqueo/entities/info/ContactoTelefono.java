/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "contacto_telefono", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactoTelefono.findAll", query = "SELECT c FROM ContactoTelefono c"),
    @NamedQuery(name = "ContactoTelefono.findByIdContacto", query = "SELECT c FROM ContactoTelefono c WHERE c.contactoTelefonoPK.idContacto = :idContacto"),
    @NamedQuery(name = "ContactoTelefono.findByIdTelefono", query = "SELECT c FROM ContactoTelefono c WHERE c.contactoTelefonoPK.idTelefono = :idTelefono"),
    @NamedQuery(name = "ContactoTelefono.findByNotificacionDocumento", query = "SELECT c FROM ContactoTelefono c WHERE c.notificacionDocumento = :notificacionDocumento")})
public class ContactoTelefono implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContactoTelefonoPK contactoTelefonoPK;
    @Basic(optional = false)
    @Column(name = "notificacion_documento")
    private Character notificacionDocumento;
    @JoinColumn(name = "id_contacto", referencedColumnName = "id_contacto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contacto contacto;
    @JoinColumn(name = "id_telefono", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Telefono telefono;

    public ContactoTelefono() {
    }

    public ContactoTelefono(ContactoTelefonoPK contactoTelefonoPK) {
        this.contactoTelefonoPK = contactoTelefonoPK;
    }

    public ContactoTelefono(ContactoTelefonoPK contactoTelefonoPK, Character notificacionDocumento) {
        this.contactoTelefonoPK = contactoTelefonoPK;
        this.notificacionDocumento = notificacionDocumento;
    }

    public ContactoTelefono(int idContacto, int idTelefono) {
        this.contactoTelefonoPK = new ContactoTelefonoPK(idContacto, idTelefono);
    }

    public ContactoTelefonoPK getContactoTelefonoPK() {
        return contactoTelefonoPK;
    }

    public void setContactoTelefonoPK(ContactoTelefonoPK contactoTelefonoPK) {
        this.contactoTelefonoPK = contactoTelefonoPK;
    }

    public Character getNotificacionDocumento() {
        return notificacionDocumento;
    }

    public void setNotificacionDocumento(Character notificacionDocumento) {
        this.notificacionDocumento = notificacionDocumento;
    }

    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactoTelefonoPK != null ? contactoTelefonoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactoTelefono)) {
            return false;
        }
        ContactoTelefono other = (ContactoTelefono) object;
        if ((this.contactoTelefonoPK == null && other.contactoTelefonoPK != null) || (this.contactoTelefonoPK != null && !this.contactoTelefonoPK.equals(other.contactoTelefonoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.ContactoTelefono[ contactoTelefonoPK=" + contactoTelefonoPK + " ]";
    }
    
}

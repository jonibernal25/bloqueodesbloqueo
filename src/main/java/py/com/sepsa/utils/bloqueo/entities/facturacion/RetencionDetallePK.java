/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class RetencionDetallePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_factura")
    private int idFactura;
    @Basic(optional = false)
    @Column(name = "id_retencion")
    private int idRetencion;

    public RetencionDetallePK() {
    }

    public RetencionDetallePK(int idFactura, int idRetencion) {
        this.idFactura = idFactura;
        this.idRetencion = idRetencion;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getIdRetencion() {
        return idRetencion;
    }

    public void setIdRetencion(int idRetencion) {
        this.idRetencion = idRetencion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idFactura;
        hash += (int) idRetencion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RetencionDetallePK)) {
            return false;
        }
        RetencionDetallePK other = (RetencionDetallePK) object;
        if (this.idFactura != other.idFactura) {
            return false;
        }
        if (this.idRetencion != other.idRetencion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.RetencionDetallePK[ idFactura=" + idFactura + ", idRetencion=" + idRetencion + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "transaccion_red_pago", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransaccionRedPago.findAll", query = "SELECT t FROM TransaccionRedPago t"),
    @NamedQuery(name = "TransaccionRedPago.findById", query = "SELECT t FROM TransaccionRedPago t WHERE t.id = :id"),
    @NamedQuery(name = "TransaccionRedPago.findByIdTransaccion", query = "SELECT t FROM TransaccionRedPago t WHERE t.idTransaccion = :idTransaccion"),
    @NamedQuery(name = "TransaccionRedPago.findByFecha", query = "SELECT t FROM TransaccionRedPago t WHERE t.fecha = :fecha")})
public class TransaccionRedPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_transaccion")
    private String idTransaccion;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "id_mensaje", referencedColumnName = "id")
    @ManyToOne
    private MensajeRedPago idMensaje;
    @JoinColumn(name = "id_operacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private OperacionRedPago idOperacion;
    @JoinColumn(name = "id_red", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private RedPago idRed;
    @OneToMany(mappedBy = "idTransaccionRed")
    private Collection<Cobro> cobroCollection;

    public TransaccionRedPago() {
    }

    public TransaccionRedPago(Integer id) {
        this.id = id;
    }

    public TransaccionRedPago(Integer id, String idTransaccion, Date fecha) {
        this.id = id;
        this.idTransaccion = idTransaccion;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public MensajeRedPago getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(MensajeRedPago idMensaje) {
        this.idMensaje = idMensaje;
    }

    public OperacionRedPago getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(OperacionRedPago idOperacion) {
        this.idOperacion = idOperacion;
    }

    public RedPago getIdRed() {
        return idRed;
    }

    public void setIdRed(RedPago idRed) {
        this.idRed = idRed;
    }

    @XmlTransient
    public Collection<Cobro> getCobroCollection() {
        return cobroCollection;
    }

    public void setCobroCollection(Collection<Cobro> cobroCollection) {
        this.cobroCollection = cobroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransaccionRedPago)) {
            return false;
        }
        TransaccionRedPago other = (TransaccionRedPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.TransaccionRedPago[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "operacion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operacion.findAll", query = "SELECT o FROM Operacion o"),
    @NamedQuery(name = "Operacion.findById", query = "SELECT o FROM Operacion o WHERE o.id = :id"),
    @NamedQuery(name = "Operacion.findByFechaOperacion", query = "SELECT o FROM Operacion o WHERE o.fechaOperacion = :fechaOperacion"),
    @NamedQuery(name = "Operacion.findByEstado", query = "SELECT o FROM Operacion o WHERE o.estado = :estado"),
    @NamedQuery(name = "Operacion.findByIdInstalacion2", query = "SELECT o FROM Operacion o WHERE o.idInstalacion2 = :idInstalacion2"),
    @NamedQuery(name = "Operacion.findByIdSoftware", query = "SELECT o FROM Operacion o WHERE o.idSoftware = :idSoftware"),
    @NamedQuery(name = "Operacion.findByVersionSoftware", query = "SELECT o FROM Operacion o WHERE o.versionSoftware = :versionSoftware")})
public class Operacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fecha_operacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaOperacion;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "id_instalacion2")
    private Integer idInstalacion2;
    @Column(name = "id_software")
    private Integer idSoftware;
    @Column(name = "version_software")
    private String versionSoftware;
    @JoinColumns({
        @JoinColumn(name = "id_documento", referencedColumnName = "id"),
        @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento")})
    @ManyToOne(optional = false)
    private Documento documento;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id")
    @ManyToOne
    private Instalacion idInstalacion;
    @JoinColumn(name = "id_tipo_operacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoOperacion idTipoOperacion;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario idUsuario;

    public Operacion() {
    }

    public Operacion(Integer id) {
        this.id = id;
    }

    public Operacion(Integer id, Date fechaOperacion, Character estado) {
        this.id = id;
        this.fechaOperacion = fechaOperacion;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(Date fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Integer getIdInstalacion2() {
        return idInstalacion2;
    }

    public void setIdInstalacion2(Integer idInstalacion2) {
        this.idInstalacion2 = idInstalacion2;
    }

    public Integer getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(Integer idSoftware) {
        this.idSoftware = idSoftware;
    }

    public String getVersionSoftware() {
        return versionSoftware;
    }

    public void setVersionSoftware(String versionSoftware) {
        this.versionSoftware = versionSoftware;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Instalacion getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(Instalacion idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public TipoOperacion getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(TipoOperacion idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operacion)) {
            return false;
        }
        Operacion other = (Operacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.Operacion[ id=" + id + " ]";
    }
    
}

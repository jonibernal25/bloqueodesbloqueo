/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class CobroDetallePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_cobro")
    private int idCobro;
    @Basic(optional = false)
    @Column(name = "id_factura")
    private int idFactura;
    @Basic(optional = false)
    @Column(name = "nro_linea")
    private int nroLinea;

    public CobroDetallePK() {
    }

    public CobroDetallePK(int idCobro, int idFactura, int nroLinea) {
        this.idCobro = idCobro;
        this.idFactura = idFactura;
        this.nroLinea = nroLinea;
    }

    public int getIdCobro() {
        return idCobro;
    }

    public void setIdCobro(int idCobro) {
        this.idCobro = idCobro;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public int getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(int nroLinea) {
        this.nroLinea = nroLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCobro;
        hash += (int) idFactura;
        hash += (int) nroLinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobroDetallePK)) {
            return false;
        }
        CobroDetallePK other = (CobroDetallePK) object;
        if (this.idCobro != other.idCobro) {
            return false;
        }
        if (this.idFactura != other.idFactura) {
            return false;
        }
        if (this.nroLinea != other.nroLinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.CobroDetallePK[ idCobro=" + idCobro + ", idFactura=" + idFactura + ", nroLinea=" + nroLinea + " ]";
    }
    
}

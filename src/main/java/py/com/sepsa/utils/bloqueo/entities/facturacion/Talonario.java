/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "talonario", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Talonario.findAll", query = "SELECT t FROM Talonario t"),
    @NamedQuery(name = "Talonario.findById", query = "SELECT t FROM Talonario t WHERE t.id = :id"),
    @NamedQuery(name = "Talonario.findByTimbrado", query = "SELECT t FROM Talonario t WHERE t.timbrado = :timbrado"),
    @NamedQuery(name = "Talonario.findByInicio", query = "SELECT t FROM Talonario t WHERE t.inicio = :inicio"),
    @NamedQuery(name = "Talonario.findByFin", query = "SELECT t FROM Talonario t WHERE t.fin = :fin"),
    @NamedQuery(name = "Talonario.findByFechaVencimiento", query = "SELECT t FROM Talonario t WHERE t.fechaVencimiento = :fechaVencimiento"),
    @NamedQuery(name = "Talonario.findByEstado", query = "SELECT t FROM Talonario t WHERE t.estado = :estado"),
    @NamedQuery(name = "Talonario.findByNroSucursal", query = "SELECT t FROM Talonario t WHERE t.nroSucursal = :nroSucursal"),
    @NamedQuery(name = "Talonario.findByNroPuntoVenta", query = "SELECT t FROM Talonario t WHERE t.nroPuntoVenta = :nroPuntoVenta"),
    @NamedQuery(name = "Talonario.findByIdEncargado", query = "SELECT t FROM Talonario t WHERE t.idEncargado = :idEncargado"),
    @NamedQuery(name = "Talonario.findByFechaInicio", query = "SELECT t FROM Talonario t WHERE t.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Talonario.findByNroResolucion", query = "SELECT t FROM Talonario t WHERE t.nroResolucion = :nroResolucion"),
    @NamedQuery(name = "Talonario.findByFechaResolucion", query = "SELECT t FROM Talonario t WHERE t.fechaResolucion = :fechaResolucion")})
public class Talonario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "timbrado")
    private Integer timbrado;
    @Basic(optional = false)
    @Column(name = "inicio")
    private int inicio;
    @Basic(optional = false)
    @Column(name = "fin")
    private int fin;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "nro_sucursal")
    private String nroSucursal;
    @Basic(optional = false)
    @Column(name = "nro_punto_venta")
    private String nroPuntoVenta;
    @Column(name = "id_encargado")
    private Integer idEncargado;
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "nro_resolucion")
    private String nroResolucion;
    @Column(name = "fecha_resolucion")
    @Temporal(TemporalType.DATE)
    private Date fechaResolucion;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoDocumento idTipoDocumento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTalonario")
    private Collection<Factura> facturaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTalonario")
    private Collection<Cobro> cobroCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTalonario")
    private Collection<NotaCredito> notaCreditoCollection;

    public Talonario() {
    }

    public Talonario(Integer id) {
        this.id = id;
    }

    public Talonario(Integer id, int inicio, int fin, Character estado, String nroSucursal, String nroPuntoVenta) {
        this.id = id;
        this.inicio = inicio;
        this.fin = fin;
        this.estado = estado;
        this.nroSucursal = nroSucursal;
        this.nroPuntoVenta = nroPuntoVenta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    public int getFin() {
        return fin;
    }

    public void setFin(int fin) {
        this.fin = fin;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getNroSucursal() {
        return nroSucursal;
    }

    public void setNroSucursal(String nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    public String getNroPuntoVenta() {
        return nroPuntoVenta;
    }

    public void setNroPuntoVenta(String nroPuntoVenta) {
        this.nroPuntoVenta = nroPuntoVenta;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public TipoDocumento getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TipoDocumento idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @XmlTransient
    public Collection<Factura> getFacturaCollection() {
        return facturaCollection;
    }

    public void setFacturaCollection(Collection<Factura> facturaCollection) {
        this.facturaCollection = facturaCollection;
    }

    @XmlTransient
    public Collection<Cobro> getCobroCollection() {
        return cobroCollection;
    }

    public void setCobroCollection(Collection<Cobro> cobroCollection) {
        this.cobroCollection = cobroCollection;
    }

    @XmlTransient
    public Collection<NotaCredito> getNotaCreditoCollection() {
        return notaCreditoCollection;
    }

    public void setNotaCreditoCollection(Collection<NotaCredito> notaCreditoCollection) {
        this.notaCreditoCollection = notaCreditoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Talonario)) {
            return false;
        }
        Talonario other = (Talonario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Talonario[ id=" + id + " ]";
    }
    
}

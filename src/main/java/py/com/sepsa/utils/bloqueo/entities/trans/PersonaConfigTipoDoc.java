/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "persona_config_tipo_doc", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonaConfigTipoDoc.findAll", query = "SELECT p FROM PersonaConfigTipoDoc p"),
    @NamedQuery(name = "PersonaConfigTipoDoc.findByIdPersona", query = "SELECT p FROM PersonaConfigTipoDoc p WHERE p.personaConfigTipoDocPK.idPersona = :idPersona"),
    @NamedQuery(name = "PersonaConfigTipoDoc.findByIdConfigTipoDoc", query = "SELECT p FROM PersonaConfigTipoDoc p WHERE p.personaConfigTipoDocPK.idConfigTipoDoc = :idConfigTipoDoc"),
    @NamedQuery(name = "PersonaConfigTipoDoc.findByValor", query = "SELECT p FROM PersonaConfigTipoDoc p WHERE p.valor = :valor")})
public class PersonaConfigTipoDoc implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonaConfigTipoDocPK personaConfigTipoDocPK;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "id_config_tipo_doc", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ConfigTipoDoc configTipoDoc;

    public PersonaConfigTipoDoc() {
    }

    public PersonaConfigTipoDoc(PersonaConfigTipoDocPK personaConfigTipoDocPK) {
        this.personaConfigTipoDocPK = personaConfigTipoDocPK;
    }

    public PersonaConfigTipoDoc(PersonaConfigTipoDocPK personaConfigTipoDocPK, String valor) {
        this.personaConfigTipoDocPK = personaConfigTipoDocPK;
        this.valor = valor;
    }

    public PersonaConfigTipoDoc(int idPersona, int idConfigTipoDoc) {
        this.personaConfigTipoDocPK = new PersonaConfigTipoDocPK(idPersona, idConfigTipoDoc);
    }

    public PersonaConfigTipoDocPK getPersonaConfigTipoDocPK() {
        return personaConfigTipoDocPK;
    }

    public void setPersonaConfigTipoDocPK(PersonaConfigTipoDocPK personaConfigTipoDocPK) {
        this.personaConfigTipoDocPK = personaConfigTipoDocPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ConfigTipoDoc getConfigTipoDoc() {
        return configTipoDoc;
    }

    public void setConfigTipoDoc(ConfigTipoDoc configTipoDoc) {
        this.configTipoDoc = configTipoDoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personaConfigTipoDocPK != null ? personaConfigTipoDocPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaConfigTipoDoc)) {
            return false;
        }
        PersonaConfigTipoDoc other = (PersonaConfigTipoDoc) object;
        if ((this.personaConfigTipoDocPK == null && other.personaConfigTipoDocPK != null) || (this.personaConfigTipoDocPK != null && !this.personaConfigTipoDocPK.equals(other.personaConfigTipoDocPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.PersonaConfigTipoDoc[ personaConfigTipoDocPK=" + personaConfigTipoDocPK + " ]";
    }
    
}

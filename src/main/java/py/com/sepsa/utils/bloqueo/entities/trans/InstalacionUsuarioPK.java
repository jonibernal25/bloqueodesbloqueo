/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class InstalacionUsuarioPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_instalacion")
    private int idInstalacion;
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;

    public InstalacionUsuarioPK() {
    }

    public InstalacionUsuarioPK(int idInstalacion, int idUsuario) {
        this.idInstalacion = idInstalacion;
        this.idUsuario = idUsuario;
    }

    public int getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(int idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idInstalacion;
        hash += (int) idUsuario;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstalacionUsuarioPK)) {
            return false;
        }
        InstalacionUsuarioPK other = (InstalacionUsuarioPK) object;
        if (this.idInstalacion != other.idInstalacion) {
            return false;
        }
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.InstalacionUsuarioPK[ idInstalacion=" + idInstalacion + ", idUsuario=" + idUsuario + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "dato_adicional", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatoAdicional.findAll", query = "SELECT d FROM DatoAdicional d"),
    @NamedQuery(name = "DatoAdicional.findByIdDocumento", query = "SELECT d FROM DatoAdicional d WHERE d.datoAdicionalPK.idDocumento = :idDocumento"),
    @NamedQuery(name = "DatoAdicional.findByIdTipoDocumento", query = "SELECT d FROM DatoAdicional d WHERE d.datoAdicionalPK.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "DatoAdicional.findByIdTipoDatoAdicional", query = "SELECT d FROM DatoAdicional d WHERE d.datoAdicionalPK.idTipoDatoAdicional = :idTipoDatoAdicional"),
    @NamedQuery(name = "DatoAdicional.findByValor", query = "SELECT d FROM DatoAdicional d WHERE d.valor = :valor")})
public class DatoAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DatoAdicionalPK datoAdicionalPK;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @JoinColumns({
        @JoinColumn(name = "id_documento", referencedColumnName = "id", insertable = false, updatable = false),
        @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Documento documento;
    @JoinColumn(name = "id_tipo_dato_adicional", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoDatoAdicional tipoDatoAdicional;

    public DatoAdicional() {
    }

    public DatoAdicional(DatoAdicionalPK datoAdicionalPK) {
        this.datoAdicionalPK = datoAdicionalPK;
    }

    public DatoAdicional(DatoAdicionalPK datoAdicionalPK, String valor) {
        this.datoAdicionalPK = datoAdicionalPK;
        this.valor = valor;
    }

    public DatoAdicional(int idDocumento, int idTipoDocumento, int idTipoDatoAdicional) {
        this.datoAdicionalPK = new DatoAdicionalPK(idDocumento, idTipoDocumento, idTipoDatoAdicional);
    }

    public DatoAdicionalPK getDatoAdicionalPK() {
        return datoAdicionalPK;
    }

    public void setDatoAdicionalPK(DatoAdicionalPK datoAdicionalPK) {
        this.datoAdicionalPK = datoAdicionalPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public TipoDatoAdicional getTipoDatoAdicional() {
        return tipoDatoAdicional;
    }

    public void setTipoDatoAdicional(TipoDatoAdicional tipoDatoAdicional) {
        this.tipoDatoAdicional = tipoDatoAdicional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datoAdicionalPK != null ? datoAdicionalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatoAdicional)) {
            return false;
        }
        DatoAdicional other = (DatoAdicional) object;
        if ((this.datoAdicionalPK == null && other.datoAdicionalPK != null) || (this.datoAdicionalPK != null && !this.datoAdicionalPK.equals(other.datoAdicionalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.DatoAdicional[ datoAdicionalPK=" + datoAdicionalPK + " ]";
    }
    
}

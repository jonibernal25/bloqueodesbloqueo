/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class FacturaDetallePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_factura")
    private int idFactura;
    @Basic(optional = false)
    @Column(name = "nro_linea")
    private BigInteger nroLinea;

    public FacturaDetallePK() {
    }

    public FacturaDetallePK(int idFactura, BigInteger nroLinea) {
        this.idFactura = idFactura;
        this.nroLinea = nroLinea;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public BigInteger getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(BigInteger nroLinea) {
        this.nroLinea = nroLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idFactura;
        hash += (nroLinea != null ? nroLinea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturaDetallePK)) {
            return false;
        }
        FacturaDetallePK other = (FacturaDetallePK) object;
        if (this.idFactura != other.idFactura) {
            return false;
        }
        if ((this.nroLinea == null && other.nroLinea != null) || (this.nroLinea != null && !this.nroLinea.equals(other.nroLinea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.FacturaDetallePK[ idFactura=" + idFactura + ", nroLinea=" + nroLinea + " ]";
    }
    
}

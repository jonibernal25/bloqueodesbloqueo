/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "proveedor_comprador", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProveedorComprador.findAll", query = "SELECT p FROM ProveedorComprador p"),
    @NamedQuery(name = "ProveedorComprador.findByIdProveedor", query = "SELECT p FROM ProveedorComprador p WHERE p.proveedorCompradorPK.idProveedor = :idProveedor"),
    @NamedQuery(name = "ProveedorComprador.findByIdComprador", query = "SELECT p FROM ProveedorComprador p WHERE p.proveedorCompradorPK.idComprador = :idComprador"),
    @NamedQuery(name = "ProveedorComprador.findByEstado", query = "SELECT p FROM ProveedorComprador p WHERE p.estado = :estado"),
    @NamedQuery(name = "ProveedorComprador.findByFechaInsercion", query = "SELECT p FROM ProveedorComprador p WHERE p.fechaInsercion = :fechaInsercion"),
    @NamedQuery(name = "ProveedorComprador.findByFechaModificacion", query = "SELECT p FROM ProveedorComprador p WHERE p.fechaModificacion = :fechaModificacion")})
public class ProveedorComprador implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProveedorCompradorPK proveedorCompradorPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;

    public ProveedorComprador() {
    }

    public ProveedorComprador(ProveedorCompradorPK proveedorCompradorPK) {
        this.proveedorCompradorPK = proveedorCompradorPK;
    }

    public ProveedorComprador(ProveedorCompradorPK proveedorCompradorPK, Character estado) {
        this.proveedorCompradorPK = proveedorCompradorPK;
        this.estado = estado;
    }

    public ProveedorComprador(int idProveedor, int idComprador) {
        this.proveedorCompradorPK = new ProveedorCompradorPK(idProveedor, idComprador);
    }

    public ProveedorCompradorPK getProveedorCompradorPK() {
        return proveedorCompradorPK;
    }

    public void setProveedorCompradorPK(ProveedorCompradorPK proveedorCompradorPK) {
        this.proveedorCompradorPK = proveedorCompradorPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorCompradorPK != null ? proveedorCompradorPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorComprador)) {
            return false;
        }
        ProveedorComprador other = (ProveedorComprador) object;
        if ((this.proveedorCompradorPK == null && other.proveedorCompradorPK != null) || (this.proveedorCompradorPK != null && !this.proveedorCompradorPK.equals(other.proveedorCompradorPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ProveedorComprador[ proveedorCompradorPK=" + proveedorCompradorPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class CompradorPromocionPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_comprador")
    private int idComprador;
    @Basic(optional = false)
    @Column(name = "id_promocion")
    private int idPromocion;

    public CompradorPromocionPK() {
    }

    public CompradorPromocionPK(int idComprador, int idPromocion) {
        this.idComprador = idComprador;
        this.idPromocion = idPromocion;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idComprador;
        hash += (int) idPromocion;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompradorPromocionPK)) {
            return false;
        }
        CompradorPromocionPK other = (CompradorPromocionPK) object;
        if (this.idComprador != other.idComprador) {
            return false;
        }
        if (this.idPromocion != other.idPromocion) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.CompradorPromocionPK[ idComprador=" + idComprador + ", idPromocion=" + idPromocion + " ]";
    }
    
}

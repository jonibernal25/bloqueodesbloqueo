/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "entidad_financiera", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntidadFinanciera.findAll", query = "SELECT e FROM EntidadFinanciera e"),
    @NamedQuery(name = "EntidadFinanciera.findByIdEntidadFinanciera", query = "SELECT e FROM EntidadFinanciera e WHERE e.idEntidadFinanciera = :idEntidadFinanciera")})
public class EntidadFinanciera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEntidadFinanciera")
    private Collection<CuentaEntidadFinanciera> cuentaEntidadFinancieraCollection;
    @JoinColumn(name = "id_entidad_financiera", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Persona persona;

    public EntidadFinanciera() {
    }

    public EntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    @XmlTransient
    public Collection<CuentaEntidadFinanciera> getCuentaEntidadFinancieraCollection() {
        return cuentaEntidadFinancieraCollection;
    }

    public void setCuentaEntidadFinancieraCollection(Collection<CuentaEntidadFinanciera> cuentaEntidadFinancieraCollection) {
        this.cuentaEntidadFinancieraCollection = cuentaEntidadFinancieraCollection;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntidadFinanciera != null ? idEntidadFinanciera.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadFinanciera)) {
            return false;
        }
        EntidadFinanciera other = (EntidadFinanciera) object;
        if ((this.idEntidadFinanciera == null && other.idEntidadFinanciera != null) || (this.idEntidadFinanciera != null && !this.idEntidadFinanciera.equals(other.idEntidadFinanciera))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.EntidadFinanciera[ idEntidadFinanciera=" + idEntidadFinanciera + " ]";
    }
    
}

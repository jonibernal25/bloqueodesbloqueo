/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "algoritmo_encriptacion", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlgoritmoEncriptacion.findAll", query = "SELECT a FROM AlgoritmoEncriptacion a"),
    @NamedQuery(name = "AlgoritmoEncriptacion.findById", query = "SELECT a FROM AlgoritmoEncriptacion a WHERE a.id = :id"),
    @NamedQuery(name = "AlgoritmoEncriptacion.findByDescripcion", query = "SELECT a FROM AlgoritmoEncriptacion a WHERE a.descripcion = :descripcion")})
public class AlgoritmoEncriptacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idAlgoritmoEncriptacion")
    private Collection<HistoricoClave> historicoClaveCollection;
    @OneToMany(mappedBy = "idAlgoritmoEncriptacion")
    private Collection<Instalacion> instalacionCollection;

    public AlgoritmoEncriptacion() {
    }

    public AlgoritmoEncriptacion(Integer id) {
        this.id = id;
    }

    public AlgoritmoEncriptacion(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<HistoricoClave> getHistoricoClaveCollection() {
        return historicoClaveCollection;
    }

    public void setHistoricoClaveCollection(Collection<HistoricoClave> historicoClaveCollection) {
        this.historicoClaveCollection = historicoClaveCollection;
    }

    @XmlTransient
    public Collection<Instalacion> getInstalacionCollection() {
        return instalacionCollection;
    }

    public void setInstalacionCollection(Collection<Instalacion> instalacionCollection) {
        this.instalacionCollection = instalacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlgoritmoEncriptacion)) {
            return false;
        }
        AlgoritmoEncriptacion other = (AlgoritmoEncriptacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.AlgoritmoEncriptacion[ id=" + id + " ]";
    }
    
}

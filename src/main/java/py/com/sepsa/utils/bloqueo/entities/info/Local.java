/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "local", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Local.findAll", query = "SELECT l FROM Local l"),
    @NamedQuery(name = "Local.findById", query = "SELECT l FROM Local l WHERE l.localPK.id = :id"),
    @NamedQuery(name = "Local.findByIdPersona", query = "SELECT l FROM Local l WHERE l.localPK.idPersona = :idPersona"),
    @NamedQuery(name = "Local.findByDescripcion", query = "SELECT l FROM Local l WHERE l.descripcion = :descripcion"),
    @NamedQuery(name = "Local.findByGln", query = "SELECT l FROM Local l WHERE l.gln = :gln"),
    @NamedQuery(name = "Local.findByEstado", query = "SELECT l FROM Local l WHERE l.estado = :estado"),
    @NamedQuery(name = "Local.findByObservacion", query = "SELECT l FROM Local l WHERE l.observacion = :observacion"),
    @NamedQuery(name = "Local.findByFacturable", query = "SELECT l FROM Local l WHERE l.facturable = :facturable"),
    @NamedQuery(name = "Local.findByEntidad", query = "SELECT l FROM Local l WHERE l.entidad = :entidad")})
public class Local implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LocalPK localPK;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "gln")
    private BigInteger gln;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "facturable")
    private Character facturable;
    @Basic(optional = false)
    @Column(name = "entidad")
    private Character entidad;
    @JoinTable(name = "local_telefono", joinColumns = {
        @JoinColumn(name = "id_local", referencedColumnName = "id"),
        @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")}, inverseJoinColumns = {
        @JoinColumn(name = "id_telefono", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Telefono> telefonoCollection;
    @ManyToMany(mappedBy = "localCollection")
    private Collection<Email> emailCollection;
    @ManyToMany(mappedBy = "localCollection")
    private Collection<Contacto> contactoCollection;
    @JoinColumn(name = "id_direccion", referencedColumnName = "id")
    @ManyToOne
    private Direccion idDireccion;
    @JoinColumn(name = "id_persona", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Persona persona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "local")
    private Collection<Cuenta> cuentaCollection;

    public Local() {
    }

    public Local(LocalPK localPK) {
        this.localPK = localPK;
    }

    public Local(LocalPK localPK, String descripcion, BigInteger gln, Character estado, Character entidad) {
        this.localPK = localPK;
        this.descripcion = descripcion;
        this.gln = gln;
        this.estado = estado;
        this.entidad = entidad;
    }

    public Local(int id, int idPersona) {
        this.localPK = new LocalPK(id, idPersona);
    }

    public LocalPK getLocalPK() {
        return localPK;
    }

    public void setLocalPK(LocalPK localPK) {
        this.localPK = localPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Character getFacturable() {
        return facturable;
    }

    public void setFacturable(Character facturable) {
        this.facturable = facturable;
    }

    public Character getEntidad() {
        return entidad;
    }

    public void setEntidad(Character entidad) {
        this.entidad = entidad;
    }

    @XmlTransient
    public Collection<Telefono> getTelefonoCollection() {
        return telefonoCollection;
    }

    public void setTelefonoCollection(Collection<Telefono> telefonoCollection) {
        this.telefonoCollection = telefonoCollection;
    }

    @XmlTransient
    public Collection<Email> getEmailCollection() {
        return emailCollection;
    }

    public void setEmailCollection(Collection<Email> emailCollection) {
        this.emailCollection = emailCollection;
    }

    @XmlTransient
    public Collection<Contacto> getContactoCollection() {
        return contactoCollection;
    }

    public void setContactoCollection(Collection<Contacto> contactoCollection) {
        this.contactoCollection = contactoCollection;
    }

    public Direccion getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Direccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @XmlTransient
    public Collection<Cuenta> getCuentaCollection() {
        return cuentaCollection;
    }

    public void setCuentaCollection(Collection<Cuenta> cuentaCollection) {
        this.cuentaCollection = cuentaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localPK != null ? localPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Local)) {
            return false;
        }
        Local other = (Local) object;
        if ((this.localPK == null && other.localPK != null) || (this.localPK != null && !this.localPK.equals(other.localPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Local[ localPK=" + localPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cuenta_entidad_financiera", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuentaEntidadFinanciera.findAll", query = "SELECT c FROM CuentaEntidadFinanciera c"),
    @NamedQuery(name = "CuentaEntidadFinanciera.findById", query = "SELECT c FROM CuentaEntidadFinanciera c WHERE c.id = :id"),
    @NamedQuery(name = "CuentaEntidadFinanciera.findByNroCuenta", query = "SELECT c FROM CuentaEntidadFinanciera c WHERE c.nroCuenta = :nroCuenta"),
    @NamedQuery(name = "CuentaEntidadFinanciera.findByFechaAlta", query = "SELECT c FROM CuentaEntidadFinanciera c WHERE c.fechaAlta = :fechaAlta")})
public class CuentaEntidadFinanciera implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nro_cuenta")
    private String nroCuenta;
    @Basic(optional = false)
    @Column(name = "fecha_alta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAlta;
    @JoinColumn(name = "id_cuenta", referencedColumnName = "id")
    @ManyToOne
    private Cuenta idCuenta;
    @JoinColumn(name = "id_entidad_financiera", referencedColumnName = "id_entidad_financiera")
    @ManyToOne(optional = false)
    private EntidadFinanciera idEntidadFinanciera;
    @JoinColumn(name = "id_persona", referencedColumnName = "id")
    @ManyToOne
    private Persona idPersona;

    public CuentaEntidadFinanciera() {
    }

    public CuentaEntidadFinanciera(Integer id) {
        this.id = id;
    }

    public CuentaEntidadFinanciera(Integer id, String nroCuenta, Date fechaAlta) {
        this.id = id;
        this.nroCuenta = nroCuenta;
        this.fechaAlta = fechaAlta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Cuenta getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Cuenta idCuenta) {
        this.idCuenta = idCuenta;
    }

    public EntidadFinanciera getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(EntidadFinanciera idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Persona getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Persona idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuentaEntidadFinanciera)) {
            return false;
        }
        CuentaEntidadFinanciera other = (CuentaEntidadFinanciera) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.CuentaEntidadFinanciera[ id=" + id + " ]";
    }
    
}

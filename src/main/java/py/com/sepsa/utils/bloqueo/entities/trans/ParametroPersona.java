/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "parametro_persona", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParametroPersona.findAll", query = "SELECT p FROM ParametroPersona p"),
    @NamedQuery(name = "ParametroPersona.findById", query = "SELECT p FROM ParametroPersona p WHERE p.id = :id"),
    @NamedQuery(name = "ParametroPersona.findByNombre", query = "SELECT p FROM ParametroPersona p WHERE p.nombre = :nombre")})
public class ParametroPersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parametroPersona")
    private Collection<ParametroPersonaValor> parametroPersonaValorCollection;

    public ParametroPersona() {
    }

    public ParametroPersona(Integer id) {
        this.id = id;
    }

    public ParametroPersona(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<ParametroPersonaValor> getParametroPersonaValorCollection() {
        return parametroPersonaValorCollection;
    }

    public void setParametroPersonaValorCollection(Collection<ParametroPersonaValor> parametroPersonaValorCollection) {
        this.parametroPersonaValorCollection = parametroPersonaValorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParametroPersona)) {
            return false;
        }
        ParametroPersona other = (ParametroPersona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ParametroPersona[ id=" + id + " ]";
    }
    
}

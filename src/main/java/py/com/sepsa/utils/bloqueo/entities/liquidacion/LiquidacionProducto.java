/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.liquidacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "liquidacion_producto", catalog = "sepsa", schema = "liquidacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LiquidacionProducto.findAll", query = "SELECT l FROM LiquidacionProducto l"),
    @NamedQuery(name = "LiquidacionProducto.findByIdProducto", query = "SELECT l FROM LiquidacionProducto l WHERE l.idProducto = :idProducto"),
    @NamedQuery(name = "LiquidacionProducto.findByIdCliente", query = "SELECT l FROM LiquidacionProducto l WHERE l.idCliente = :idCliente"),
    @NamedQuery(name = "LiquidacionProducto.findByPlazo", query = "SELECT l FROM LiquidacionProducto l WHERE l.plazo = :plazo"),
    @NamedQuery(name = "LiquidacionProducto.findByExtension", query = "SELECT l FROM LiquidacionProducto l WHERE l.extension = :extension"),
    @NamedQuery(name = "LiquidacionProducto.findById", query = "SELECT l FROM LiquidacionProducto l WHERE l.id = :id")})
public class LiquidacionProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "plazo")
    private int plazo;
    @Basic(optional = false)
    @Column(name = "extension")
    private int extension;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public LiquidacionProducto() {
    }

    public LiquidacionProducto(Integer id) {
        this.id = id;
    }

    public LiquidacionProducto(Integer id, int idProducto, int plazo, int extension) {
        this.id = id;
        this.idProducto = idProducto;
        this.plazo = plazo;
        this.extension = extension;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int getExtension() {
        return extension;
    }

    public void setExtension(int extension) {
        this.extension = extension;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionProducto)) {
            return false;
        }
        LiquidacionProducto other = (LiquidacionProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.liquidacion.LiquidacionProducto[ id=" + id + " ]";
    }
    
}

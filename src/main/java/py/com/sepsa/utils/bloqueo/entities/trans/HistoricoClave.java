/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "historico_clave", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoClave.findAll", query = "SELECT h FROM HistoricoClave h"),
    @NamedQuery(name = "HistoricoClave.findById", query = "SELECT h FROM HistoricoClave h WHERE h.id = :id"),
    @NamedQuery(name = "HistoricoClave.findByFechaInicio", query = "SELECT h FROM HistoricoClave h WHERE h.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "HistoricoClave.findByFechaFin", query = "SELECT h FROM HistoricoClave h WHERE h.fechaFin = :fechaFin"),
    @NamedQuery(name = "HistoricoClave.findByClave", query = "SELECT h FROM HistoricoClave h WHERE h.clave = :clave")})
public class HistoricoClave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Column(name = "clave")
    private String clave;
    @JoinColumn(name = "id_algoritmo_encriptacion", referencedColumnName = "id")
    @ManyToOne
    private AlgoritmoEncriptacion idAlgoritmoEncriptacion;
    @JoinColumn(name = "id_instalacion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Instalacion idInstalacion;

    public HistoricoClave() {
    }

    public HistoricoClave(Integer id) {
        this.id = id;
    }

    public HistoricoClave(Integer id, Date fechaInicio) {
        this.id = id;
        this.fechaInicio = fechaInicio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public AlgoritmoEncriptacion getIdAlgoritmoEncriptacion() {
        return idAlgoritmoEncriptacion;
    }

    public void setIdAlgoritmoEncriptacion(AlgoritmoEncriptacion idAlgoritmoEncriptacion) {
        this.idAlgoritmoEncriptacion = idAlgoritmoEncriptacion;
    }

    public Instalacion getIdInstalacion() {
        return idInstalacion;
    }

    public void setIdInstalacion(Instalacion idInstalacion) {
        this.idInstalacion = idInstalacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoClave)) {
            return false;
        }
        HistoricoClave other = (HistoricoClave) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.HistoricoClave[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "sociedad_cliente_local", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SociedadClienteLocal.findAll", query = "SELECT s FROM SociedadClienteLocal s"),
    @NamedQuery(name = "SociedadClienteLocal.findByIdSociedad", query = "SELECT s FROM SociedadClienteLocal s WHERE s.sociedadClienteLocalPK.idSociedad = :idSociedad"),
    @NamedQuery(name = "SociedadClienteLocal.findByIdPersona", query = "SELECT s FROM SociedadClienteLocal s WHERE s.sociedadClienteLocalPK.idPersona = :idPersona"),
    @NamedQuery(name = "SociedadClienteLocal.findByIdLocal", query = "SELECT s FROM SociedadClienteLocal s WHERE s.sociedadClienteLocalPK.idLocal = :idLocal")})
public class SociedadClienteLocal implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SociedadClienteLocalPK sociedadClienteLocalPK;
    @JoinColumn(name = "id_sociedad", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SociedadCliente sociedadCliente;

    public SociedadClienteLocal() {
    }

    public SociedadClienteLocal(SociedadClienteLocalPK sociedadClienteLocalPK) {
        this.sociedadClienteLocalPK = sociedadClienteLocalPK;
    }

    public SociedadClienteLocal(int idSociedad, int idPersona, int idLocal) {
        this.sociedadClienteLocalPK = new SociedadClienteLocalPK(idSociedad, idPersona, idLocal);
    }

    public SociedadClienteLocalPK getSociedadClienteLocalPK() {
        return sociedadClienteLocalPK;
    }

    public void setSociedadClienteLocalPK(SociedadClienteLocalPK sociedadClienteLocalPK) {
        this.sociedadClienteLocalPK = sociedadClienteLocalPK;
    }

    public SociedadCliente getSociedadCliente() {
        return sociedadCliente;
    }

    public void setSociedadCliente(SociedadCliente sociedadCliente) {
        this.sociedadCliente = sociedadCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sociedadClienteLocalPK != null ? sociedadClienteLocalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SociedadClienteLocal)) {
            return false;
        }
        SociedadClienteLocal other = (SociedadClienteLocal) object;
        if ((this.sociedadClienteLocalPK == null && other.sociedadClienteLocalPK != null) || (this.sociedadClienteLocalPK != null && !this.sociedadClienteLocalPK.equals(other.sociedadClienteLocalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.SociedadClienteLocal[ sociedadClienteLocalPK=" + sociedadClienteLocalPK + " ]";
    }
    
}

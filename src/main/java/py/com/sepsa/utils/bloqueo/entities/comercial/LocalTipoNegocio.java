/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "local_tipo_negocio", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalTipoNegocio.findAll", query = "SELECT l FROM LocalTipoNegocio l"),
    @NamedQuery(name = "LocalTipoNegocio.findByIdLocal", query = "SELECT l FROM LocalTipoNegocio l WHERE l.localTipoNegocioPK.idLocal = :idLocal"),
    @NamedQuery(name = "LocalTipoNegocio.findByIdPersona", query = "SELECT l FROM LocalTipoNegocio l WHERE l.localTipoNegocioPK.idPersona = :idPersona"),
    @NamedQuery(name = "LocalTipoNegocio.findByIdTipoNegocio", query = "SELECT l FROM LocalTipoNegocio l WHERE l.localTipoNegocioPK.idTipoNegocio = :idTipoNegocio")})
public class LocalTipoNegocio implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LocalTipoNegocioPK localTipoNegocioPK;
    @JoinColumn(name = "id_tipo_negocio", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TipoNegocio tipoNegocio;

    public LocalTipoNegocio() {
    }

    public LocalTipoNegocio(LocalTipoNegocioPK localTipoNegocioPK) {
        this.localTipoNegocioPK = localTipoNegocioPK;
    }

    public LocalTipoNegocio(int idLocal, int idPersona, int idTipoNegocio) {
        this.localTipoNegocioPK = new LocalTipoNegocioPK(idLocal, idPersona, idTipoNegocio);
    }

    public LocalTipoNegocioPK getLocalTipoNegocioPK() {
        return localTipoNegocioPK;
    }

    public void setLocalTipoNegocioPK(LocalTipoNegocioPK localTipoNegocioPK) {
        this.localTipoNegocioPK = localTipoNegocioPK;
    }

    public TipoNegocio getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(TipoNegocio tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localTipoNegocioPK != null ? localTipoNegocioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalTipoNegocio)) {
            return false;
        }
        LocalTipoNegocio other = (LocalTipoNegocio) object;
        if ((this.localTipoNegocioPK == null && other.localTipoNegocioPK != null) || (this.localTipoNegocioPK != null && !this.localTipoNegocioPK.equals(other.localTipoNegocioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.LocalTipoNegocio[ localTipoNegocioPK=" + localTipoNegocioPK + " ]";
    }
    
}

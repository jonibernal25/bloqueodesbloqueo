/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "documento_producto", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocumentoProducto.findAll", query = "SELECT d FROM DocumentoProducto d"),
    @NamedQuery(name = "DocumentoProducto.findByIdDocumento", query = "SELECT d FROM DocumentoProducto d WHERE d.documentoProductoPK.idDocumento = :idDocumento"),
    @NamedQuery(name = "DocumentoProducto.findByIdTipoDocumento", query = "SELECT d FROM DocumentoProducto d WHERE d.documentoProductoPK.idTipoDocumento = :idTipoDocumento"),
    @NamedQuery(name = "DocumentoProducto.findByIdProducto", query = "SELECT d FROM DocumentoProducto d WHERE d.documentoProductoPK.idProducto = :idProducto")})
public class DocumentoProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DocumentoProductoPK documentoProductoPK;
    @JoinColumns({
        @JoinColumn(name = "id_documento", referencedColumnName = "id", insertable = false, updatable = false),
        @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Documento documento;

    public DocumentoProducto() {
    }

    public DocumentoProducto(DocumentoProductoPK documentoProductoPK) {
        this.documentoProductoPK = documentoProductoPK;
    }

    public DocumentoProducto(int idDocumento, int idTipoDocumento, int idProducto) {
        this.documentoProductoPK = new DocumentoProductoPK(idDocumento, idTipoDocumento, idProducto);
    }

    public DocumentoProductoPK getDocumentoProductoPK() {
        return documentoProductoPK;
    }

    public void setDocumentoProductoPK(DocumentoProductoPK documentoProductoPK) {
        this.documentoProductoPK = documentoProductoPK;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentoProductoPK != null ? documentoProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoProducto)) {
            return false;
        }
        DocumentoProducto other = (DocumentoProducto) object;
        if ((this.documentoProductoPK == null && other.documentoProductoPK != null) || (this.documentoProductoPK != null && !this.documentoProductoPK.equals(other.documentoProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.DocumentoProducto[ documentoProductoPK=" + documentoProductoPK + " ]";
    }
    
}

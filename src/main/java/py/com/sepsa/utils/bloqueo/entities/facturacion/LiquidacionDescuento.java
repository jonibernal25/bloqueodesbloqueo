/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "liquidacion_descuento", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LiquidacionDescuento.findAll", query = "SELECT l FROM LiquidacionDescuento l"),
    @NamedQuery(name = "LiquidacionDescuento.findByIdLiquidacion", query = "SELECT l FROM LiquidacionDescuento l WHERE l.liquidacionDescuentoPK.idLiquidacion = :idLiquidacion"),
    @NamedQuery(name = "LiquidacionDescuento.findByIdDescuento", query = "SELECT l FROM LiquidacionDescuento l WHERE l.liquidacionDescuentoPK.idDescuento = :idDescuento")})
public class LiquidacionDescuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LiquidacionDescuentoPK liquidacionDescuentoPK;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Liquidacion liquidacion;

    public LiquidacionDescuento() {
    }

    public LiquidacionDescuento(LiquidacionDescuentoPK liquidacionDescuentoPK) {
        this.liquidacionDescuentoPK = liquidacionDescuentoPK;
    }

    public LiquidacionDescuento(int idLiquidacion, int idDescuento) {
        this.liquidacionDescuentoPK = new LiquidacionDescuentoPK(idLiquidacion, idDescuento);
    }

    public LiquidacionDescuentoPK getLiquidacionDescuentoPK() {
        return liquidacionDescuentoPK;
    }

    public void setLiquidacionDescuentoPK(LiquidacionDescuentoPK liquidacionDescuentoPK) {
        this.liquidacionDescuentoPK = liquidacionDescuentoPK;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liquidacionDescuentoPK != null ? liquidacionDescuentoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionDescuento)) {
            return false;
        }
        LiquidacionDescuento other = (LiquidacionDescuento) object;
        if ((this.liquidacionDescuentoPK == null && other.liquidacionDescuentoPK != null) || (this.liquidacionDescuentoPK != null && !this.liquidacionDescuentoPK.equals(other.liquidacionDescuentoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.LiquidacionDescuento[ liquidacionDescuentoPK=" + liquidacionDescuentoPK + " ]";
    }
    
}

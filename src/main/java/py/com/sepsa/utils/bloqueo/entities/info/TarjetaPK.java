/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class TarjetaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "nro_tarjeta")
    private String nroTarjeta;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;

    public TarjetaPK() {
    }

    public TarjetaPK(String nroTarjeta, int idPersona) {
        this.nroTarjeta = nroTarjeta;
        this.idPersona = idPersona;
    }

    public String getNroTarjeta() {
        return nroTarjeta;
    }

    public void setNroTarjeta(String nroTarjeta) {
        this.nroTarjeta = nroTarjeta;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nroTarjeta != null ? nroTarjeta.hashCode() : 0);
        hash += (int) idPersona;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TarjetaPK)) {
            return false;
        }
        TarjetaPK other = (TarjetaPK) object;
        if ((this.nroTarjeta == null && other.nroTarjeta != null) || (this.nroTarjeta != null && !this.nroTarjeta.equals(other.nroTarjeta))) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.TarjetaPK[ nroTarjeta=" + nroTarjeta + ", idPersona=" + idPersona + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "telefono", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Telefono.findAll", query = "SELECT t FROM Telefono t"),
    @NamedQuery(name = "Telefono.findById", query = "SELECT t FROM Telefono t WHERE t.id = :id"),
    @NamedQuery(name = "Telefono.findByNumero", query = "SELECT t FROM Telefono t WHERE t.numero = :numero"),
    @NamedQuery(name = "Telefono.findByPrincipal", query = "SELECT t FROM Telefono t WHERE t.principal = :principal")})
public class Telefono implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "numero")
    private String numero;
    @Basic(optional = false)
    @Column(name = "principal")
    private Character principal;
    @ManyToMany(mappedBy = "telefonoCollection")
    private Collection<Persona> personaCollection;
    @ManyToMany(mappedBy = "telefonoCollection")
    private Collection<Local> localCollection;
    @JoinColumn(name = "id_cuenta", referencedColumnName = "id")
    @ManyToOne
    private Cuenta idCuenta;
    @JoinColumn(name = "id_prefijo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private PrefijoTelefono idPrefijo;
    @JoinColumn(name = "id_tipo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoTelefono idTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "telefono")
    private Collection<ContactoTelefono> contactoTelefonoCollection;

    public Telefono() {
    }

    public Telefono(Integer id) {
        this.id = id;
    }

    public Telefono(Integer id, String numero, Character principal) {
        this.id = id;
        this.numero = numero;
        this.principal = principal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    @XmlTransient
    public Collection<Local> getLocalCollection() {
        return localCollection;
    }

    public void setLocalCollection(Collection<Local> localCollection) {
        this.localCollection = localCollection;
    }

    public Cuenta getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Cuenta idCuenta) {
        this.idCuenta = idCuenta;
    }

    public PrefijoTelefono getIdPrefijo() {
        return idPrefijo;
    }

    public void setIdPrefijo(PrefijoTelefono idPrefijo) {
        this.idPrefijo = idPrefijo;
    }

    public TipoTelefono getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(TipoTelefono idTipo) {
        this.idTipo = idTipo;
    }

    @XmlTransient
    public Collection<ContactoTelefono> getContactoTelefonoCollection() {
        return contactoTelefonoCollection;
    }

    public void setContactoTelefonoCollection(Collection<ContactoTelefono> contactoTelefonoCollection) {
        this.contactoTelefonoCollection = contactoTelefonoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telefono)) {
            return false;
        }
        Telefono other = (Telefono) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Telefono[ id=" + id + " ]";
    }
    
}

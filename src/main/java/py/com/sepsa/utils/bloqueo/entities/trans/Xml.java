/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "xml", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Xml.findAll", query = "SELECT x FROM Xml x"),
    @NamedQuery(name = "Xml.findById", query = "SELECT x FROM Xml x WHERE x.id = :id"),
    @NamedQuery(name = "Xml.findByNombre", query = "SELECT x FROM Xml x WHERE x.nombre = :nombre"),
    @NamedQuery(name = "Xml.findByEtiqueta", query = "SELECT x FROM Xml x WHERE x.etiqueta = :etiqueta"),
    @NamedQuery(name = "Xml.findByAtributo", query = "SELECT x FROM Xml x WHERE x.atributo = :atributo")})
public class Xml implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "etiqueta")
    private String etiqueta;
    @Basic(optional = false)
    @Column(name = "atributo")
    private Character atributo;
    @JoinColumn(name = "id_tipo_dato", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoDato idTipoDato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "xml")
    private Collection<ConfiguracionXml> configuracionXmlCollection;

    public Xml() {
    }

    public Xml(Integer id) {
        this.id = id;
    }

    public Xml(Integer id, String nombre, String etiqueta, Character atributo) {
        this.id = id;
        this.nombre = nombre;
        this.etiqueta = etiqueta;
        this.atributo = atributo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public Character getAtributo() {
        return atributo;
    }

    public void setAtributo(Character atributo) {
        this.atributo = atributo;
    }

    public TipoDato getIdTipoDato() {
        return idTipoDato;
    }

    public void setIdTipoDato(TipoDato idTipoDato) {
        this.idTipoDato = idTipoDato;
    }

    @XmlTransient
    public Collection<ConfiguracionXml> getConfiguracionXmlCollection() {
        return configuracionXmlCollection;
    }

    public void setConfiguracionXmlCollection(Collection<ConfiguracionXml> configuracionXmlCollection) {
        this.configuracionXmlCollection = configuracionXmlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Xml)) {
            return false;
        }
        Xml other = (Xml) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.Xml[ id=" + id + " ]";
    }
    
}

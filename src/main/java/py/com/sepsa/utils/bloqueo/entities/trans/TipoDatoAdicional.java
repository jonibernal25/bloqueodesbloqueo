/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tipo_dato_adicional", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDatoAdicional.findAll", query = "SELECT t FROM TipoDatoAdicional t"),
    @NamedQuery(name = "TipoDatoAdicional.findById", query = "SELECT t FROM TipoDatoAdicional t WHERE t.id = :id"),
    @NamedQuery(name = "TipoDatoAdicional.findByDescripcion", query = "SELECT t FROM TipoDatoAdicional t WHERE t.descripcion = :descripcion")})
public class TipoDatoAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_tipo_dato", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoDato idTipoDato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDatoAdicional")
    private Collection<DatoAdicional> datoAdicionalCollection;

    public TipoDatoAdicional() {
    }

    public TipoDatoAdicional(Integer id) {
        this.id = id;
    }

    public TipoDatoAdicional(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoDato getIdTipoDato() {
        return idTipoDato;
    }

    public void setIdTipoDato(TipoDato idTipoDato) {
        this.idTipoDato = idTipoDato;
    }

    @XmlTransient
    public Collection<DatoAdicional> getDatoAdicionalCollection() {
        return datoAdicionalCollection;
    }

    public void setDatoAdicionalCollection(Collection<DatoAdicional> datoAdicionalCollection) {
        this.datoAdicionalCollection = datoAdicionalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDatoAdicional)) {
            return false;
        }
        TipoDatoAdicional other = (TipoDatoAdicional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.TipoDatoAdicional[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class DocumentoProductoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_documento")
    private int idDocumento;
    @Basic(optional = false)
    @Column(name = "id_tipo_documento")
    private int idTipoDocumento;
    @Basic(optional = false)
    @Column(name = "id_producto")
    private int idProducto;

    public DocumentoProductoPK() {
    }

    public DocumentoProductoPK(int idDocumento, int idTipoDocumento, int idProducto) {
        this.idDocumento = idDocumento;
        this.idTipoDocumento = idTipoDocumento;
        this.idProducto = idProducto;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idDocumento;
        hash += (int) idTipoDocumento;
        hash += (int) idProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoProductoPK)) {
            return false;
        }
        DocumentoProductoPK other = (DocumentoProductoPK) object;
        if (this.idDocumento != other.idDocumento) {
            return false;
        }
        if (this.idTipoDocumento != other.idTipoDocumento) {
            return false;
        }
        if (this.idProducto != other.idProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.DocumentoProductoPK[ idDocumento=" + idDocumento + ", idTipoDocumento=" + idTipoDocumento + ", idProducto=" + idProducto + " ]";
    }
    
}

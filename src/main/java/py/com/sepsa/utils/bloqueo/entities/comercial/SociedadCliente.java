/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "sociedad_cliente", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SociedadCliente.findAll", query = "SELECT s FROM SociedadCliente s"),
    @NamedQuery(name = "SociedadCliente.findById", query = "SELECT s FROM SociedadCliente s WHERE s.id = :id"),
    @NamedQuery(name = "SociedadCliente.findByDescripcion", query = "SELECT s FROM SociedadCliente s WHERE s.descripcion = :descripcion")})
public class SociedadCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sociedadCliente")
    private Collection<ProveedorSociedad> proveedorSociedadCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sociedadCliente")
    private Collection<SociedadClienteLocal> sociedadClienteLocalCollection;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne(optional = false)
    private Cliente idCliente;

    public SociedadCliente() {
    }

    public SociedadCliente(Integer id) {
        this.id = id;
    }

    public SociedadCliente(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<ProveedorSociedad> getProveedorSociedadCollection() {
        return proveedorSociedadCollection;
    }

    public void setProveedorSociedadCollection(Collection<ProveedorSociedad> proveedorSociedadCollection) {
        this.proveedorSociedadCollection = proveedorSociedadCollection;
    }

    @XmlTransient
    public Collection<SociedadClienteLocal> getSociedadClienteLocalCollection() {
        return sociedadClienteLocalCollection;
    }

    public void setSociedadClienteLocalCollection(Collection<SociedadClienteLocal> sociedadClienteLocalCollection) {
        this.sociedadClienteLocalCollection = sociedadClienteLocalCollection;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SociedadCliente)) {
            return false;
        }
        SociedadCliente other = (SociedadCliente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.SociedadCliente[ id=" + id + " ]";
    }
    
}

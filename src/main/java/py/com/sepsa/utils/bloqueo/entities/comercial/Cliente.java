/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cliente", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByIdCliente", query = "SELECT c FROM Cliente c WHERE c.idCliente = :idCliente"),
    @NamedQuery(name = "Cliente.findByIdComercial", query = "SELECT c FROM Cliente c WHERE c.idComercial = :idComercial"),
    @NamedQuery(name = "Cliente.findBySaldo", query = "SELECT c FROM Cliente c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "Cliente.findByEstado", query = "SELECT c FROM Cliente c WHERE c.estado = :estado"),
    @NamedQuery(name = "Cliente.findByNroPlanCuenta", query = "SELECT c FROM Cliente c WHERE c.nroPlanCuenta = :nroPlanCuenta"),
    @NamedQuery(name = "Cliente.findByIdGrupoImpresion", query = "SELECT c FROM Cliente c WHERE c.idGrupoImpresion = :idGrupoImpresion"),
    @NamedQuery(name = "Cliente.findByPorcentajeRetencion", query = "SELECT c FROM Cliente c WHERE c.porcentajeRetencion = :porcentajeRetencion")})
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Column(name = "id_comercial")
    private Integer idComercial;
    @Basic(optional = false)
    @Column(name = "saldo")
    private BigInteger saldo;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "nro_plan_cuenta")
    private String nroPlanCuenta;
    @Column(name = "id_grupo_impresion")
    private Integer idGrupoImpresion;
    @Basic(optional = false)
    @Column(name = "porcentaje_retencion")
    private int porcentajeRetencion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private Collection<ClienteProducto> clienteProductoCollection;
    @OneToMany(mappedBy = "idCadena")
    private Collection<Descuento> descuentoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCliente")
    private Collection<HistoricoCliente> historicoClienteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCliente")
    private Collection<Contrato> contratoCollection;
    @JoinColumn(name = "id_calificacion", referencedColumnName = "id")
    @ManyToOne
    private Calificacion idCalificacion;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id")
    @ManyToOne
    private Categoria idCategoria;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id")
    @ManyToOne
    private GrupoCliente idGrupo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCliente")
    private Collection<SociedadCliente> sociedadClienteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private Collection<ClienteTipoNegocio> clienteTipoNegocioCollection;

    public Cliente() {
    }

    public Cliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente(Integer idCliente, BigInteger saldo, Character estado, int porcentajeRetencion) {
        this.idCliente = idCliente;
        this.saldo = saldo;
        this.estado = estado;
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public BigInteger getSaldo() {
        return saldo;
    }

    public void setSaldo(BigInteger saldo) {
        this.saldo = saldo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getNroPlanCuenta() {
        return nroPlanCuenta;
    }

    public void setNroPlanCuenta(String nroPlanCuenta) {
        this.nroPlanCuenta = nroPlanCuenta;
    }

    public Integer getIdGrupoImpresion() {
        return idGrupoImpresion;
    }

    public void setIdGrupoImpresion(Integer idGrupoImpresion) {
        this.idGrupoImpresion = idGrupoImpresion;
    }

    public int getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(int porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    @XmlTransient
    public Collection<ClienteProducto> getClienteProductoCollection() {
        return clienteProductoCollection;
    }

    public void setClienteProductoCollection(Collection<ClienteProducto> clienteProductoCollection) {
        this.clienteProductoCollection = clienteProductoCollection;
    }

    @XmlTransient
    public Collection<Descuento> getDescuentoCollection() {
        return descuentoCollection;
    }

    public void setDescuentoCollection(Collection<Descuento> descuentoCollection) {
        this.descuentoCollection = descuentoCollection;
    }

    @XmlTransient
    public Collection<HistoricoCliente> getHistoricoClienteCollection() {
        return historicoClienteCollection;
    }

    public void setHistoricoClienteCollection(Collection<HistoricoCliente> historicoClienteCollection) {
        this.historicoClienteCollection = historicoClienteCollection;
    }

    @XmlTransient
    public Collection<Contrato> getContratoCollection() {
        return contratoCollection;
    }

    public void setContratoCollection(Collection<Contrato> contratoCollection) {
        this.contratoCollection = contratoCollection;
    }

    public Calificacion getIdCalificacion() {
        return idCalificacion;
    }

    public void setIdCalificacion(Calificacion idCalificacion) {
        this.idCalificacion = idCalificacion;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public GrupoCliente getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(GrupoCliente idGrupo) {
        this.idGrupo = idGrupo;
    }

    @XmlTransient
    public Collection<SociedadCliente> getSociedadClienteCollection() {
        return sociedadClienteCollection;
    }

    public void setSociedadClienteCollection(Collection<SociedadCliente> sociedadClienteCollection) {
        this.sociedadClienteCollection = sociedadClienteCollection;
    }

    @XmlTransient
    public Collection<ClienteTipoNegocio> getClienteTipoNegocioCollection() {
        return clienteTipoNegocioCollection;
    }

    public void setClienteTipoNegocioCollection(Collection<ClienteTipoNegocio> clienteTipoNegocioCollection) {
        this.clienteTipoNegocioCollection = clienteTipoNegocioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCliente != null ? idCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idCliente == null && other.idCliente != null) || (this.idCliente != null && !this.idCliente.equals(other.idCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Cliente[ idCliente=" + idCliente + " ]";
    }
    
}

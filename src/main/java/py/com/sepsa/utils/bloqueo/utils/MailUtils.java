/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.utils;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

/**
 * Clase utilizada para el manejo de envio de mail
 *
 * @author Rubén Ortiz
 */
//@Singleton
//@Startup
public class MailUtils {

    /**
     * Sesión utilizada para el envio de mail
     */
    private static Session sesion;

    /**
     * Inicializa los valores para el envio de mail
     */
    static {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        MyAuthentificator myAuth = new MyAuthentificator();
        sesion = Session.getInstance(props, myAuth);
    }

    /**
     * Envío de mail junto con el adjunto correspondiente
     *
     * @param to Mail destinatario
     * @param msgContent Contenido del Mail
     * @param subject Asunto del Mail
     * @param mimeType MimeType
     * @throws MessagingException
     */
    public static void send(List<Report> to, String msgContent, String subject, MimeType mimeType) throws MessagingException {
        Transport t = sesion.getTransport("smtp");
        t.connect();
        for (int i = 0; i < to.size(); i++) {
            for (int k = 0; k < to.get(i).getMailToList().size(); k++) {
                MimeMessage mMessage = new MimeMessage(sesion);
                mMessage.setSentDate(Calendar.getInstance().getTime());
                mMessage.setSubject(subject);
                mMessage.setFrom(new InternetAddress("alertas@sepsa.com.py"));
                mMessage.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(to.get(i).getMailToList().get(k).trim()));
                MimeMultipart mp = new MimeMultipart("related");
                MimeBodyPart messagePart = new MimeBodyPart();
                messagePart.setContent(msgContent, mimeType.value);
                mp.addBodyPart(messagePart);
                mMessage.setContent(mp);
                t.sendMessage(mMessage, mMessage.getAllRecipients());
            }
        }
        t.close();
    }

    /**
     * Envío de mail junto con el adjunto correspondiente
     *
     * @param to Mail destinatario
     * @param mimeMultiPart MimeMultiPart
     * @param subject Asunto del Mail
     * @throws MessagingException
     */
    public static void send(List<Report> to, MimeMultipart mimeMultiPart, String subject) throws MessagingException {
        Transport t = sesion.getTransport("smtp");
        t.connect();
        for (int i = 0; i < to.size(); i++) {
            for (int k = 0; k < to.get(i).getMailToList().size(); k++) {
                MimeMessage mMessage = new MimeMessage(sesion);
                mMessage.setSentDate(Calendar.getInstance().getTime());
                mMessage.setSubject(subject);
                mMessage.setFrom(new InternetAddress("alertas@sepsa.com.py"));
                mMessage.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(to.get(i).getMailToList().get(k).trim()));
                mMessage.setContent(mimeMultiPart);
                t.sendMessage(mMessage, mMessage.getAllRecipients());
            }
        }
        t.close();
    }

    /**
     * Envío de mail junto con el adjunto correspondiente
     *
     * @param to Mail destinatario
     * @param msgContent Contenido del Mail
     * @param subject Asunto del Mail
     * @throws MessagingException
     */
    public static void send(List<Report> to, String msgContent, String subject) throws MessagingException {
        send(to, msgContent, subject, MimeType.TXT);
    }
    
    public enum MimeType {
        TXT("text/plain; charset=UTF-8"),
        PDF("application/pdf"),
        XLS("application/vnd.ms-excel");
        
        /**
         * Valor del tipo MIME
         */
        private String value;

        /**
         * Obtiene el valor MIME
         */
        public String getValue() {
            return value;
        }

        /**
         * Setea el valor MIME
         */
        public void setValue(String value) {
            this.value = value;
        }
        
        private MimeType(String value) {
            this.value = value;
        }
    }
}

/**
 * Autenticador del mail
 */
class MyAuthentificator extends Authenticator {

    /**
     * Obtiene la autenticación
     *
     * @return Autenticación del mail
     */
    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication("alertas@sepsa.com.py", "SEPSA12345");
    }
}

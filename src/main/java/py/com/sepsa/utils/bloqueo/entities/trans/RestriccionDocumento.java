/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "restriccion_documento", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RestriccionDocumento.findAll", query = "SELECT r FROM RestriccionDocumento r"),
    @NamedQuery(name = "RestriccionDocumento.findById", query = "SELECT r FROM RestriccionDocumento r WHERE r.id = :id"),
    @NamedQuery(name = "RestriccionDocumento.findByIdPersonaOrigen", query = "SELECT r FROM RestriccionDocumento r WHERE r.idPersonaOrigen = :idPersonaOrigen"),
    @NamedQuery(name = "RestriccionDocumento.findByIdLocalOrigen", query = "SELECT r FROM RestriccionDocumento r WHERE r.idLocalOrigen = :idLocalOrigen"),
    @NamedQuery(name = "RestriccionDocumento.findByIdPersonaDestino", query = "SELECT r FROM RestriccionDocumento r WHERE r.idPersonaDestino = :idPersonaDestino"),
    @NamedQuery(name = "RestriccionDocumento.findByIdLocalDestino", query = "SELECT r FROM RestriccionDocumento r WHERE r.idLocalDestino = :idLocalDestino")})
public class RestriccionDocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_persona_origen")
    private Integer idPersonaOrigen;
    @Column(name = "id_local_origen")
    private Integer idLocalOrigen;
    @Column(name = "id_persona_destino")
    private Integer idPersonaDestino;
    @Column(name = "id_local_destino")
    private Integer idLocalDestino;

    public RestriccionDocumento() {
    }

    public RestriccionDocumento(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPersonaOrigen() {
        return idPersonaOrigen;
    }

    public void setIdPersonaOrigen(Integer idPersonaOrigen) {
        this.idPersonaOrigen = idPersonaOrigen;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdPersonaDestino() {
        return idPersonaDestino;
    }

    public void setIdPersonaDestino(Integer idPersonaDestino) {
        this.idPersonaDestino = idPersonaDestino;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RestriccionDocumento)) {
            return false;
        }
        RestriccionDocumento other = (RestriccionDocumento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.RestriccionDocumento[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ProveedorCompradorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private int idProveedor;
    @Basic(optional = false)
    @Column(name = "id_comprador")
    private int idComprador;

    public ProveedorCompradorPK() {
    }

    public ProveedorCompradorPK(int idProveedor, int idComprador) {
        this.idProveedor = idProveedor;
        this.idComprador = idComprador;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProveedor;
        hash += (int) idComprador;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorCompradorPK)) {
            return false;
        }
        ProveedorCompradorPK other = (ProveedorCompradorPK) object;
        if (this.idProveedor != other.idProveedor) {
            return false;
        }
        if (this.idComprador != other.idComprador) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ProveedorCompradorPK[ idProveedor=" + idProveedor + ", idComprador=" + idComprador + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class LiquidacionDetallePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_liquidacion")
    private int idLiquidacion;
    @Basic(optional = false)
    @Column(name = "nro_linea")
    private int nroLinea;

    public LiquidacionDetallePK() {
    }

    public LiquidacionDetallePK(int idLiquidacion, int nroLinea) {
        this.idLiquidacion = idLiquidacion;
        this.nroLinea = nroLinea;
    }

    public int getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(int idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public int getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(int nroLinea) {
        this.nroLinea = nroLinea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idLiquidacion;
        hash += (int) nroLinea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionDetallePK)) {
            return false;
        }
        LiquidacionDetallePK other = (LiquidacionDetallePK) object;
        if (this.idLiquidacion != other.idLiquidacion) {
            return false;
        }
        if (this.nroLinea != other.nroLinea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.LiquidacionDetallePK[ idLiquidacion=" + idLiquidacion + ", nroLinea=" + nroLinea + " ]";
    }
    
}

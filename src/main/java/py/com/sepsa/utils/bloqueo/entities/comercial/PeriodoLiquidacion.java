/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "periodo_liquidacion", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PeriodoLiquidacion.findAll", query = "SELECT p FROM PeriodoLiquidacion p"),
    @NamedQuery(name = "PeriodoLiquidacion.findById", query = "SELECT p FROM PeriodoLiquidacion p WHERE p.id = :id"),
    @NamedQuery(name = "PeriodoLiquidacion.findByFechaInicio", query = "SELECT p FROM PeriodoLiquidacion p WHERE p.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "PeriodoLiquidacion.findByFechaFin", query = "SELECT p FROM PeriodoLiquidacion p WHERE p.fechaFin = :fechaFin"),
    @NamedQuery(name = "PeriodoLiquidacion.findByEstado", query = "SELECT p FROM PeriodoLiquidacion p WHERE p.estado = :estado")})
public class PeriodoLiquidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Column(name = "estado")
    private Character estado;

    public PeriodoLiquidacion() {
    }

    public PeriodoLiquidacion(Integer id) {
        this.id = id;
    }

    public PeriodoLiquidacion(Integer id, Date fechaInicio, Date fechaFin) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeriodoLiquidacion)) {
            return false;
        }
        PeriodoLiquidacion other = (PeriodoLiquidacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.PeriodoLiquidacion[ id=" + id + " ]";
    }
    
}

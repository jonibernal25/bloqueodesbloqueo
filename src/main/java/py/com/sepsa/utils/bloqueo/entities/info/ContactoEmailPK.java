/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ContactoEmailPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_email")
    private int idEmail;
    @Basic(optional = false)
    @Column(name = "id_contacto")
    private int idContacto;

    public ContactoEmailPK() {
    }

    public ContactoEmailPK(int idEmail, int idContacto) {
        this.idEmail = idEmail;
        this.idContacto = idContacto;
    }

    public int getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(int idEmail) {
        this.idEmail = idEmail;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idEmail;
        hash += (int) idContacto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactoEmailPK)) {
            return false;
        }
        ContactoEmailPK other = (ContactoEmailPK) object;
        if (this.idEmail != other.idEmail) {
            return false;
        }
        if (this.idContacto != other.idContacto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.ContactoEmailPK[ idEmail=" + idEmail + ", idContacto=" + idContacto + " ]";
    }
    
}

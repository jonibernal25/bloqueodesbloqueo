/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ServicioSoftwarePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_servicio")
    private int idServicio;
    @Basic(optional = false)
    @Column(name = "id_software")
    private int idSoftware;

    public ServicioSoftwarePK() {
    }

    public ServicioSoftwarePK(int idServicio, int idSoftware) {
        this.idServicio = idServicio;
        this.idSoftware = idSoftware;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public int getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(int idSoftware) {
        this.idSoftware = idSoftware;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idServicio;
        hash += (int) idSoftware;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicioSoftwarePK)) {
            return false;
        }
        ServicioSoftwarePK other = (ServicioSoftwarePK) object;
        if (this.idServicio != other.idServicio) {
            return false;
        }
        if (this.idSoftware != other.idSoftware) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ServicioSoftwarePK[ idServicio=" + idServicio + ", idSoftware=" + idSoftware + " ]";
    }
    
}

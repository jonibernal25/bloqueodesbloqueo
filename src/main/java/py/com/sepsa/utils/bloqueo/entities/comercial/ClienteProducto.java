/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "cliente_producto", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClienteProducto.findAll", query = "SELECT c FROM ClienteProducto c"),
    @NamedQuery(name = "ClienteProducto.findByIdProducto", query = "SELECT c FROM ClienteProducto c WHERE c.clienteProductoPK.idProducto = :idProducto"),
    @NamedQuery(name = "ClienteProducto.findByIdCliente", query = "SELECT c FROM ClienteProducto c WHERE c.clienteProductoPK.idCliente = :idCliente"),
    @NamedQuery(name = "ClienteProducto.findByEstado", query = "SELECT c FROM ClienteProducto c WHERE c.estado = :estado"),
    @NamedQuery(name = "ClienteProducto.findByFacturable", query = "SELECT c FROM ClienteProducto c WHERE c.facturable = :facturable")})
public class ClienteProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClienteProductoPK clienteProductoPK;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "facturable")
    private Character facturable;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;

    public ClienteProducto() {
    }

    public ClienteProducto(ClienteProductoPK clienteProductoPK) {
        this.clienteProductoPK = clienteProductoPK;
    }

    public ClienteProducto(ClienteProductoPK clienteProductoPK, Character estado) {
        this.clienteProductoPK = clienteProductoPK;
        this.estado = estado;
    }

    public ClienteProducto(int idProducto, int idCliente) {
        this.clienteProductoPK = new ClienteProductoPK(idProducto, idCliente);
    }

    public ClienteProductoPK getClienteProductoPK() {
        return clienteProductoPK;
    }

    public void setClienteProductoPK(ClienteProductoPK clienteProductoPK) {
        this.clienteProductoPK = clienteProductoPK;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getFacturable() {
        return facturable;
    }

    public void setFacturable(Character facturable) {
        this.facturable = facturable;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clienteProductoPK != null ? clienteProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteProducto)) {
            return false;
        }
        ClienteProducto other = (ClienteProducto) object;
        if ((this.clienteProductoPK == null && other.clienteProductoPK != null) || (this.clienteProductoPK != null && !this.clienteProductoPK.equals(other.clienteProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ClienteProducto[ clienteProductoPK=" + clienteProductoPK + " ]";
    }
    
}

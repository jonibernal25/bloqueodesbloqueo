/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "email", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Email.findAll", query = "SELECT e FROM Email e"),
    @NamedQuery(name = "Email.findById", query = "SELECT e FROM Email e WHERE e.id = :id"),
    @NamedQuery(name = "Email.findByEmail", query = "SELECT e FROM Email e WHERE e.email = :email"),
    @NamedQuery(name = "Email.findByPrincipal", query = "SELECT e FROM Email e WHERE e.principal = :principal")})
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "principal")
    private Character principal;
    @JoinTable(name = "persona_email", joinColumns = {
        @JoinColumn(name = "id_email", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_persona", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Persona> personaCollection;
    @JoinTable(name = "local_email", joinColumns = {
        @JoinColumn(name = "id_email", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_local", referencedColumnName = "id"),
        @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")})
    @ManyToMany
    private Collection<Local> localCollection;
    @JoinColumn(name = "id_tipo_email", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoEmail idTipoEmail;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "email")
    private Collection<ContactoEmail> contactoEmailCollection;

    public Email() {
    }

    public Email(Integer id) {
        this.id = id;
    }

    public Email(Integer id, String email, Character principal) {
        this.id = id;
        this.email = email;
        this.principal = principal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    @XmlTransient
    public Collection<Local> getLocalCollection() {
        return localCollection;
    }

    public void setLocalCollection(Collection<Local> localCollection) {
        this.localCollection = localCollection;
    }

    public TipoEmail getIdTipoEmail() {
        return idTipoEmail;
    }

    public void setIdTipoEmail(TipoEmail idTipoEmail) {
        this.idTipoEmail = idTipoEmail;
    }

    @XmlTransient
    public Collection<ContactoEmail> getContactoEmailCollection() {
        return contactoEmailCollection;
    }

    public void setContactoEmailCollection(Collection<ContactoEmail> contactoEmailCollection) {
        this.contactoEmailCollection = contactoEmailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Email)) {
            return false;
        }
        Email other = (Email) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.Email[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.info;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "prefijo_telefono", catalog = "sepsa", schema = "info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrefijoTelefono.findAll", query = "SELECT p FROM PrefijoTelefono p"),
    @NamedQuery(name = "PrefijoTelefono.findById", query = "SELECT p FROM PrefijoTelefono p WHERE p.id = :id"),
    @NamedQuery(name = "PrefijoTelefono.findByPrefijo", query = "SELECT p FROM PrefijoTelefono p WHERE p.prefijo = :prefijo")})
public class PrefijoTelefono implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "prefijo")
    private String prefijo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPrefijo")
    private Collection<Telefono> telefonoCollection;

    public PrefijoTelefono() {
    }

    public PrefijoTelefono(Integer id) {
        this.id = id;
    }

    public PrefijoTelefono(Integer id, String prefijo) {
        this.id = id;
        this.prefijo = prefijo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    @XmlTransient
    public Collection<Telefono> getTelefonoCollection() {
        return telefonoCollection;
    }

    public void setTelefonoCollection(Collection<Telefono> telefonoCollection) {
        this.telefonoCollection = telefonoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PrefijoTelefono)) {
            return false;
        }
        PrefijoTelefono other = (PrefijoTelefono) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.info.PrefijoTelefono[ id=" + id + " ]";
    }
    
}

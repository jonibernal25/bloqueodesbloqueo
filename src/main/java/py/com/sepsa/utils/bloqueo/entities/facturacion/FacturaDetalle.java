/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "factura_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FacturaDetalle.findAll", query = "SELECT f FROM FacturaDetalle f"),
    @NamedQuery(name = "FacturaDetalle.findByIdFactura", query = "SELECT f FROM FacturaDetalle f WHERE f.facturaDetallePK.idFactura = :idFactura"),
    @NamedQuery(name = "FacturaDetalle.findByNroLinea", query = "SELECT f FROM FacturaDetalle f WHERE f.facturaDetallePK.nroLinea = :nroLinea"),
    @NamedQuery(name = "FacturaDetalle.findByDescripcion", query = "SELECT f FROM FacturaDetalle f WHERE f.descripcion = :descripcion"),
    @NamedQuery(name = "FacturaDetalle.findByPorcentajeIva", query = "SELECT f FROM FacturaDetalle f WHERE f.porcentajeIva = :porcentajeIva"),
    @NamedQuery(name = "FacturaDetalle.findByMontoIva", query = "SELECT f FROM FacturaDetalle f WHERE f.montoIva = :montoIva"),
    @NamedQuery(name = "FacturaDetalle.findByMontoImponible", query = "SELECT f FROM FacturaDetalle f WHERE f.montoImponible = :montoImponible"),
    @NamedQuery(name = "FacturaDetalle.findByMontoTotal", query = "SELECT f FROM FacturaDetalle f WHERE f.montoTotal = :montoTotal")})
public class FacturaDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FacturaDetallePK facturaDetallePK;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "porcentaje_iva")
    private BigInteger porcentajeIva;
    @Basic(optional = false)
    @Column(name = "monto_iva")
    private BigInteger montoIva;
    @Basic(optional = false)
    @Column(name = "monto_imponible")
    private BigInteger montoImponible;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private BigInteger montoTotal;
    @JoinTable(name = "factura_detalle_liquidacion_detalle", joinColumns = {
        @JoinColumn(name = "nro_linea_factura", referencedColumnName = "nro_linea"),
        @JoinColumn(name = "id_factura", referencedColumnName = "id_factura")}, inverseJoinColumns = {
        @JoinColumn(name = "id_liquidacion", referencedColumnName = "id_liquidacion"),
        @JoinColumn(name = "nro_linea_liquidacion", referencedColumnName = "nro_linea")})
    @ManyToMany
    private Collection<LiquidacionDetalle> liquidacionDetalleCollection;
    @JoinColumn(name = "id_factura", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Factura factura;

    public FacturaDetalle() {
    }

    public FacturaDetalle(FacturaDetallePK facturaDetallePK) {
        this.facturaDetallePK = facturaDetallePK;
    }

    public FacturaDetalle(FacturaDetallePK facturaDetallePK, String descripcion, BigInteger porcentajeIva, BigInteger montoIva, BigInteger montoImponible, BigInteger montoTotal) {
        this.facturaDetallePK = facturaDetallePK;
        this.descripcion = descripcion;
        this.porcentajeIva = porcentajeIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
    }

    public FacturaDetalle(int idFactura, BigInteger nroLinea) {
        this.facturaDetallePK = new FacturaDetallePK(idFactura, nroLinea);
    }

    public FacturaDetallePK getFacturaDetallePK() {
        return facturaDetallePK;
    }

    public void setFacturaDetallePK(FacturaDetallePK facturaDetallePK) {
        this.facturaDetallePK = facturaDetallePK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(BigInteger porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigInteger getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigInteger montoIva) {
        this.montoIva = montoIva;
    }

    public BigInteger getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigInteger montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigInteger getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigInteger montoTotal) {
        this.montoTotal = montoTotal;
    }

    @XmlTransient
    public Collection<LiquidacionDetalle> getLiquidacionDetalleCollection() {
        return liquidacionDetalleCollection;
    }

    public void setLiquidacionDetalleCollection(Collection<LiquidacionDetalle> liquidacionDetalleCollection) {
        this.liquidacionDetalleCollection = liquidacionDetalleCollection;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (facturaDetallePK != null ? facturaDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturaDetalle)) {
            return false;
        }
        FacturaDetalle other = (FacturaDetalle) object;
        if ((this.facturaDetallePK == null && other.facturaDetallePK != null) || (this.facturaDetallePK != null && !this.facturaDetallePK.equals(other.facturaDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.FacturaDetalle[ facturaDetallePK=" + facturaDetallePK + " ]";
    }
    
}

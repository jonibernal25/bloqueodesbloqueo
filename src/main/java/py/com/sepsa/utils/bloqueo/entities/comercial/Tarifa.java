/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tarifa", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarifa.findAll", query = "SELECT t FROM Tarifa t"),
    @NamedQuery(name = "Tarifa.findById", query = "SELECT t FROM Tarifa t WHERE t.id = :id"),
    @NamedQuery(name = "Tarifa.findByIdMoneda", query = "SELECT t FROM Tarifa t WHERE t.idMoneda = :idMoneda"),
    @NamedQuery(name = "Tarifa.findByMonto", query = "SELECT t FROM Tarifa t WHERE t.monto = :monto"),
    @NamedQuery(name = "Tarifa.findByEstado", query = "SELECT t FROM Tarifa t WHERE t.estado = :estado"),
    @NamedQuery(name = "Tarifa.findByDescripcion", query = "SELECT t FROM Tarifa t WHERE t.descripcion = :descripcion")})
public class Tarifa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_moneda")
    private int idMoneda;
    @Column(name = "monto")
    private BigInteger monto;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTarifa")
    private Collection<TarifaRango> tarifaRangoCollection;
    @JoinColumns({
        @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio"),
        @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")})
    @ManyToOne
    private ProductoServicio productoServicio;
    @JoinColumn(name = "id_tipo_tarifa", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoTarifa idTipoTarifa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarifa")
    private Collection<ContratoTarifa> contratoTarifaCollection;

    public Tarifa() {
    }

    public Tarifa(Integer id) {
        this.id = id;
    }

    public Tarifa(Integer id, int idMoneda, Character estado) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TarifaRango> getTarifaRangoCollection() {
        return tarifaRangoCollection;
    }

    public void setTarifaRangoCollection(Collection<TarifaRango> tarifaRangoCollection) {
        this.tarifaRangoCollection = tarifaRangoCollection;
    }

    public ProductoServicio getProductoServicio() {
        return productoServicio;
    }

    public void setProductoServicio(ProductoServicio productoServicio) {
        this.productoServicio = productoServicio;
    }

    public TipoTarifa getIdTipoTarifa() {
        return idTipoTarifa;
    }

    public void setIdTipoTarifa(TipoTarifa idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    @XmlTransient
    public Collection<ContratoTarifa> getContratoTarifaCollection() {
        return contratoTarifaCollection;
    }

    public void setContratoTarifaCollection(Collection<ContratoTarifa> contratoTarifaCollection) {
        this.contratoTarifaCollection = contratoTarifaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarifa)) {
            return false;
        }
        Tarifa other = (Tarifa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Tarifa[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "operacion_red_pago", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OperacionRedPago.findAll", query = "SELECT o FROM OperacionRedPago o"),
    @NamedQuery(name = "OperacionRedPago.findById", query = "SELECT o FROM OperacionRedPago o WHERE o.id = :id"),
    @NamedQuery(name = "OperacionRedPago.findByDescripcion", query = "SELECT o FROM OperacionRedPago o WHERE o.descripcion = :descripcion")})
public class OperacionRedPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOperacion")
    private Collection<TransaccionRedPago> transaccionRedPagoCollection;

    public OperacionRedPago() {
    }

    public OperacionRedPago(Integer id) {
        this.id = id;
    }

    public OperacionRedPago(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TransaccionRedPago> getTransaccionRedPagoCollection() {
        return transaccionRedPagoCollection;
    }

    public void setTransaccionRedPagoCollection(Collection<TransaccionRedPago> transaccionRedPagoCollection) {
        this.transaccionRedPagoCollection = transaccionRedPagoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OperacionRedPago)) {
            return false;
        }
        OperacionRedPago other = (OperacionRedPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.OperacionRedPago[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "configuracion_xml", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConfiguracionXml.findAll", query = "SELECT c FROM ConfiguracionXml c"),
    @NamedQuery(name = "ConfiguracionXml.findByIdConfiguracion", query = "SELECT c FROM ConfiguracionXml c WHERE c.configuracionXmlPK.idConfiguracion = :idConfiguracion"),
    @NamedQuery(name = "ConfiguracionXml.findByIdXml", query = "SELECT c FROM ConfiguracionXml c WHERE c.configuracionXmlPK.idXml = :idXml"),
    @NamedQuery(name = "ConfiguracionXml.findByValor", query = "SELECT c FROM ConfiguracionXml c WHERE c.valor = :valor")})
public class ConfiguracionXml implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConfiguracionXmlPK configuracionXmlPK;
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "id_configuracion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Configuracion configuracion;
    @JoinColumn(name = "id_xml", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Xml xml;

    public ConfiguracionXml() {
    }

    public ConfiguracionXml(ConfiguracionXmlPK configuracionXmlPK) {
        this.configuracionXmlPK = configuracionXmlPK;
    }

    public ConfiguracionXml(int idConfiguracion, int idXml) {
        this.configuracionXmlPK = new ConfiguracionXmlPK(idConfiguracion, idXml);
    }

    public ConfiguracionXmlPK getConfiguracionXmlPK() {
        return configuracionXmlPK;
    }

    public void setConfiguracionXmlPK(ConfiguracionXmlPK configuracionXmlPK) {
        this.configuracionXmlPK = configuracionXmlPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Configuracion getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(Configuracion configuracion) {
        this.configuracion = configuracion;
    }

    public Xml getXml() {
        return xml;
    }

    public void setXml(Xml xml) {
        this.xml = xml;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (configuracionXmlPK != null ? configuracionXmlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfiguracionXml)) {
            return false;
        }
        ConfiguracionXml other = (ConfiguracionXml) object;
        if ((this.configuracionXmlPK == null && other.configuracionXmlPK != null) || (this.configuracionXmlPK != null && !this.configuracionXmlPK.equals(other.configuracionXmlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.ConfiguracionXml[ configuracionXmlPK=" + configuracionXmlPK + " ]";
    }
    
}

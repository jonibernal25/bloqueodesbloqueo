package py.com.sepsa.utils.bloqueo.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
/**
 * Utilidades de un Archivo
 * @author Cristina Insfrán
 */
public class FileUtils {
    /**
     * Convierte un archivo a un array de caracteres, donde cada item de la
     * lista es una linea del archivo
     * @param file Archivo a convertir
     * @return Array de caracteres
     */
    public static String [] fileToArray(File file) {
        String [] fArray=null;
        BufferedReader fileBuff= null;
        try {
            ArrayList <String> list= new ArrayList<>();
            fileBuff = new BufferedReader(new FileReader(file));
            String linea= fileBuff.readLine();
            
            while(linea!=null){
                list.add(linea);
                linea=fileBuff.readLine();
            }
            fArray= new String [list.size()];
            list.toArray(fArray);
            fileBuff.close();
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
        return fArray;
    }
    /**
     * Obtiene los bytes de un archivo
     * @param file Archivo a convertir
     * @return Bytes de un archivo
     * @throws IOException
     */
    public static byte[] getBytesFromFile(File file) throws IOException {
     
        InputStream is= new FileInputStream(file);
      
        long length= file.length();
        if (length>Integer.MAX_VALUE) {
            throw new IOException("Archivo muy grande, no se puede leer todo "
                    + "el archivo "+file.getName());
        }
        byte[] bytes= new byte[(int)length];
        int offset= 0;
        int numRead= 0;
        while (offset<bytes.length && (numRead=is.read(bytes, offset, bytes.
                length-offset))>=0)
            offset+= numRead;
        if(is!=null)
            is.close();
        if (offset<bytes.length)
            throw new IOException("No se puede leer todo el archivo: "+file.
                    getName());
        return bytes;
    }
    /**
     * Graba un array de bytes en un archivo
     * @param data Datos a grabar
     * @param fileName Nombre del archivo a grabar
     * @throws IOException
     */
    public static void writeFile(byte[] data, String fileName) throws
            IOException{
       
        OutputStream out = new FileOutputStream(fileName);
        out.write(data);
        out.close();
    } 
    
   /**
    * Convierte una cadena de texto en un archivo y lo guarda en el disco duro
     * @param text Texto a guardar
     * @param path Ruta donde se almacena el archivo
     * @return Si se ha guardado o no el archivo
    */
    public static boolean stringToFile (String text, String path) {

        boolean saved= false;
        try (PrintWriter writer = new PrintWriter(new File(path))) {
            writer.print(text);
            saved= true;
        } catch (Exception ex) {
            LogHandler.logFatal("Se ha producido el siguiente error al tratar de convertir cadena a archivo: ", ex);
        }
        return saved;
    } 
    
    /**
     * Realiza el copiado de un archivo
     * @param file Archivo
     * @param dest Destino
     */
    public static void copy(File file, String dest) throws Exception {
        try (FileInputStream is = new FileInputStream(file)) {
            Path targetFilePath = Paths.get(dest);
            Files.copy(is, targetFilePath, StandardCopyOption.REPLACE_EXISTING);
        }
    }
}

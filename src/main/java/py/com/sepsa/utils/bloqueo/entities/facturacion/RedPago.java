/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "red_pago", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RedPago.findAll", query = "SELECT r FROM RedPago r"),
    @NamedQuery(name = "RedPago.findById", query = "SELECT r FROM RedPago r WHERE r.id = :id"),
    @NamedQuery(name = "RedPago.findByNombre", query = "SELECT r FROM RedPago r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "RedPago.findByUsuario", query = "SELECT r FROM RedPago r WHERE r.usuario = :usuario"),
    @NamedQuery(name = "RedPago.findByContrasena", query = "SELECT r FROM RedPago r WHERE r.contrasena = :contrasena")})
public class RedPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "contrasena")
    private String contrasena;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRed")
    private Collection<TransaccionRedPago> transaccionRedPagoCollection;

    public RedPago() {
    }

    public RedPago(Integer id) {
        this.id = id;
    }

    public RedPago(Integer id, String nombre, String usuario, String contrasena) {
        this.id = id;
        this.nombre = nombre;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @XmlTransient
    public Collection<TransaccionRedPago> getTransaccionRedPagoCollection() {
        return transaccionRedPagoCollection;
    }

    public void setTransaccionRedPagoCollection(Collection<TransaccionRedPago> transaccionRedPagoCollection) {
        this.transaccionRedPagoCollection = transaccionRedPagoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RedPago)) {
            return false;
        }
        RedPago other = (RedPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.RedPago[ id=" + id + " ]";
    }
    
}

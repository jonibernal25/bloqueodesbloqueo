/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class LocalTipoNegocioPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_local")
    private int idLocal;
    @Basic(optional = false)
    @Column(name = "id_persona")
    private int idPersona;
    @Basic(optional = false)
    @Column(name = "id_tipo_negocio")
    private int idTipoNegocio;

    public LocalTipoNegocioPK() {
    }

    public LocalTipoNegocioPK(int idLocal, int idPersona, int idTipoNegocio) {
        this.idLocal = idLocal;
        this.idPersona = idPersona;
        this.idTipoNegocio = idTipoNegocio;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public int getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public void setIdTipoNegocio(int idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idLocal;
        hash += (int) idPersona;
        hash += (int) idTipoNegocio;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalTipoNegocioPK)) {
            return false;
        }
        LocalTipoNegocioPK other = (LocalTipoNegocioPK) object;
        if (this.idLocal != other.idLocal) {
            return false;
        }
        if (this.idPersona != other.idPersona) {
            return false;
        }
        if (this.idTipoNegocio != other.idTipoNegocio) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.LocalTipoNegocioPK[ idLocal=" + idLocal + ", idPersona=" + idPersona + ", idTipoNegocio=" + idTipoNegocio + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.main;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.sepsa.utils.bloqueo.entities.comercial.Cliente;
import py.com.sepsa.utils.bloqueo.entities.comercial.Producto;
import py.com.sepsa.utils.bloqueo.entities.facturacion.Factura;
import py.com.sepsa.utils.bloqueo.entities.info.Persona;
import py.com.sepsa.utils.bloqueo.entities.liquidacion.LiquidacionProducto;
import py.com.sepsa.utils.bloqueo.entities.trans.Usuario;
import py.com.sepsa.utils.bloqueo.utils.BdUtils;
import py.com.sepsa.utils.bloqueo.utils.MailUtils;
import py.com.sepsa.utils.bloqueo.utils.Report;

/**
 * Clase para la generación de reporte
 * @author Jonathan D. Bernal Fernández
 */
public class ReportGenerator {
    
    
    public static void generarReporteDesbloqueo(BdUtils bdUtils,
            List<Usuario> usuarios, String mails)
            throws IOException, MessagingException {
        
        if(usuarios == null || usuarios.isEmpty()) {
            return;
        }
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Resultado");


        XSSFFont headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 10);

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        CellStyle numberCellStyle = workbook.createCellStyle();
        numberCellStyle.setFont(headerFont);
        numberCellStyle.setDataFormat((short) 3);

        XSSFFont resaltFont = workbook.createFont();
        resaltFont.setBold(true);
        resaltFont.setFontHeightInPoints((short) 10);

        CellStyle resaltCellStyle = workbook.createCellStyle();
        resaltCellStyle.setFont(resaltFont);

        CellStyle resaltCenterCellStyle = workbook.createCellStyle();
        resaltCenterCellStyle.setAlignment(HorizontalAlignment.CENTER);
        resaltCenterCellStyle.setFont(resaltFont);

        XSSFRow dateRow = sheet.createRow(1);

        XSSFCell cell1 = dateRow.createCell(1);
        cell1.setCellValue("Fecha Proceso:");
        cell1.setCellStyle(resaltCellStyle);

        cell1 = dateRow.createCell(2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cell1.setCellValue(sdf.format(Calendar.getInstance().getTime()));
        cell1.setCellStyle(resaltCenterCellStyle);
        
        XSSFRow headerRow = sheet.createRow(3);

        XSSFCell cell = headerRow.createCell(1);
        cell.setCellValue("Id Cliente");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(2);
        cell.setCellValue("Ruc");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(3);
        cell.setCellValue("Cliente");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(4);
        cell.setCellValue("Id Usuario");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(5);
        cell.setCellValue("Usuario");
        cell.setCellStyle(resaltCenterCellStyle);

        int rowCount = 4;
        
        for (int i = 0; i < usuarios.size(); i++) {
            
            Persona persona = bdUtils.findPersona(usuarios
                    .get(i).getIdPersona());
            
            String ruc = String.format("%s-%s",
                    String.valueOf(persona.getRuc()),
                    String.valueOf(persona.getDvRuc()));
            
            String razon = persona.getPersonaJuridica() != null
                    ? persona.getPersonaJuridica().getRazonSocial()
                    : String.format("%s %s",
                            persona.getPersonaFisica().getNombre(),
                            persona.getPersonaFisica().getApellido());
            
            XSSFRow row = sheet.createRow(rowCount + i);

            cell = row.createCell(1);
            cell.setCellValue(usuarios.get(i).getIdPersona());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(2);
            cell.setCellValue(ruc);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(3);
            cell.setCellValue(razon);
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(4);
            cell.setCellValue(usuarios.get(i).getId());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(5);
            cell.setCellValue(usuarios.get(i).getUsuario());
            cell.setCellStyle(headerCellStyle);
        }
        
        ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
        workbook.write(fileOut);

        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(mails, ";");
        while (st.hasMoreElements()) {
            String nextElement = (String)st.nextElement();
            list.add(nextElement);
        }
        
        sendMaild("Reporte de Desbloqueo de Usuarios",
                fileOut.toByteArray(), list);
    }
    
    public static void generarReporteBloqueo(BdUtils bdUtils, Producto producto,
            List<Factura> facturas, String mails)
            throws IOException, MessagingException {
        
        if(facturas == null || facturas.isEmpty()) {
            return;
        }
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Resultado");


        XSSFFont headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 10);

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        CellStyle numberCellStyle = workbook.createCellStyle();
        numberCellStyle.setFont(headerFont);
        numberCellStyle.setDataFormat((short) 3);

        XSSFFont resaltFont = workbook.createFont();
        resaltFont.setBold(true);
        resaltFont.setFontHeightInPoints((short) 10);

        CellStyle resaltCellStyle = workbook.createCellStyle();
        resaltCellStyle.setFont(resaltFont);

        CellStyle resaltCenterCellStyle = workbook.createCellStyle();
        resaltCenterCellStyle.setAlignment(HorizontalAlignment.CENTER);
        resaltCenterCellStyle.setFont(resaltFont);

        XSSFRow dateRow = sheet.createRow(1);

        XSSFCell cell1 = dateRow.createCell(1);
        cell1.setCellValue("Fecha Proceso:");
        cell1.setCellStyle(resaltCellStyle);

        cell1 = dateRow.createCell(2);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cell1.setCellValue(sdf.format(Calendar.getInstance().getTime()));
        cell1.setCellStyle(resaltCenterCellStyle);
        
        XSSFRow headerRow = sheet.createRow(3);

        XSSFCell cell = headerRow.createCell(1);
        cell.setCellValue("Id Cliente");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(2);
        cell.setCellValue("Ruc");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(3);
        cell.setCellValue("Cliente");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(4);
        cell.setCellValue("Comercial");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(5);
        cell.setCellValue("Nro. Factura");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(6);
        cell.setCellValue("Fecha Factura");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(7);
        cell.setCellValue("Plazo");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(8);
        cell.setCellValue("Extensión");
        cell.setCellStyle(resaltCenterCellStyle);

        cell = headerRow.createCell(9);
        cell.setCellValue("Producto");
        cell.setCellStyle(resaltCenterCellStyle);

        int rowCount = 4;
        
        for (int i = 0; i < facturas.size(); i++) {
            
            LiquidacionProducto liquidacionProducto =
                    getLiquidacionProducto(bdUtils,
                            facturas.get(i).getIdCliente(),
                            producto.getId());
            
            XSSFRow row = sheet.createRow(rowCount + i);

            cell = row.createCell(1);
            cell.setCellValue(facturas.get(i).getIdCliente());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(2);
            cell.setCellValue(facturas.get(i).getRuc());
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(3);
            cell.setCellValue(facturas.get(i).getRazonSocial());
            cell.setCellStyle(headerCellStyle);

            Cliente cliente = bdUtils.findCliente(facturas
                    .get(i).getIdCliente());
            
            Persona persona = cliente.getIdComercial() == null
                    ? null
                    : bdUtils.findPersona(cliente.getIdComercial());
            
            String comercial = persona == null
                    ? "N/A"
                    : persona.getPersonaJuridica() != null
                    ? persona.getPersonaJuridica().getRazonSocial()
                    : String.format("%s %s",
                            persona.getPersonaFisica().getNombre(),
                            persona.getPersonaFisica().getApellido());
            
            cell = row.createCell(4);
            cell.setCellValue(comercial);
            cell.setCellStyle(headerCellStyle);

            SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
            
            cell = row.createCell(5);
            cell.setCellValue(sdf2.format(facturas.get(i).getNroFactura()));
            cell.setCellStyle(headerCellStyle);
            
            cell = row.createCell(6);
            cell.setCellValue(sdf2.format(facturas.get(i).getFecha()));
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(7);
            cell.setCellValue(String.format("%d dias", liquidacionProducto.getPlazo()));
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(8);
            cell.setCellValue(String.format("%d dias", liquidacionProducto.getExtension()));
            cell.setCellStyle(headerCellStyle);

            cell = row.createCell(9);
            cell.setCellValue(producto.getDescripcion());
            cell.setCellStyle(headerCellStyle);
        }
        
        ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
        workbook.write(fileOut);

        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(mails, ";");
        while (st.hasMoreElements()) {
            String nextElement = st.nextToken();
            list.add(nextElement);
        }
        
        sendMaild("Reporte de Bloqueo de Usuarios",
                fileOut.toByteArray(), list);
    }
    
    /**
     * Obtiene una liquidación de producto
     * @param idCliente Identificador de cliente
     * @param idProducto Identificador de producto
     * @return Liquidación de producto
     */
    private static LiquidacionProducto getLiquidacionProducto(BdUtils bdUtils,
            Integer idCliente, Integer idProducto) {
        
        if(idProducto == null) {
            return null;
        }
        
        LiquidacionProducto result = bdUtils.findLiquidacionProducto(idProducto,
                idCliente);
        
        if(result != null) {
            return result;
        }
        
        result = bdUtils.findLiquidacionProducto(idProducto, null);
        
        return result;
    }
    
    public static void sendMaild(String issue, byte[] bytes, List<String> mails) throws MessagingException {
        MimeMultipart mp = new MimeMultipart("related");
            
        MimeBodyPart att = new MimeBodyPart(); 
        ByteArrayDataSource bds = new ByteArrayDataSource(bytes, MailUtils.MimeType.XLS.getValue()); 
        att.setDataHandler(new DataHandler(bds)); 
        att.setFileName(issue); 
        mp.addBodyPart(att);

        Report t = new Report();
        t.setName(issue);
        t.setMailToList(mails);
        List<Report> tos = new ArrayList();
        tos.add(t);
        MailUtils.send(tos, mp, issue);
    }
}

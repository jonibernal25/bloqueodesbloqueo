/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "retencion", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Retencion.findAll", query = "SELECT r FROM Retencion r"),
    @NamedQuery(name = "Retencion.findById", query = "SELECT r FROM Retencion r WHERE r.id = :id"),
    @NamedQuery(name = "Retencion.findByIdCliente", query = "SELECT r FROM Retencion r WHERE r.idCliente = :idCliente"),
    @NamedQuery(name = "Retencion.findByNroRetencion", query = "SELECT r FROM Retencion r WHERE r.nroRetencion = :nroRetencion"),
    @NamedQuery(name = "Retencion.findByFecha", query = "SELECT r FROM Retencion r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "Retencion.findByMontoImponibleTotal", query = "SELECT r FROM Retencion r WHERE r.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "Retencion.findByMontoIvaTotal", query = "SELECT r FROM Retencion r WHERE r.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "Retencion.findByMontoTotal", query = "SELECT r FROM Retencion r WHERE r.montoTotal = :montoTotal"),
    @NamedQuery(name = "Retencion.findByPorcentajeRetencion", query = "SELECT r FROM Retencion r WHERE r.porcentajeRetencion = :porcentajeRetencion"),
    @NamedQuery(name = "Retencion.findByMontoRetenidoTotal", query = "SELECT r FROM Retencion r WHERE r.montoRetenidoTotal = :montoRetenidoTotal")})
public class Retencion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "nro_retencion")
    private String nroRetencion;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "monto_imponible_total")
    private BigInteger montoImponibleTotal;
    @Basic(optional = false)
    @Column(name = "monto_iva_total")
    private BigInteger montoIvaTotal;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private BigInteger montoTotal;
    @Basic(optional = false)
    @Column(name = "porcentaje_retencion")
    private int porcentajeRetencion;
    @Basic(optional = false)
    @Column(name = "monto_retenido_total")
    private BigInteger montoRetenidoTotal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "retencion")
    private Collection<RetencionDetalle> retencionDetalleCollection;

    public Retencion() {
    }

    public Retencion(Integer id) {
        this.id = id;
    }

    public Retencion(Integer id, String nroRetencion, Date fecha, BigInteger montoImponibleTotal, BigInteger montoIvaTotal, BigInteger montoTotal, int porcentajeRetencion, BigInteger montoRetenidoTotal) {
        this.id = id;
        this.nroRetencion = nroRetencion;
        this.fecha = fecha;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoIvaTotal = montoIvaTotal;
        this.montoTotal = montoTotal;
        this.porcentajeRetencion = porcentajeRetencion;
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigInteger montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigInteger getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigInteger montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigInteger getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigInteger montoTotal) {
        this.montoTotal = montoTotal;
    }

    public int getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(int porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigInteger getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigInteger montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    @XmlTransient
    public Collection<RetencionDetalle> getRetencionDetalleCollection() {
        return retencionDetalleCollection;
    }

    public void setRetencionDetalleCollection(Collection<RetencionDetalle> retencionDetalleCollection) {
        this.retencionDetalleCollection = retencionDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Retencion)) {
            return false;
        }
        Retencion other = (Retencion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Retencion[ id=" + id + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class ProveedorSociedadPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_sociedad")
    private int idSociedad;
    @Basic(optional = false)
    @Column(name = "id_proveedor")
    private int idProveedor;

    public ProveedorSociedadPK() {
    }

    public ProveedorSociedadPK(int idSociedad, int idProveedor) {
        this.idSociedad = idSociedad;
        this.idProveedor = idProveedor;
    }

    public int getIdSociedad() {
        return idSociedad;
    }

    public void setIdSociedad(int idSociedad) {
        this.idSociedad = idSociedad;
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSociedad;
        hash += (int) idProveedor;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProveedorSociedadPK)) {
            return false;
        }
        ProveedorSociedadPK other = (ProveedorSociedadPK) object;
        if (this.idSociedad != other.idSociedad) {
            return false;
        }
        if (this.idProveedor != other.idProveedor) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.ProveedorSociedadPK[ idSociedad=" + idSociedad + ", idProveedor=" + idProveedor + " ]";
    }
    
}

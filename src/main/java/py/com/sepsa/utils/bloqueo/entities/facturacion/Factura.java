/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "factura", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Factura.findAll", query = "SELECT f FROM Factura f"),
    @NamedQuery(name = "Factura.findById", query = "SELECT f FROM Factura f WHERE f.id = :id"),
    @NamedQuery(name = "Factura.findByIdCliente", query = "SELECT f FROM Factura f WHERE f.idCliente = :idCliente"),
    @NamedQuery(name = "Factura.findByIdMoneda", query = "SELECT f FROM Factura f WHERE f.idMoneda = :idMoneda"),
    @NamedQuery(name = "Factura.findByNroFactura", query = "SELECT f FROM Factura f WHERE f.nroFactura = :nroFactura"),
    @NamedQuery(name = "Factura.findByFecha", query = "SELECT f FROM Factura f WHERE f.fecha = :fecha"),
    @NamedQuery(name = "Factura.findByRazonSocial", query = "SELECT f FROM Factura f WHERE f.razonSocial = :razonSocial"),
    @NamedQuery(name = "Factura.findByDireccion", query = "SELECT f FROM Factura f WHERE f.direccion = :direccion"),
    @NamedQuery(name = "Factura.findByRuc", query = "SELECT f FROM Factura f WHERE f.ruc = :ruc"),
    @NamedQuery(name = "Factura.findByTelefono", query = "SELECT f FROM Factura f WHERE f.telefono = :telefono"),
    @NamedQuery(name = "Factura.findByFechaDesde", query = "SELECT f FROM Factura f WHERE f.fechaDesde = :fechaDesde"),
    @NamedQuery(name = "Factura.findByFechaHasta", query = "SELECT f FROM Factura f WHERE f.fechaHasta = :fechaHasta"),
    @NamedQuery(name = "Factura.findByAnulado", query = "SELECT f FROM Factura f WHERE f.anulado = :anulado"),
    @NamedQuery(name = "Factura.findByCobrado", query = "SELECT f FROM Factura f WHERE f.cobrado = :cobrado"),
    @NamedQuery(name = "Factura.findByImpreso", query = "SELECT f FROM Factura f WHERE f.impreso = :impreso"),
    @NamedQuery(name = "Factura.findByEntregado", query = "SELECT f FROM Factura f WHERE f.entregado = :entregado"),
    @NamedQuery(name = "Factura.findByFechaEntrega", query = "SELECT f FROM Factura f WHERE f.fechaEntrega = :fechaEntrega"),
    @NamedQuery(name = "Factura.findByDiasCredito", query = "SELECT f FROM Factura f WHERE f.diasCredito = :diasCredito"),
    @NamedQuery(name = "Factura.findByObservacion", query = "SELECT f FROM Factura f WHERE f.observacion = :observacion"),
    @NamedQuery(name = "Factura.findByMontoIva5", query = "SELECT f FROM Factura f WHERE f.montoIva5 = :montoIva5"),
    @NamedQuery(name = "Factura.findByMontoImponible5", query = "SELECT f FROM Factura f WHERE f.montoImponible5 = :montoImponible5"),
    @NamedQuery(name = "Factura.findByMontoTotal5", query = "SELECT f FROM Factura f WHERE f.montoTotal5 = :montoTotal5"),
    @NamedQuery(name = "Factura.findByMontoIva10", query = "SELECT f FROM Factura f WHERE f.montoIva10 = :montoIva10"),
    @NamedQuery(name = "Factura.findByMontoImponible10", query = "SELECT f FROM Factura f WHERE f.montoImponible10 = :montoImponible10"),
    @NamedQuery(name = "Factura.findByMontoTotal10", query = "SELECT f FROM Factura f WHERE f.montoTotal10 = :montoTotal10"),
    @NamedQuery(name = "Factura.findByMontoTotalExento", query = "SELECT f FROM Factura f WHERE f.montoTotalExento = :montoTotalExento"),
    @NamedQuery(name = "Factura.findByMontoIvaTotal", query = "SELECT f FROM Factura f WHERE f.montoIvaTotal = :montoIvaTotal"),
    @NamedQuery(name = "Factura.findByMontoImponibleTotal", query = "SELECT f FROM Factura f WHERE f.montoImponibleTotal = :montoImponibleTotal"),
    @NamedQuery(name = "Factura.findByMontoTotalFactura", query = "SELECT f FROM Factura f WHERE f.montoTotalFactura = :montoTotalFactura")})
public class Factura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "id_cliente")
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "id_moneda")
    private int idMoneda;
    @Basic(optional = false)
    @Column(name = "nro_factura")
    private String nroFactura;
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "ruc")
    private String ruc;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "fecha_desde")
    @Temporal(TemporalType.DATE)
    private Date fechaDesde;
    @Column(name = "fecha_hasta")
    @Temporal(TemporalType.DATE)
    private Date fechaHasta;
    @Basic(optional = false)
    @Column(name = "anulado")
    private Character anulado;
    @Basic(optional = false)
    @Column(name = "cobrado")
    private Character cobrado;
    @Basic(optional = false)
    @Column(name = "impreso")
    private Character impreso;
    @Basic(optional = false)
    @Column(name = "entregado")
    private Character entregado;
    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrega;
    @Basic(optional = false)
    @Column(name = "dias_credito")
    private int diasCredito;
    @Column(name = "observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "monto_iva_5")
    private BigInteger montoIva5;
    @Basic(optional = false)
    @Column(name = "monto_imponible_5")
    private BigInteger montoImponible5;
    @Basic(optional = false)
    @Column(name = "monto_total_5")
    private BigInteger montoTotal5;
    @Basic(optional = false)
    @Column(name = "monto_iva_10")
    private BigInteger montoIva10;
    @Basic(optional = false)
    @Column(name = "monto_imponible_10")
    private BigInteger montoImponible10;
    @Basic(optional = false)
    @Column(name = "monto_total_10")
    private BigInteger montoTotal10;
    @Basic(optional = false)
    @Column(name = "monto_total_exento")
    private BigInteger montoTotalExento;
    @Basic(optional = false)
    @Column(name = "monto_iva_total")
    private BigInteger montoIvaTotal;
    @Basic(optional = false)
    @Column(name = "monto_imponible_total")
    private BigInteger montoImponibleTotal;
    @Basic(optional = false)
    @Column(name = "monto_total_factura")
    private BigInteger montoTotalFactura;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "factura")
    private Collection<FacturaDetalle> facturaDetalleCollection;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id")
    @ManyToOne
    private Liquidacion idLiquidacion;
    @JoinColumn(name = "id_talonario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Talonario idTalonario;
    @JoinColumn(name = "id_tipo_factura", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoFactura idTipoFactura;

    public Factura() {
    }

    public Factura(Integer id) {
        this.id = id;
    }

    public Factura(Integer id, int idMoneda, String nroFactura, Date fecha, String razonSocial, Character anulado, Character cobrado, Character impreso, Character entregado, int diasCredito, BigInteger montoIva5, BigInteger montoImponible5, BigInteger montoTotal5, BigInteger montoIva10, BigInteger montoImponible10, BigInteger montoTotal10, BigInteger montoTotalExento, BigInteger montoIvaTotal, BigInteger montoImponibleTotal, BigInteger montoTotalFactura) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.nroFactura = nroFactura;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.anulado = anulado;
        this.cobrado = cobrado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.diasCredito = diasCredito;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalFactura = montoTotalFactura;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getCobrado() {
        return cobrado;
    }

    public void setCobrado(Character cobrado) {
        this.cobrado = cobrado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public int getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(int diasCredito) {
        this.diasCredito = diasCredito;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigInteger getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigInteger montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigInteger getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigInteger montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigInteger getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigInteger montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigInteger getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigInteger montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigInteger getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigInteger montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigInteger getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigInteger montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigInteger getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigInteger montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigInteger getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigInteger montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigInteger getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigInteger montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigInteger getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigInteger montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    @XmlTransient
    public Collection<FacturaDetalle> getFacturaDetalleCollection() {
        return facturaDetalleCollection;
    }

    public void setFacturaDetalleCollection(Collection<FacturaDetalle> facturaDetalleCollection) {
        this.facturaDetalleCollection = facturaDetalleCollection;
    }

    public Liquidacion getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Liquidacion idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Talonario getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Talonario idTalonario) {
        this.idTalonario = idTalonario;
    }

    public TipoFactura getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(TipoFactura idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Factura[ id=" + id + " ]";
    }
    
}

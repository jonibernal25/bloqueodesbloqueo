/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "deposito", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Deposito.findAll", query = "SELECT d FROM Deposito d"),
    @NamedQuery(name = "Deposito.findById", query = "SELECT d FROM Deposito d WHERE d.id = :id"),
    @NamedQuery(name = "Deposito.findByIdCuenta", query = "SELECT d FROM Deposito d WHERE d.idCuenta = :idCuenta"),
    @NamedQuery(name = "Deposito.findByFechaDeposito", query = "SELECT d FROM Deposito d WHERE d.fechaDeposito = :fechaDeposito"),
    @NamedQuery(name = "Deposito.findByMontoDeposito", query = "SELECT d FROM Deposito d WHERE d.montoDeposito = :montoDeposito"),
    @NamedQuery(name = "Deposito.findByEstado", query = "SELECT d FROM Deposito d WHERE d.estado = :estado")})
public class Deposito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_cuenta")
    private int idCuenta;
    @Basic(optional = false)
    @Column(name = "fecha_deposito")
    @Temporal(TemporalType.DATE)
    private Date fechaDeposito;
    @Basic(optional = false)
    @Column(name = "monto_deposito")
    private BigInteger montoDeposito;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @OneToMany(mappedBy = "idDeposito")
    private Collection<Cheque> chequeCollection;

    public Deposito() {
    }

    public Deposito(Integer id) {
        this.id = id;
    }

    public Deposito(Integer id, int idCuenta, Date fechaDeposito, BigInteger montoDeposito, Character estado) {
        this.id = id;
        this.idCuenta = idCuenta;
        this.fechaDeposito = fechaDeposito;
        this.montoDeposito = montoDeposito;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public Date getFechaDeposito() {
        return fechaDeposito;
    }

    public void setFechaDeposito(Date fechaDeposito) {
        this.fechaDeposito = fechaDeposito;
    }

    public BigInteger getMontoDeposito() {
        return montoDeposito;
    }

    public void setMontoDeposito(BigInteger montoDeposito) {
        this.montoDeposito = montoDeposito;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Cheque> getChequeCollection() {
        return chequeCollection;
    }

    public void setChequeCollection(Collection<Cheque> chequeCollection) {
        this.chequeCollection = chequeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deposito)) {
            return false;
        }
        Deposito other = (Deposito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.Deposito[ id=" + id + " ]";
    }
    
}

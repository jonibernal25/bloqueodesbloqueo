/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.pojos;

/**
 * POJO para el manejo del resultado de procesamiento
 * @author Jonathan D. Bernal Fernández
 */
public class Resultado {
    /**
     * Ruc del proveedor del producto
     */
    private String ruc;
    
    /**
     * Código gtin del producto
     */
    private String codigoGtin;
    
    /**
     * Nombre del archivo
     */
    private String nombreArchivo;
    
    /**
     * Nombre del archivo duplicado
     */
    private String archivoDuplicado;
    
    /**
     * Hash del archivo duplicado
     */
    private String hash;
    
    /**
     * Tipo
     */
    private Type tipo;

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getArchivoDuplicado() {
        return archivoDuplicado;
    }

    public void setArchivoDuplicado(String archivoDuplicado) {
        this.archivoDuplicado = archivoDuplicado;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setTipo(Type tipo) {
        this.tipo = tipo;
    }

    public Type getTipo() {
        return tipo;
    }

    public Resultado(String ruc, String codigoGtin, String nombreArchivo, String archivoDuplicado, String hash, Type tipo) {
        this.ruc = ruc;
        this.codigoGtin = codigoGtin;
        this.nombreArchivo = nombreArchivo;
        this.archivoDuplicado = archivoDuplicado;
        this.hash = hash;
        this.tipo = tipo;
    }
    
    public enum Type {
        REPETIDO, SIN_PRODUCTO, CARGADO
    }
}

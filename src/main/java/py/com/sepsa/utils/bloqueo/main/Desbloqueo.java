/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.mail.MessagingException;
import py.com.sepsa.utils.bloqueo.entities.comercial.Producto;
import py.com.sepsa.utils.bloqueo.entities.facturacion.Factura;
import py.com.sepsa.utils.bloqueo.entities.liquidacion.LiquidacionProducto;
import py.com.sepsa.utils.bloqueo.entities.trans.Usuario;
import py.com.sepsa.utils.bloqueo.utils.BdUtils;

/**
 * Clase para el manejo de desbloqueo de usuarios
 * @author Jonathan D. Bernal Fernández
 */
public class Desbloqueo {
    /**
     * Manejador de base de datos
     */
    private final BdUtils bdUtils;
    
    /**
     * Lista de productos
     */
    private final List<String> productos;
    
    /**
     * Lista de mails
     */
    private final String mails;

    public void procesarDesbloqueo() throws IOException, MessagingException {
            
        for (String producto : productos) {

            Producto prod = bdUtils.findProducto(producto);

            if(prod == null) {
                continue;
            }
            
            List<Usuario> usuarios =  bdUtils.findUsuario(null, 'B');
        
            List<Usuario> desbloqueados = new ArrayList<>();
            
            for (Usuario usuario : usuarios) {
                
                List<Factura> facturas = bdUtils
                    .getFacturasPendientes(usuario.getIdPersona());

                Factura factura = facturas == null || facturas.isEmpty()
                        ? null
                        : facturas.get(0);

                if(factura == null) {
                    if(realizarDesbloqueo(usuario)) {
                        desbloqueados.add(usuario);
                    }
                    
                    continue;
                }

                LiquidacionProducto lp = getLiquidacionProducto(
                        factura.getIdCliente(), prod.getId());

                if(lp == null) {
                    continue;
                }

                Calendar now = Calendar.getInstance();
                Calendar fact = Calendar.getInstance();
                fact.setTime(factura.getFecha());
                fact.add(Calendar.DAY_OF_MONTH, lp.getPlazo());
                fact.add(Calendar.DAY_OF_MONTH, lp.getExtension());

                if(now.compareTo(fact) <= 0) {
                    if(realizarDesbloqueo(usuario)) {
                        desbloqueados.add(usuario);
                    }
                }
            }
            
            ReportGenerator.generarReporteDesbloqueo(bdUtils,
                    desbloqueados, mails);
        }
    }
    
    /**
     * Obtiene una liquidación de producto
     * @param idCliente Identificador de cliente
     * @param idProducto Identificador de producto
     * @return Liquidación de producto
     */
    private LiquidacionProducto getLiquidacionProducto(Integer idCliente,
            Integer idProducto) {
        
        if(idProducto == null) {
            return null;
        }
        
        LiquidacionProducto result = bdUtils.findLiquidacionProducto(idProducto,
                idCliente);
        
        if(result != null) {
            return result;
        }
        
        result = bdUtils.findLiquidacionProducto(idProducto, null);
        
        return result;
    }
    
    /**
     * Realiza el desbloqueo del usuario
     * @param usuario Usuario
     */
    private boolean realizarDesbloqueo(Usuario usuario) {
        
        if(usuario == null) {
            return false;
        }
        
        usuario.setEstado('A');
        bdUtils.edit(usuario);
        
        return true;
    }
    
    /**
     * Constructor
     * @param bdUtils Manejador de base de datos
     * @param productos Lista de productos
     * @param mails Lista de mails
     */
    public Desbloqueo(BdUtils bdUtils, List<String> productos, String mails) {
        this.bdUtils = bdUtils;
        this.productos = productos;
        this.mails = mails;
    }
}

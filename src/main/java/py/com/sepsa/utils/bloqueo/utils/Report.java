/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rubén Ortiz
 */
public class Report {
    /*
     * 
     */
    private String name = "";
    /*
     * 
     */
    private List<String> mailToList = new ArrayList();

    /*
     * 
     */
    public List<String> getMailToList() {
        return mailToList;
    }

    public void setMailToList(List<String> mailToList) {
        this.mailToList = mailToList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Report() {
    }
}

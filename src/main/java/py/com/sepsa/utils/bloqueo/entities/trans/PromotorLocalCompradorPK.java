/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Embeddable
public class PromotorLocalCompradorPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_promotor")
    private int idPromotor;
    @Basic(optional = false)
    @Column(name = "id_comprador")
    private int idComprador;
    @Basic(optional = false)
    @Column(name = "id_local_comprador")
    private int idLocalComprador;

    public PromotorLocalCompradorPK() {
    }

    public PromotorLocalCompradorPK(int idPromotor, int idComprador, int idLocalComprador) {
        this.idPromotor = idPromotor;
        this.idComprador = idComprador;
        this.idLocalComprador = idLocalComprador;
    }

    public int getIdPromotor() {
        return idPromotor;
    }

    public void setIdPromotor(int idPromotor) {
        this.idPromotor = idPromotor;
    }

    public int getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(int idComprador) {
        this.idComprador = idComprador;
    }

    public int getIdLocalComprador() {
        return idLocalComprador;
    }

    public void setIdLocalComprador(int idLocalComprador) {
        this.idLocalComprador = idLocalComprador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idPromotor;
        hash += (int) idComprador;
        hash += (int) idLocalComprador;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromotorLocalCompradorPK)) {
            return false;
        }
        PromotorLocalCompradorPK other = (PromotorLocalCompradorPK) object;
        if (this.idPromotor != other.idPromotor) {
            return false;
        }
        if (this.idComprador != other.idComprador) {
            return false;
        }
        if (this.idLocalComprador != other.idLocalComprador) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.PromotorLocalCompradorPK[ idPromotor=" + idPromotor + ", idComprador=" + idComprador + ", idLocalComprador=" + idLocalComprador + " ]";
    }
    
}

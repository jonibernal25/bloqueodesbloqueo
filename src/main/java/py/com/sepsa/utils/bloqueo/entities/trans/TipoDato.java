/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.trans;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "tipo_dato", catalog = "sepsa", schema = "trans")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoDato.findAll", query = "SELECT t FROM TipoDato t"),
    @NamedQuery(name = "TipoDato.findById", query = "SELECT t FROM TipoDato t WHERE t.id = :id"),
    @NamedQuery(name = "TipoDato.findByDescripcion", query = "SELECT t FROM TipoDato t WHERE t.descripcion = :descripcion")})
public class TipoDato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoDato")
    private Collection<TipoDatoAdicional> tipoDatoAdicionalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoDato")
    private Collection<Xml> xmlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoDato")
    private Collection<ConfigTipoDoc> configTipoDocCollection;

    public TipoDato() {
    }

    public TipoDato(Integer id) {
        this.id = id;
    }

    public TipoDato(Integer id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TipoDatoAdicional> getTipoDatoAdicionalCollection() {
        return tipoDatoAdicionalCollection;
    }

    public void setTipoDatoAdicionalCollection(Collection<TipoDatoAdicional> tipoDatoAdicionalCollection) {
        this.tipoDatoAdicionalCollection = tipoDatoAdicionalCollection;
    }

    @XmlTransient
    public Collection<Xml> getXmlCollection() {
        return xmlCollection;
    }

    public void setXmlCollection(Collection<Xml> xmlCollection) {
        this.xmlCollection = xmlCollection;
    }

    @XmlTransient
    public Collection<ConfigTipoDoc> getConfigTipoDocCollection() {
        return configTipoDocCollection;
    }

    public void setConfigTipoDocCollection(Collection<ConfigTipoDoc> configTipoDocCollection) {
        this.configTipoDocCollection = configTipoDocCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoDato)) {
            return false;
        }
        TipoDato other = (TipoDato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.trans.TipoDato[ id=" + id + " ]";
    }
    
}

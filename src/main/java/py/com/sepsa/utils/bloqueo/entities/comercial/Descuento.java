/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.comercial;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "descuento", catalog = "sepsa", schema = "comercial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Descuento.findAll", query = "SELECT d FROM Descuento d"),
    @NamedQuery(name = "Descuento.findById", query = "SELECT d FROM Descuento d WHERE d.id = :id"),
    @NamedQuery(name = "Descuento.findByFechaInicial", query = "SELECT d FROM Descuento d WHERE d.fechaInicial = :fechaInicial"),
    @NamedQuery(name = "Descuento.findByFechaFinal", query = "SELECT d FROM Descuento d WHERE d.fechaFinal = :fechaFinal"),
    @NamedQuery(name = "Descuento.findByValorDescuento", query = "SELECT d FROM Descuento d WHERE d.valorDescuento = :valorDescuento"),
    @NamedQuery(name = "Descuento.findByEstado", query = "SELECT d FROM Descuento d WHERE d.estado = :estado"),
    @NamedQuery(name = "Descuento.findByExcedente", query = "SELECT d FROM Descuento d WHERE d.excedente = :excedente"),
    @NamedQuery(name = "Descuento.findByPorcentual", query = "SELECT d FROM Descuento d WHERE d.porcentual = :porcentual"),
    @NamedQuery(name = "Descuento.findByDescripcion", query = "SELECT d FROM Descuento d WHERE d.descripcion = :descripcion")})
public class Descuento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha_inicial")
    @Temporal(TemporalType.DATE)
    private Date fechaInicial;
    @Column(name = "fecha_final")
    @Temporal(TemporalType.DATE)
    private Date fechaFinal;
    @Basic(optional = false)
    @Column(name = "valor_descuento")
    private BigInteger valorDescuento;
    @Basic(optional = false)
    @Column(name = "estado")
    private Character estado;
    @Basic(optional = false)
    @Column(name = "excedente")
    private Character excedente;
    @Basic(optional = false)
    @Column(name = "porcentual")
    private Character porcentual;
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_cadena", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCadena;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Contrato idContrato;

    public Descuento() {
    }

    public Descuento(Integer id) {
        this.id = id;
    }

    public Descuento(Integer id, BigInteger valorDescuento, Character estado, Character excedente, Character porcentual) {
        this.id = id;
        this.valorDescuento = valorDescuento;
        this.estado = estado;
        this.excedente = excedente;
        this.porcentual = porcentual;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public BigInteger getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(BigInteger valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getExcedente() {
        return excedente;
    }

    public void setExcedente(Character excedente) {
        this.excedente = excedente;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Cliente getIdCadena() {
        return idCadena;
    }

    public void setIdCadena(Cliente idCadena) {
        this.idCadena = idCadena;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Descuento)) {
            return false;
        }
        Descuento other = (Descuento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.comercial.Descuento[ id=" + id + " ]";
    }
    
}

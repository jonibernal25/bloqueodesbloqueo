/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.utils.bloqueo.entities.facturacion;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Entity
@Table(name = "liquidacion_detalle", catalog = "sepsa", schema = "facturacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LiquidacionDetalle.findAll", query = "SELECT l FROM LiquidacionDetalle l"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdLiquidacion", query = "SELECT l FROM LiquidacionDetalle l WHERE l.liquidacionDetallePK.idLiquidacion = :idLiquidacion"),
    @NamedQuery(name = "LiquidacionDetalle.findByNroLinea", query = "SELECT l FROM LiquidacionDetalle l WHERE l.liquidacionDetallePK.nroLinea = :nroLinea"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdTarifa", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idTarifa = :idTarifa"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdDescuento", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idDescuento = :idDescuento"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdClienteRel", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idClienteRel = :idClienteRel"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdGrupoClienteRel", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idGrupoClienteRel = :idGrupoClienteRel"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdSociedadClienteRel", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idSociedadClienteRel = :idSociedadClienteRel"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdPersonaRel", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idPersonaRel = :idPersonaRel"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdLocalRel", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idLocalRel = :idLocalRel"),
    @NamedQuery(name = "LiquidacionDetalle.findByMontoTarifa", query = "SELECT l FROM LiquidacionDetalle l WHERE l.montoTarifa = :montoTarifa"),
    @NamedQuery(name = "LiquidacionDetalle.findByMontoDescuento", query = "SELECT l FROM LiquidacionDetalle l WHERE l.montoDescuento = :montoDescuento"),
    @NamedQuery(name = "LiquidacionDetalle.findByMontoTotal", query = "SELECT l FROM LiquidacionDetalle l WHERE l.montoTotal = :montoTotal"),
    @NamedQuery(name = "LiquidacionDetalle.findByCantDocEnviados", query = "SELECT l FROM LiquidacionDetalle l WHERE l.cantDocEnviados = :cantDocEnviados"),
    @NamedQuery(name = "LiquidacionDetalle.findByCantDocRecibidos", query = "SELECT l FROM LiquidacionDetalle l WHERE l.cantDocRecibidos = :cantDocRecibidos"),
    @NamedQuery(name = "LiquidacionDetalle.findByClienteRelCantLocales", query = "SELECT l FROM LiquidacionDetalle l WHERE l.clienteRelCantLocales = :clienteRelCantLocales"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdTarifaRango", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idTarifaRango = :idTarifaRango"),
    @NamedQuery(name = "LiquidacionDetalle.findByIdTipoNegocio", query = "SELECT l FROM LiquidacionDetalle l WHERE l.idTipoNegocio = :idTipoNegocio"),
    @NamedQuery(name = "LiquidacionDetalle.findByClienteRelCantCadenas", query = "SELECT l FROM LiquidacionDetalle l WHERE l.clienteRelCantCadenas = :clienteRelCantCadenas")})
public class LiquidacionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LiquidacionDetallePK liquidacionDetallePK;
    @Column(name = "id_tarifa")
    private Integer idTarifa;
    @Column(name = "id_descuento")
    private Integer idDescuento;
    @Column(name = "id_cliente_rel")
    private Integer idClienteRel;
    @Column(name = "id_grupo_cliente_rel")
    private Integer idGrupoClienteRel;
    @Column(name = "id_sociedad_cliente_rel")
    private Integer idSociedadClienteRel;
    @Column(name = "id_persona_rel")
    private Integer idPersonaRel;
    @Column(name = "id_local_rel")
    private Integer idLocalRel;
    @Basic(optional = false)
    @Column(name = "monto_tarifa")
    private BigInteger montoTarifa;
    @Basic(optional = false)
    @Column(name = "monto_descuento")
    private BigInteger montoDescuento;
    @Basic(optional = false)
    @Column(name = "monto_total")
    private BigInteger montoTotal;
    @Basic(optional = false)
    @Column(name = "cant_doc_enviados")
    private int cantDocEnviados;
    @Basic(optional = false)
    @Column(name = "cant_doc_recibidos")
    private int cantDocRecibidos;
    @Basic(optional = false)
    @Column(name = "cliente_rel_cant_locales")
    private int clienteRelCantLocales;
    @Column(name = "id_tarifa_rango")
    private Integer idTarifaRango;
    @Column(name = "id_tipo_negocio")
    private Integer idTipoNegocio;
    @Basic(optional = false)
    @Column(name = "cliente_rel_cant_cadenas")
    private int clienteRelCantCadenas;
    @ManyToMany(mappedBy = "liquidacionDetalleCollection")
    private Collection<FacturaDetalle> facturaDetalleCollection;
    @JoinColumn(name = "id_liquidacion", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Liquidacion liquidacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "liquidacionDetalle")
    private Collection<TraficoDetalle> traficoDetalleCollection;

    public LiquidacionDetalle() {
    }

    public LiquidacionDetalle(LiquidacionDetallePK liquidacionDetallePK) {
        this.liquidacionDetallePK = liquidacionDetallePK;
    }

    public LiquidacionDetalle(LiquidacionDetallePK liquidacionDetallePK, BigInteger montoTarifa, BigInteger montoDescuento, BigInteger montoTotal, int cantDocEnviados, int cantDocRecibidos, int clienteRelCantLocales, int clienteRelCantCadenas) {
        this.liquidacionDetallePK = liquidacionDetallePK;
        this.montoTarifa = montoTarifa;
        this.montoDescuento = montoDescuento;
        this.montoTotal = montoTotal;
        this.cantDocEnviados = cantDocEnviados;
        this.cantDocRecibidos = cantDocRecibidos;
        this.clienteRelCantLocales = clienteRelCantLocales;
        this.clienteRelCantCadenas = clienteRelCantCadenas;
    }

    public LiquidacionDetalle(int idLiquidacion, int nroLinea) {
        this.liquidacionDetallePK = new LiquidacionDetallePK(idLiquidacion, nroLinea);
    }

    public LiquidacionDetallePK getLiquidacionDetallePK() {
        return liquidacionDetallePK;
    }

    public void setLiquidacionDetallePK(LiquidacionDetallePK liquidacionDetallePK) {
        this.liquidacionDetallePK = liquidacionDetallePK;
    }

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public Integer getIdClienteRel() {
        return idClienteRel;
    }

    public void setIdClienteRel(Integer idClienteRel) {
        this.idClienteRel = idClienteRel;
    }

    public Integer getIdGrupoClienteRel() {
        return idGrupoClienteRel;
    }

    public void setIdGrupoClienteRel(Integer idGrupoClienteRel) {
        this.idGrupoClienteRel = idGrupoClienteRel;
    }

    public Integer getIdSociedadClienteRel() {
        return idSociedadClienteRel;
    }

    public void setIdSociedadClienteRel(Integer idSociedadClienteRel) {
        this.idSociedadClienteRel = idSociedadClienteRel;
    }

    public Integer getIdPersonaRel() {
        return idPersonaRel;
    }

    public void setIdPersonaRel(Integer idPersonaRel) {
        this.idPersonaRel = idPersonaRel;
    }

    public Integer getIdLocalRel() {
        return idLocalRel;
    }

    public void setIdLocalRel(Integer idLocalRel) {
        this.idLocalRel = idLocalRel;
    }

    public BigInteger getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigInteger montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigInteger getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigInteger montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigInteger getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigInteger montoTotal) {
        this.montoTotal = montoTotal;
    }

    public int getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(int cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public int getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(int cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public int getClienteRelCantLocales() {
        return clienteRelCantLocales;
    }

    public void setClienteRelCantLocales(int clienteRelCantLocales) {
        this.clienteRelCantLocales = clienteRelCantLocales;
    }

    public Integer getIdTarifaRango() {
        return idTarifaRango;
    }

    public void setIdTarifaRango(Integer idTarifaRango) {
        this.idTarifaRango = idTarifaRango;
    }

    public Integer getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public void setIdTipoNegocio(Integer idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    public int getClienteRelCantCadenas() {
        return clienteRelCantCadenas;
    }

    public void setClienteRelCantCadenas(int clienteRelCantCadenas) {
        this.clienteRelCantCadenas = clienteRelCantCadenas;
    }

    @XmlTransient
    public Collection<FacturaDetalle> getFacturaDetalleCollection() {
        return facturaDetalleCollection;
    }

    public void setFacturaDetalleCollection(Collection<FacturaDetalle> facturaDetalleCollection) {
        this.facturaDetalleCollection = facturaDetalleCollection;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @XmlTransient
    public Collection<TraficoDetalle> getTraficoDetalleCollection() {
        return traficoDetalleCollection;
    }

    public void setTraficoDetalleCollection(Collection<TraficoDetalle> traficoDetalleCollection) {
        this.traficoDetalleCollection = traficoDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (liquidacionDetallePK != null ? liquidacionDetallePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LiquidacionDetalle)) {
            return false;
        }
        LiquidacionDetalle other = (LiquidacionDetalle) object;
        if ((this.liquidacionDetallePK == null && other.liquidacionDetallePK != null) || (this.liquidacionDetallePK != null && !this.liquidacionDetallePK.equals(other.liquidacionDetallePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.sepsa.utils.bloqueo.entities.facturacion.LiquidacionDetalle[ liquidacionDetallePK=" + liquidacionDetallePK + " ]";
    }
    
}
